\documentclass[10pt]{beamer}

\usetheme[usetitleprogressbar]{m}

\usepackage{graphicx}
\usepackage{xspace}
\usepackage{booktabs}
\usepackage[normalem]{ulem}
\usepackage[scale=2]{ccicons}

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}
%\usepackage{tikz}
\usetikzlibrary{arrows,matrix,fit,positioning}

\input{../../commands}

%\renewcommand{\mthemetitleformat}{\MakeUppercase}
\title{Improved Parallel Gaussian Elimination for Gr\"obner Basis Computations in
Finite Fields}
\subtitle{}
\date{July 20, 2015}
%\date{\today}
\author{Brice Boyer, Christian Eder, Jean-Charles Faug\`ere, Sylvian Lachartre
  and\\ Fayssal Martani}
\institute{University of Kaiserslautern}
%\titlegraphic{\hfill\includegraphics[height=1.5cm]{../../logo/tu-kl-logo.png}}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Table of Contents}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents[hideallsubsections]
\end{frame}

\section{Linear Algebra for Gr\"obner basis computations}
\begin{frame}{Using Linear Algebra to compute Gr\"obner bases}
\begin{itemize}[<+- | alert@+>]
\item Algorithms like Faug\`ere's \ffo compute Gr\"obner bases via isolating
the tasks of \emph{searching for reducers} and \emph{performing the reduction}.
\item Taking a \emph{subset of S-pairs} a \textbf{symbolic preprocessing} is
performed.
\item Out of this data a \textbf{matrix $M$ is generated}: Its rows correspond to
polynomials, its columns represent all appearing monomials in the given order.
\item Performing \textbf{Gaussian Elimination on $M$} corresponds to reducing
the chosen subset of S-pairs at once.
\item New data for the Gr\"obner basis can then be read off the reduced matrix:
Restore corresponding rows as polynomials.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Idea by Faug\`ere \& Lachartre}
Specialize \alert{Linear Algebra} for reduction steps in GB computations.
\begin{center}
\begin{tikzpicture}[scale=.80, transform shape]
\onslide<2-> {
\matrix [cells={scale=1, transform shape},matrix of math nodes,ampersand replacement=\&]
{
    \color<6->{magenta}{1} \& 3 \& 0 \& 0 \& 7 \& 1 \& 0 \\
    \color<6->{magenta}{1} \& 0 \& 4 \& 1 \& 0 \& 0 \& 5 \\
    \alert<5->{0} \& \color<6->{magenta}{1} \& 6 \& 0 \& 8 \& 0 \& 1 \\
    \alert<5->{0} \& \color<6->{magenta}{1} \& 0 \& 0 \& 0 \& 7 \& 0 \\
    \alert<5->{0} \& \alert<5->{0} \& \alert<5->{0} \&
      \alert<5->{0} \& \color<6->{magenta}{1} \& 3 \& 1 \\
};
}
\onslide<4-> {
\begin{scope}[shift={(-2.2,1.45)}]
\draw [decorate,decoration={brace,mirror,amplitude=4pt},xshift=0pt,yshift=-0.0cm]
(-0,-0.1) -- (0,-1.1) node [midway,xshift=-1.2cm] {S-pair};
\draw [decorate,decoration={brace,mirror,amplitude=4pt},xshift=0pt,yshift=0.0cm]
(-0,-1.3) -- (0,-2.3) node [midway,xshift=-1.2cm] {S-pair};
\draw[->] (-0,-2.65) -- (-0.3,-2.65) node[xshift=-1.0cm] {reducer};
\end{scope}
}
\end{tikzpicture}
\end{center}
\onslide<3->
\begin{center}
Try to exploit underlying GB structure.
\end{center}
\onslide<7->
\begin{block}{Main idea}
Do a static \alert{reordering before} the Gaussian Elimination to achieve a
better initial shape. \alert{Invert the reordering afterwards}.
\end{block}
\end{frame}

\begin{frame}
\frametitle{Faug\`ere-Lachartre Idea}
\alert{1st step}: Sort pivot and non-pivot columns
\begin{center}
\begin{tikzpicture}[scale=.80, transform shape]
\onslide<1-> {
\matrix [cells={scale=1, transform shape},matrix of math nodes,ampersand replacement=\&]
{
    \color<2->{orange}{1} \& \color<3->{orange}{3} \&
      \color<4->{magenta}{0}
      \& \color<5->{magenta}{0} \& \color<5->{orange}{7}
      \& \color<5->{magenta}{1} \& \color<5->{magenta}{0} \\
    \color<2->{orange}{1} \& \color<3->{orange}{0} \&
    \color<4->{magenta}{4}
      \& \color<5->{magenta}{1} \& \color<5->{orange}{0}
      \& \color<5->{magenta}{0} \& \color<5->{magenta}{5} \\
    \color<2->{orange}{0} \& \color<3->{orange}{1} \&
    \color<4->{magenta}{6}
      \& \color<5->{magenta}{0} \& \color<5->{orange}{8} \&
        \color<5->{magenta}{0} \& \color<5->{magenta}{1} \\
    \color<2->{orange}{0} \& \color<3->{orange}{1} \&
    \color<4->{magenta}{0}
      \& \color<5->{magenta}{0} \& \color<5->{orange}{0}
      \& \color<5->{magenta}{7} \& \color<5->{magenta}{0} \\
    \color<2->{orange}{0} \& \color<3->{orange}{0} \&
    \color<4->{magenta}{0}
      \& \color<5->{magenta}{0} \& \color<5->{orange}{1} \&
        \color<5->{magenta}{3} \& \color<5->{magenta}{1} \\
};
}
\begin{scope}[shift={(-2.2,1.45)}]
\onslide<2-> {
\node (pivots) at (-1,-5) {Pivot column};
\path[->,>=latex] (pivots) edge[in=-90, out=90] (0.6,-3);
}
\onslide<4-> {
\node (nonpivots) at (3,-5) {Non-Pivot column};
\path[->,>=latex] (nonpivots) edge[in=-90, out=90] (1.65,-3);
}
\end{scope}
\onslide<6-> {
\begin{scope}[shift={(7,0)}]
\path[->,>=latex] (-4.5,0) edge[in=170,out=10] (-2.5,0);
\matrix [cells={scale=1, transform shape},matrix of math nodes,ampersand replacement=\&]
{
    \color<6->{orange}{1} \& \color<6->{orange}{3} \&
    \color<6->{orange}{7} \& \color<6->{magenta}{0} \&
    \color<6->{magenta}{0} \& \color<6->{magenta}{1} \&
    \color<6->{magenta}{0} \\
    \color<6->{orange}{1} \& \color<6->{orange}{0} \&
    \color<6->{orange}{0} \& \color<6->{magenta}{4} \&
    \color<6->{magenta}{1} \& \color<6->{magenta}{0} \&
    \color<6->{magenta}{5} \\
    \color<6->{orange}{0} \& \color<6->{orange}{1} \&
    \color<6->{orange}{8} \& \color<6->{magenta}{6} \&
    \color<6->{magenta}{0} \& \color<6->{magenta}{0} \&
    \color<6->{magenta}{9} \\
    \color<6->{orange}{0} \& \color<6->{orange}{1} \&
    \color<6->{orange}{0} \& \color<6->{magenta}{0} \&
    \color<6->{magenta}{0} \& \color<6->{magenta}{7} \&
    \color<6->{magenta}{0} \\
    \color<6->{orange}{0} \& \color<6->{orange}{0} \&
    \color<6->{orange}{1} \& \color<6->{magenta}{0} \&
    \color<6->{magenta}{0} \& \color<6->{magenta}{3} \&
    \color<6->{magenta}{1} \\
};
\draw[dashed] (-0.25,1.6) -- (-0.25,-1.6);
\end{scope}
}
\end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Faug\`ere-Lachartre Idea}
\alert{2nd step}: Sort pivot and non-pivot rows
\begin{center}
\begin{tikzpicture}[scale=.80, transform shape]
\onslide<1-> {
\matrix [cells={scale=1, transform shape},matrix of math nodes,ampersand replacement=\&]
{
    \color<3->{magenta}{1} \& \color<3->{magenta}{3} \&
    \color<3->{magenta}{7} \& \color<3->{magenta}{0} \&
    \color<3->{magenta}{0} \& \color<3->{magenta}{1} \&
    \color<3->{magenta}{0} \\
    \color<2->{orange}{1} \& \color<2->{orange}{0} \&
    \color<2->{orange}{0} \& \color<2->{orange}{4} \&
    \color<2->{orange}{1} \& \color<2->{orange}{0} \&
    \color<2->{orange}{5} \\
    \color<4->{magenta}{0} \& \color<4->{magenta}{1} \&
    \color<4->{magenta}{8} \& \color<4->{magenta}{6} \&
    \color<4->{magenta}{0} \& \color<4->{magenta}{0} \&
    \color<4->{magenta}{9} \\
    \color<4->{orange}{0} \& \color<4->{orange}{1} \&
    \color<4->{orange}{0} \& \color<4->{orange}{0} \&
    \color<4->{orange}{0} \& \color<4->{orange}{7} \&
    \color<4->{orange}{0} \\
    \color<4->{orange}{0} \& \color<4->{orange}{0} \&
    \color<4->{orange}{1} \& \color<4->{orange}{0} \&
    \color<4->{orange}{0} \& \color<4->{orange}{3} \&
    \color<4->{orange}{1} \\
};
\draw[dashed] (-0.25,1.6) -- (-0.25,-1.6);
}
\begin{scope}[shift={(-2.2,1.45)}]
\onslide<2-> {
\node (pivots) at (-1,-5) {Pivot row};
\path[->,>=latex] (pivots) edge[in=180, out=0] (0.1,-0.9);
}
\onslide<3-> {
\node (nonpivots) at (3,-5) {Non-Pivot row};
\path[->,>=latex] (nonpivots) edge[in=180, out=-180] (0.1,-0.35);
}
\end{scope}
\onslide<5-> {
\begin{scope}[shift={(7,0)}]
\path[->,>=latex] (-4.5,0) edge[in=170,out=10] (-2.5,0);
\matrix [cells={scale=1, transform shape},matrix of math nodes,ampersand replacement=\&]
{
    \color<2->{orange}{1} \& \color<2->{orange}{0} \&
    \color<2->{orange}{0} \& \color<2->{orange}{4} \&
    \color<2->{orange}{1} \& \color<2->{orange}{0} \&
    \color<2->{orange}{5} \\
    \color<4->{orange}{0} \& \color<4->{orange}{1} \&
    \color<4->{orange}{0} \& \color<4->{orange}{0} \&
    \color<4->{orange}{0} \& \color<4->{orange}{7} \&
    \color<4->{orange}{0} \\
    \color<4->{orange}{0} \& \color<4->{orange}{0} \&
    \color<4->{orange}{1} \& \color<4->{orange}{0} \&
    \color<4->{orange}{0} \& \color<4->{orange}{3} \&
    \color<4->{orange}{1} \\
    \color<3->{magenta}{1} \& \color<3->{magenta}{3} \&
    \color<3->{magenta}{7} \& \color<3->{magenta}{0} \&
    \color<3->{magenta}{0} \& \color<3->{magenta}{1} \&
    \color<3->{magenta}{0} \\
    \color<4->{magenta}{0} \& \color<4->{magenta}{1} \&
    \color<4->{magenta}{8} \& \color<4->{magenta}{6} \&
    \color<4->{magenta}{0} \& \color<4->{magenta}{0} \&
    \color<4->{magenta}{9} \\
};
\draw[dashed] (-2,-0.28) -- (2,-0.28);
\draw[dashed] (-0.25,1.6) -- (-0.25,-1.6);
\end{scope}
}
\end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Faug\`ere-Lachartre Idea}
\alert{3rd step}: Reduce lower left part to zero
\begin{center}
\begin{tikzpicture}[scale=.80, transform shape]
\onslide<1-> {
\matrix [cells={scale=1, transform shape},matrix of math nodes,ampersand replacement=\&]
{
    1 \& 0 \&
    0 \& 4 \&
    1 \& 0 \&
    5 \\
    0 \& 1 \&
    0 \& 0 \&
    0 \& 7 \&
    0 \\
    0 \& 0 \&
    1 \& 0 \&
    0 \& 3 \&
    1 \\
    1 \& 3 \&
    7 \& 0 \&
    0 \& 1 \&
    0 \\
    0 \& 1 \&
    8 \& 6 \&
    0 \& 0 \&
    9 \\
};
\draw[dashed] (-2,-0.28) -- (2,-0.28);
\draw[dashed] (-0.25,1.6) -- (-0.25,-1.6);
}
\onslide<2-> {
\begin{scope}[shift={(8,0)}]
\path[->,>=latex] (-5.5,0) edge[in=170,out=10] (-2.7,0);
\matrix [cells={scale=1, transform shape},matrix of math nodes,ampersand replacement=\&]
{
    1 \& 0 \&
    0 \& 4 \&
    1 \& 0 \&
    5 \\
    0 \& 1 \&
    0 \& 0 \&
    0 \& 7 \&
    0 \\
    0 \& 0 \&
    1 \& 0 \&
    0 \& 3 \&
    1 \\
    \alert 0 \& \alert 0 \&
    \alert 0 \& \alert 7 \&
    \alert{10} \& \alert 3 \&
    \alert{10} \\
    \alert 0 \& \alert 0 \&
    \alert 0 \& \alert 6 \&
    \alert 0 \& \alert 2 \&
    \alert 1 \\
};
\draw[dashed] (-2.1,-0.28) -- (2.1,-0.28);
\draw[dashed] (-0.45,1.6) -- (-0.45,-1.6);
\end{scope}
}
\end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Faug\`ere-Lachartre Idea}
\alert{4th step}: Reduce lower right part
\begin{center}
\begin{tikzpicture}[scale=.80, transform shape]
\onslide<1-> {
\matrix [cells={scale=1, transform shape},matrix of math nodes,ampersand replacement=\&]
{
    1 \& 0 \&
    0 \& 4 \&
    1 \& 0 \&
    5 \\
    0 \& 1 \&
    0 \& 0 \&
    0 \& 7 \&
    0 \\
    0 \& 0 \&
    1 \& 0 \&
    0 \& 3 \&
    1 \\
    0 \& 0 \&
    0 \& 7 \&
    10 \& 3 \&
    10 \\
    0 \& 0 \&
    0 \& 6 \&
    0 \& 2 \&
    1 \\
};
\draw[dashed] (-2.1,-0.28) -- (2.1,-0.28);
\draw[dashed] (-0.45,1.6) -- (-0.45,-1.6);
}
\onslide<2-> {
\begin{scope}[shift={(8,0)}]
\path[->,>=latex] (-5.5,0) edge[in=170,out=10] (-2.7,0);
\matrix [cells={scale=1, transform shape},matrix of math nodes,ampersand replacement=\&]
{
    1 \& 0 \&
    0 \& 4 \&
    1 \& 0 \&
    5 \\
    0 \& 1 \&
    0 \& 0 \&
    0 \& 7 \&
    0 \\
    0 \& 0 \&
    1 \& 0 \&
    0 \& 3 \&
    1 \\
    0 \& 0 \&
    0 \& \alert 7 \&
    \alert 0 \& \alert 6 \&
    \alert 3 \\
    0 \& 0 \&
    0 \& \alert 0 \&
    \alert 4 \& \alert 1 \&
    \alert 5 \\
};
\draw[dashed] (-2.1,-0.28) -- (2.1,-0.28);
\draw[dashed] (-0.3,1.6) -- (-0.3,-1.6);
\end{scope}
}
\end{tikzpicture}
\end{center}
\onslide<3->
\alert{5th step}: Remap columns and get new polynomials for GB out of lower
right part.

\end{frame}

\plain{So, how do ``real world'' matrices from GB computations look like?}

\begin{frame}
\frametitle{How our matrices look like}
\begin{center}
\includegraphics[width=0.6\textwidth]{orig-mat-inv.png}
\end{center}
\end{frame}

\begin{frame}
\frametitle{How our matrices look like}
Some data about the matrix:
\onslide<+->
\begin{itemize}
\item \ffo computation of homogeneous \alert{\textsc{Katsura-12}}, degree $6$ matrix
\onslide<+->
\item Size $55$MB
\onslide<+->
\item \alert{$24,006,869$} nonzero elements (density: $5\%$)
\onslide<+->
\item Dimensions: \\
\vspace*{3mm}
$
\begin{array}{rrcrl}
\text{full matrix:} & 21,182 & \times & 22,207 &\\
&&&&\\
\onslide<+->
\text{upper-left:} & 17,915 & \times & 17,915 & \text{\alert{known pivots}}\\
\text{lower-left:} & 3,267 & \times & 17,915 &\\
\text{upper-right:} & 17,915 & \times & 4,292 &\\
\text{lower-right:} & 3,267 & \times & 4,292 & \text{\alert{new information}}
\end{array}
$
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{How our matrices look like}
\begin{center}
\includegraphics[width=0.6\textwidth]{orig-mat-inv.png}
\end{center}
\end{frame}

\begin{frame}
\frametitle{How our matrices look like}
\begin{center}
\includegraphics[width=0.6\textwidth]{ABCD-1-inv.png}
\end{center}
\end{frame}
\begin{frame}
\frametitle{Hybrid Matrix Multiplication
  A$^{\text{-1}}$B}
\begin{center}
\includegraphics[width=0.6\textwidth]{ABCD-1-color-inv.png}
\end{center}
\end{frame}
\begin{frame}
\frametitle{Hybrid Matrix Multiplication
  A$^{\text{-1}}$B}
\begin{center}
\includegraphics[width=0.6\textwidth]{ABCD-2-inv.png}
\end{center}
\end{frame}
\begin{frame}
\frametitle{Reduce C to zero}
\begin{center}
\includegraphics[width=0.6\textwidth]{ABCD-3-inv.png}
\end{center}
\end{frame}
\begin{frame}
\frametitle{Gaussian Elimination on D}
\begin{center}
\includegraphics[width=0.6\textwidth]{ABCD-3-red-inv.png}
\end{center}
\end{frame}
\begin{frame}
\frametitle{New information}
\begin{center}
\includegraphics[width=0.6\textwidth]{ABCD-4-inv.png}
\end{center}
\end{frame}

\section{Features of GBLA}

\begin{frame}{Library Overview}
\begin{itemize}[<+- | alert@+>]
\item \textbf{Open source} library written in \textbf{plain C}.
\item Specialized linear algebra for GB computations.
\item \textbf{Parallel implementation} (OpenMP), scaling ``nicely'' up to $32$ cores.
\item Works over finite fields for $16$-bit primes (at the moment).
\item \textbf{Several strategies} for splicing and reduction.
\item Includes \textbf{converter} from and to our dedicated matrix format.
\item Access to \textbf{huge matrix database}: $>500$ matrices, $>280$GB of data.
\end{itemize}
\onslide<+->
\begin{center}
\alert{\url{http://hpac.imag.fr/gbla}}
\end{center}
\end{frame}

\begin{frame}{Exploiting block structures in matrices}
\begin{center}
\onslide<+->
Matrices from GB computations have \textbf{nonzero entries} often \textbf{grouped in
  blocks}.
\end{center}
\begin{center}
$
\begin{array}{rl}
 \text{\textbf{Horizontal Pattern}} &\text{If $m_{i,j} \neq 0$ then often $m_{i,j+1} \neq
   0$.}\\
\onslide<+->
\text{\textbf{Vertical Pattern}} & \text{If $m_{i,j} \neq 0$ then often $m_{i+1,j} \neq
  0$.}
\end{array}
$
\end{center}
\begin{itemize}[<+- | alert@+>]
  \item Can be used to optimize \axpy and \trsm operations in \flr.
  \item Horizontal pattern taken care of canonically.
  \item Need to take care of vertical pattern.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Multiline TRSM step}
\begin{tikzpicture}[scale=0.65]
\begin{scope}[shift={(0,0)}]
  \matrix [matrix of math nodes, nodes={minimum height=10pt,text height=1.5ex,
    text depth=.3ex, transform shape}, nodes in empty cells,
  row sep=-2pt, column sep=-2pt, left delimiter={[},right delimiter={]}] (m)
  {
    \ddots &\vdots &\vdots &\vdots &\vdots &\vdots &\vdots\\
      & 1 & \cdots &* &* &* &* \\
      &  & 1 & \cdots & a_{i,j} & a_{i,j+1} &*\\
      &  &   & \ddots &\vdots & \vdots &\vdots\\
      &  &   &       & 1 & \cdots &*\\
      &  &   &       &   & 1 &* \\
      &  &   &       &   &   &1 \\
  };
  \node[above=5pt of m]{$A$};
  %\node[fit=(m-1-2)(m-1-7)]{$\cdots \cdots \cdots$};
  \fill[draw=green!70!ggrey, fill=green!70!ggrey, fill opacity=0.2] (m-3-5.north west) -- (m-3-6.north east) -- (m-3-6.south east) -- (m-3-5.south west) -- (m-3-5.north west);
  \fill[draw=orange, fill=orange, fill opacity=0.2] (m-5-5.north west) -- (m-5-5.north east) -- (m-5-5.south east) -- (m-5-5.south west) -- (m-5-5.north west);
  \fill[draw=orange, fill=orange, fill opacity=0.2] (m-6-6.north west) -- (m-6-6.north east) -- (m-6-6.south east) -- (m-6-6.south west) -- (m-6-6.north west);
  \draw[color=gray,very thick,<-,shorten >=2pt, shorten <=2pt](m-5-5.north) -- (m-3-5.south);
  \draw[color=gray,very thick,<-,shorten >=2pt, shorten <=2pt](m-6-6.north) -- (m-3-6.south);
\end{scope}
\begin{scope}[shift={(7.5,0)}]
  \matrix [matrix of math nodes, nodes={minimum height=10pt,text height=1.5ex,
    text depth=.3ex}, nodes in empty cells,
  row sep=-2pt, column sep=-2pt, left delimiter={[},right delimiter={]}] (n)
  {
    \vdots &\vdots&\vdots&\vdots&\vdots&\vdots&\vdots\\
    * &* &\cdots &* &\cdots &* &*\\
     * & * & \cdots &b_{i,\ell}&* &\cdots&* \\
    \vdots &\vdots&\vdots&\vdots&\vdots&\vdots&\vdots\\
     * & * & \cdots &b_{k,\ell}&* &\cdots&* \\
     * & * & \cdots &b_{k+1,\ell}&* &\cdots&* \\
    * &* &\cdots &* &\cdots &* &*\\
  };
  \node[above=5pt of n]{$B$};
  %\node[fit=(m-1-2)(m-1-7)]{$\cdots \cdots \cdots$};
  \fill[draw=blue, fill=blue, fill opacity=0.2] (n-3-4.north west) -- (n-3-4.north east) -- (n-3-4.south east) -- (n-3-4.south west) -- (n-3-4.north west);
  \fill[draw=red, fill=red, fill opacity=0.2] (n-5-4.north west)+(-0.2,0) rectangle (n-6-4.south east);
  \draw[color=gray,very thick,->,shorten >=2pt, shorten <=2pt](n-5-4.north) -- (n-3-4.south);

  \path[color=gray,very thick,->,shorten >=9pt, shorten <=3pt] (m-5-5.east) edge[out=15,in=165] (n-5-4.west);
  \path[color=gray,very thick,->,shorten >=5pt, shorten <=3pt] (m-6-6.east) edge[out=-15,in=-165] (n-6-4.west);
\end{scope}
\end{tikzpicture}

\begin{center}
Exploiting \alert{horizontal} and \alert{vertical} patterns in the \trsm step.
\end{center}
\end{frame}

\begin{frame}
\frametitle{Multiline data structure -- an example}
\onslide<1->
Consider the following two rows:
\begin{center}
$
\begin{array}[]{rcccccccccc}
\ro & = & [ & 2 & 3 & 0 & 1 & 4 & 0 & 5 & ],\\
\rt & = & [ & 1 & 7 & 0 & 0 & 3 & 1 & 2 & ].
\end{array}
$
\end{center}
\onslide<2->
A sparse vector representation of the two rows would be
given by
\begin{center}
$
\begin{array}[]{rcccccccc}
\ro.\val & = & [ & 2 & 3 & 1 & 4 & 5 & ],\\
\ro.\pos & = & [ & 0 & 1 & 3 & 4 & 6 & ],\\
&&&&&&&&\\
\rt.\val & = & [ & 1 & 7 & 3 & 1 & 2 & ],\\
\rt.\pos & = & [ & 0 & 1 & 4 & 5 & 6 & ].
\end{array}
$
\end{center}
\onslide<3->
A \alert{multiline vector} representation of $\ro$ and $\rt$ is given by
\begin{center}
$
\begin{array}[]{lccccccccccccccc}
\ml.\val & = & [ & 2 & 1 & 3 & 7 & 1 & \alert<4->{0} & 4 & 3 & \alert<4->{0} & 1 & 5 & 2 & ],\\
\ml.\pos & = & [ & 0 & 1 & 3 & 4 & 5 & 6 & ].\\
\end{array}
$
\end{center}
\end{frame}

\begin{frame}{New order of operations}
\begin{itemize}[<+- | alert@+>]
  \item Number of initially known pivots (i.e. \# rows of $A$ and $B$) is large
  compared to \# rows of $C$ and $D$.
  \item Most time of \flr is spent in \trsm step $A^{-1}B$.
  \item Only interested in $D$ resp. rank of $M$?\\
\end{itemize}
\onslide<+->
\begin{center}
    \textbf{Change order of
  operations.}
\end{center}
\vspace*{-5mm}
\begin{enumerate}[<+- | alert@+>]
\item Reduce $C$ directly with $A$ (store corresponding data in $C$).
\item Carry out corresponding operations from $B$
    to $D$ using updated $C$.
\item Reduce $D$.
\end{enumerate}
\end{frame}

\begin{frame}{GBLA Matrix formats}
\begin{itemize}[<+- | alert@+>]
\item Matrices are pretty sparse, but structured.
\item \gbla supports \textbf{two matrix formats}, both use binary format.
\item \gbla includes a converter between the two supported formats and can also
dump to \magma matrix format.
\end{itemize}
\onslide<+->
\begin{table}
  \caption{\footnotesize{Old matrix format (legacy version)}}
  \vspace*{-2mm}
  \scalebox{0.7}{
  \begin{tabular}{c|c|c|l}
    \toprule
    Size & Length & Data & Description\\
    \midrule
    \texttt{uint32\_t} & $1$ & \texttt b & version number\\
    \texttt{uint32\_t} & $1$ & \texttt m & \# rows\\
    \texttt{uint32\_t} & $1$ & \texttt n & \# columns\\
    \texttt{uint32\_t} & $1$ & \texttt p & prime / field characteristic\\
    \texttt{uint64\_t} & $1$ & \texttt{nnz} & \# nonzero entries\\
    \texttt{uint16\_t} & \texttt{nnz} & \texttt{data} & \# entries in matrix\\
    \texttt{uint32\_t} & \texttt{nnz} & \texttt{cols} & column index of entry\\
    \texttt{uint32\_t} & \texttt m & \texttt{rows} & length of rows\\
    \bottomrule
  \end{tabular}
  }
\end{table}

\end{frame}

\begin{frame}{GBLA Matrix formats}
\onslide<1->
\vspace*{-3mm}
\begin{table}
  \caption{\footnotesize{New matrix format (compressing \texttt{data} and
      \texttt{cols})}}
  \vspace*{-2mm}
  \scalebox{0.7}{
  \begin{tabular}{c|c|c|l}
    \toprule
    Size & Length & Data & Description\\
    \midrule
    \texttt{uint32\_t} & $1$ & \texttt b & version number \onslide<2->{\alert{$+$ information for
    data type of \texttt{pdata}}}\\
    \texttt{uint32\_t} & $1$ & \texttt m & \# rows\\
    \texttt{uint32\_t} & $1$ & \texttt n & \# columns\\
    \texttt{uint32\_t} & $1$ & \texttt p & prime / field characteristic\\
    \texttt{uint64\_t} & $1$ & \texttt{nnz} & \# nonzero entries\\
    \textcolor{vlgrey!70!white}{\texttt{uint16\_t}} &
    \textcolor{vlgrey!70!white}{\texttt{nnz}} &
    \textcolor{vlgrey!70!white}{\texttt{data}} &
    \alert{several rows are of type $x_i f_j$}\\
    \textcolor{vlgrey!70!white}{\texttt{uint32\_t}} &
    \textcolor{vlgrey!70!white}{\texttt{nnz}} &
    \textcolor{vlgrey!70!white}{\texttt{cols}} &
    \alert{can be compressed for consecutive elements}\\
    \texttt{uint32\_t} & \texttt m & \texttt{rows} & length of rows\\
    \midrule
    \texttt{uint32\_t} & \texttt m & \texttt{pmap} & maps rows to \texttt{pdata}\\
    \texttt{uint64\_t} & $1$ & \texttt{k} & size of compressed \texttt{colid}\\
    \texttt{uint64\_t} & \texttt k & \texttt{colid} & compression of columns:\\
    &&&Single column entry masked via $(1 << 31)$;\\
    &&&$s$ consecutive entries starting at column $c$ are stored as ``$c\;s$''\\
    \texttt{uint32\_t} & $1$ & \texttt{pnb} & \# polynomials\\
    \texttt{uint64\_t} & $1$ & \texttt{pnnz} & \# nonzero coefficients in polynomials\\
    \texttt{uint32\_t} & \texttt{pnb} & \texttt{prow} & length of polynomial /
    row representation\\
    \texttt{xinty\_t} & \texttt{pnnz} & \texttt{pdata} & coefficients of
    polynomials\\
    \bottomrule
  \end{tabular}
  }
\end{table}

\end{frame}

\begin{frame}{GBLA Matrix formats -- Comparison}
\onslide<+->
\begin{table}
	\centering
	\caption{\footnotesize{Storage and time efficiency of the new format}}
  \vspace*{-2mm}
  \scalebox{0.7}{
	\begin{tabular}{c|cc|cc|cc}
		\toprule
		Matrix & Size old & Size new & \texttt{gzip}ped old & \texttt{gzip}ped new & Time old & Time new \\
		\midrule
		\texttt{F4-kat14-mat9} & 2.3GB & 0.74GB & 1.2GB & 0.29GB & 230s & 66s \\
		\texttt{F5-kat17-mat10} & 43GB & 12GB & 24GB & 5.3GB & 4419s & 883s \\
		\bottomrule
	\end{tabular}
  }
\end{table}
\onslide<+->
\begin{center}
\textbf{New format} vs. \textbf{Old format}
\end{center}
\begin{itemize}[<+- | alert@+>]
  \item $1/3$rd of memory usage.
  \item $1/4$th of memory usage when compressed with \texttt{gzip}.
  \item Compression $4-5$ times faster.
\end{itemize}
\end{frame}

\section{Some benchmarks}
\begin{frame}
\frametitle{GBLA vs. Faug\`ere-Lachartre}
\begin{table}
	\centering
	\renewcommand{\tabcolsep}{1mm}
  \scalebox{0.9}{
	\begin{tabular}{c|ccc|ccc}
		\toprule
    Implementation & \multicolumn{3}{c|}{FL Implementation} & \multicolumn{3}{c}{\gbla} \\
    \midrule
		Matrix/Threads:& 1 & 16 & 32  & 1 & 16 & 32 \\
    \midrule
\texttt{F5-kat13-mat5} & 16.7& 2.7 & 2.3 &  \ok{ 14.5} & \ok{ 2.02} & \ok{ 1.87} \\
\texttt{F5-kat13-mat6} & 27.3 &  4.15  & 4.0 & \ok{ 23.9} & \ok{ 3.08} & \ok{
  2.65} \\[1.5mm]
\texttt{F5-kat14-mat7} & \ok{ 139} & 17.4 & 16.6 & 142 & \ok{ 13.4} & \ok{ 10.6} \\ 
\texttt{F5-kat14-mat8} & 181 & 24.95 & 23.1 & \ok{ 177} & \ok{ 16.9} & \ok{
  12.7} \\[1.5mm]
\texttt{F5-kat15-mat7} & \ok{ 629} & 61.8 & 55.6 & 633 & \ok{ 55.1} & \ok{ 38.2}
\\[1.5mm]
\texttt{F5-kat16-mat6} & 1,203 & 110 & 83.3 & \ok{ 1,147 }& \ok{ 98.7} & \ok{
  69.9} \\[1.5mm] 
\texttt{F5-mr-9-10-7-mat3} & \ok{ 591} & 70.8 & 71.3 & 733 & \ok{ 57.3} & \ok{ 37.9} \\
    \bottomrule
	\end{tabular}
  }
\end{table}
\end{frame}

\begin{frame}
\frametitle{GBLA vs. MAGMA V2.20-10}
\begin{table}
	\centering
	\renewcommand{\tabcolsep}{1mm}
  \scalebox{0.9}{
	\begin{tabular}{c|c|ccc}
    \toprule
		Implementation & \magma & \multicolumn{3}{c}{\gbla} \\
    \midrule
		Matrix/Threads: & 1 & 1 & 16  & 32 \\
    \midrule
    \texttt{F4-kat12-mat9} & \ok{ 11.2} & 11.4       & 1.46  & 1.60 \\[1.5mm]
    \texttt{F4-kat13-mat2} & \ok{ 0.94}& 1.18       & 0.38  & 0.61  \\
    \texttt{F4-kat13-mat3} & \ok{ 9.33} & 11.0       & 1.70  & 3.10  \\
    \texttt{F4-kat13-mat9} & 168 & \ok{ 165}    & 16.0  & 11.8  \\[1.5mm]
    \texttt{F4-kat14-mat8} & 2747  & \ok{ 2545}   & 207   & 165  \\[1.5mm]
    \texttt{F4-kat15-mat7} & 10,345 & \ok{ 9,514}  & 742   & 537  \\
    \texttt{F4-kat15-mat8} & 13,936 & \ok{ 12,547} & 961   & 604   \\
    \texttt{F4-kat15-mat9} & 24,393 & \ok{ 22,247} & 1,709 & 1,256 \\
    \bottomrule
	\end{tabular}
  }
\end{table}
\begin{center}
\textbf{Note} that \magma generates slightly bigger matrices for the given
examples.
\end{center}
\end{frame}

\begin{frame}[shrink=8]
\frametitle{References}
\bibliographystyle{plain}
%\bibliography{/home/ederc/uni/publications/bibliography/bib}{}
\begin{thebibliography}{10}
\bibitem{bGroebner1965}
{Buchberger, B.}
\newblock {Ein Algorithmus zum Auffinden der Basiselemente des Restklassenringes nach einem nulldimensionalen Polynomideal},
  1965.
\newblock {PhD thesis, Universtiy of Innsbruck, Austria}

\bibitem{bGroebner1979}
{Buchberger, B.}
\newblock {A criterion for detecting unnecessary reductions in the construction of Gr{\"o}bner bases},
  1979.
\newblock {EUROSAM '79, An International Symposium on Symbolic and Algebraic Manipulation}

\bibitem{buchberger2ndCriterion1985}
{Buchberger, B.}
\newblock {Gr{\"o}bner Bases: An Algorithmic Method in Polynomial Ideal 
  Theory},
  1985.
\newblock {Multidimensional Systems Theory, D. Reidel Publication 
  Company}

\bibitem{eder-faugere-2014}
{Eder, C.} and {Faug\`ere, J.-C.}
\newblock {A survey on signature-based Groebner basis algorithms}, 2014.
\newblock {\url{http://arxiv.org/abs/1404.1774}}

\bibitem{F99}
{Faug\`ere, J.-C.}
\newblock {A new efficient algorithm for computing Gr\"obner bases (F4)}, 1999.
\newblock {Journal of Pure and Applied Algebra}

\bibitem{F02}
{Faug\`ere, J.-C.}
\newblock {A new efficient algorithm for computing Gr\"obner bases without 
  reduction to zero (F5)}, 2002.
\newblock {Proceedings of the 2002 international symposium on Symbolic and algebraic computation}

\bibitem{FL10}
{Faug\`ere, J.-C.} and {Lachartre, S.}
\newblock {Parallel Gaussian Elimination for Gr\"obner bases computations in 
  finite fields}, 2010.
\newblock {Proceedings of the 4th International Workshop on Parallel and Symbolic Computation}

\bibitem{gm-1988}
{Gebauer, R.} and {M\"oller, H. M.}
\newblock {On an installation of Buchberger's algorithm}, 1988.
\newblock {Journal of Symbolic Computation}
\end{thebibliography}
\end{frame}

\plain{Thank you!}
\plain{Comments? Questions?}

\end{document}
