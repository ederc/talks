Computing Groebner Bases

In 1965 Buchberger introduced a first algorithmic approach to the computation of
Groebner Bases. Over the last decades optimizations to this basic attempt were found.
In this talk we discuss two main aspects of the computation of Groebner Bases:
Predicting zero reductions is essential to keep the computational overhead and
memory usage at a low. We show how Faugère's idea, initially presented in the F5
algorithm, can be generalized and further improved. The 2nd part of this talk is
dedicated to the exploitation of the algebraic structures of a Groebner Basis.
Thus we are not only able to replace polynomial reduction by linear algebra
(Macaulay matrices, Faugère's F4 algorithm), but we can also specialize the
Gaussian Elimination process for our purposes.
