git commit: 20b3d03dd95cf356487549de61117ad57ec90e96
---------------------------------------------------------------------------
----------------------------- Computing Groebner --------------------------
--------------------- with the following options set ----------------------
---------------------------------------------------------------------------
number of threads                        12
hash table size                    16777216 (2^24)
compute reduced basis?                    0
do not reduce A|B in gbla                 0
limit for handling spairs??            7000
use simplify?                             1
generate pbm files?                       0
print resulting basis?                    0
---------------------------------------------------------------------------
Loading input data ...                    0.000 sec
---------------------------------------------------------------------------
Data for tests/cyclic/affine/10
---------------------------------------------------------------------------
field characteristic                  65521
monomial order                          DRL
number of variables                      10
number of generators                     10
homogeneous input?                        0
homogeneous computation?                  0
input file size                           1.44 KB
---------------------------------------------------------------------------
step   1 :     1 spairs of degree   1  --->        1 x      10 matrix
step   2 :     1 spairs of degree   2  --->        3 x      25 matrix
step   3 :     1 spairs of degree   3  --->        6 x      55 matrix
step   4 :     2 spairs of degree   4  --->       18 x     210 matrix
step   5 :     4 spairs of degree   5  --->       71 x     518 matrix
step   6 :     9 spairs of degree   6  --->      125 x    1130 matrix
step   7 :    22 spairs of degree   7  --->      435 x    2796 matrix
step   8 :    47 spairs of degree   8  --->     1323 x    6196 matrix
step   9 :   106 spairs of degree   9  --->     2718 x   10967 matrix
step  10 :   254 spairs of degree  10  --->     5968 x   19194 matrix
step  11 :   571 spairs of degree  11  --->    11008 x   30606 matrix
step  12 :  1192 spairs of degree  12  --->    22088 x   51284 matrix
step  13 :  2596 spairs of degree  13  --->    44085 x   85050 matrix
step  14 :  4974 spairs of degree  14  --->    77862 x  132094 matrix
step  15 :  7001 spairs of degree  15  --->    71248 x  114365 matrix
step  16 :     4 spairs of degree   6  --->      137 x    1441 matrix
step  17 :    17 spairs of degree   7  --->      716 x    3759 matrix
step  18 :    58 spairs of degree   8  --->     2061 x    8041 matrix
step  19 :   109 spairs of degree   9  --->     4676 x   15009 matrix
step  20 :   243 spairs of degree  10  --->     9937 x   26547 matrix
step  21 :   372 spairs of degree  11  --->    20204 x   45559 matrix
step  22 :   817 spairs of degree  12  --->    39307 x   75583 matrix
step  23 :  1381 spairs of degree  13  --->    70538 x  120021 matrix
step  24 :  2193 spairs of degree  14  --->   120252 x  184253 matrix
step  25 :  3935 spairs of degree  15  --->   201675 x  281619 matrix
step  26 :  7000 spairs of degree  16  --->   100299 x  145491 matrix
step  27 :     8 spairs of degree   7  --->      513 x    3470 matrix
step  28 :    36 spairs of degree   8  --->     1982 x    7822 matrix
step  29 :   132 spairs of degree   9  --->     4676 x   14699 matrix
step  30 :   366 spairs of degree  10  --->    10470 x   26287 matrix
step  31 :   665 spairs of degree  11  --->    21678 x   44764 matrix
step  32 :  1532 spairs of degree  12  --->    44459 x   76243 matrix
step  33 :  2987 spairs of degree  13  --->    81133 x  122071 matrix
step  34 :  4975 spairs of degree  14  --->   136531 x  186846 matrix
step  35 :  6783 spairs of degree  15  --->   218341 x  278428 matrix
step  36 :  7001 spairs of degree  16  --->   111725 x  148467 matrix
step  37 :  2676 spairs of degree  16  --->   340281 x  413336 matrix
step  38 :  7000 spairs of degree  17  --->    94779 x  125788 matrix
step  39 :   596 spairs of degree   8  --->     4347 x   10760 matrix
step  40 :  2844 spairs of degree   9  --->    12129 x   18317 matrix
step  41 :  7000 spairs of degree  10  --->    22015 x   21320 matrix
step  42 :  7003 spairs of degree  10  --->    21784 x   18554 matrix
step  43 :  5528 spairs of degree  10  --->    31960 x   30359 matrix
step  44 :  7002 spairs of degree  11  --->    14968 x   11695 matrix
step  45 :  7000 spairs of degree  11  --->    14791 x   11483 matrix
step  46 :  5577 spairs of degree  11  --->    47015 x   45391 matrix
step  47 :   859 spairs of degree  12  --->    67952 x   71160 matrix
step  48 :  1571 spairs of degree  13  --->   105877 x  108422 matrix
step  49 :  2611 spairs of degree  14  --->    94876 x   96630 matrix
step  50 :  4688 spairs of degree  15  --->   145033 x  145146 matrix
step  51 :  7000 spairs of degree  16  --->    57837 x   55915 matrix
step  52 :  1604 spairs of degree  16  --->   221420 x  224033 matrix
step  53 :  7000 spairs of degree  17  --->    12615 x   11266 matrix
step  54 :  3040 spairs of degree   8  --->     9363 x   11031 matrix
step  55 :  4854 spairs of degree   9  --->    13844 x   12484 matrix
step  56 :  2471 spairs of degree  10  --->     7877 x    8901 matrix
step  57 :    13 spairs of degree  11  --->     5010 x    8499 matrix
step  58 :    55 spairs of degree  12  --->     5291 x    8765 matrix
step  59 :   293 spairs of degree  13  --->     5553 x    8865 matrix
step  60 :   615 spairs of degree  14  --->     6063 x    9079 matrix
step  61 :   276 spairs of degree  15  --->     5839 x    9194 matrix
step  62 :   229 spairs of degree  16  --->     5854 x    9256 matrix
step  63 :  2296 spairs of degree  17  --->     9962 x   11267 matrix
step  64 :  7003 spairs of degree  18  --->    14519 x   11010 matrix
step  65 :  2436 spairs of degree  18  --->    10431 x   11489 matrix
---------------------------------------------------------------------------
Time for updating pairs ...             345.358 sec
Time for symbolic preprocessing ...     585.545 sec
Time for sorting of columns ...           1.881 sec
Time for constructing matrices ...       99.815 sec
Time for linear algebra ...            3380.293 sec
---------------------------------------------------------------------------
Computation completed ...              4313.080 sec
---------------------------------------------------------------------------
Size of basis                          5690
Number of zero reductions            121863
---------------------------------------------------------------------------
