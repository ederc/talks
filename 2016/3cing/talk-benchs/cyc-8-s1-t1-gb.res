---------------------------------------------------------------------------
----------------------------- Computing Groebner --------------------------
--------------------- with the following options set ----------------------
---------------------------------------------------------------------------
number of threads                         1
hash table size                      524288 (2^19)
compute reduced basis?                    0
do not reduce A|B in gbla                 0
limit for handling spairs??            2500
use simplify?                             1
generate pbm files?                       0
print resulting basis?                    0
---------------------------------------------------------------------------
Loading input data ...                    0.000 sec
---------------------------------------------------------------------------
Data for tests/cyclic/affine/8
---------------------------------------------------------------------------
field characteristic                  65521
monomial order                          DRL
number of variables                       8
number of generators                      8
homogeneous input?                        0
homogeneous computation?                  0
input file size                           0.72 KB
---------------------------------------------------------------------------
step   1 :     1 spairs of degree   1  --->        1 x       8 matrix
step   2 :     1 spairs of degree   2  --->        3 x      19 matrix
step   3 :     1 spairs of degree   3  --->        6 x      39 matrix
step   4 :     2 spairs of degree   4  --->       16 x     111 matrix
step   5 :     4 spairs of degree   5  --->       51 x     233 matrix
step   6 :     9 spairs of degree   6  --->       91 x     420 matrix
step   7 :    22 spairs of degree   7  --->      243 x     831 matrix
step   8 :    47 spairs of degree   8  --->      511 x    1418 matrix
step   9 :   105 spairs of degree   9  --->      918 x    2139 matrix
step  10 :   233 spairs of degree  10  --->     1617 x    3000 matrix
step  11 :   416 spairs of degree  11  --->     2168 x    3512 matrix
step  12 :   618 spairs of degree  12  --->     3380 x    4617 matrix
step  13 :     3 spairs of degree   5  --->       48 x     293 matrix
step  14 :    10 spairs of degree   6  --->      169 x     623 matrix
step  15 :    26 spairs of degree   7  --->      410 x    1120 matrix
step  16 :    55 spairs of degree   8  --->      704 x    1703 matrix
step  17 :    98 spairs of degree   9  --->     1158 x    2423 matrix
step  18 :   129 spairs of degree  10  --->     1815 x    3295 matrix
step  19 :   197 spairs of degree  11  --->     2982 x    4637 matrix
step  20 :   296 spairs of degree  12  --->     3055 x    4366 matrix
step  21 :   537 spairs of degree  13  --->     4482 x    5628 matrix
step  22 :   124 spairs of degree   6  --->      494 x     799 matrix
step  23 :   605 spairs of degree   7  --->     1317 x    1044 matrix
step  24 :   157 spairs of degree   8  --->     1201 x    1373 matrix
step  25 :    75 spairs of degree   9  --->     1445 x    1704 matrix
step  26 :   122 spairs of degree  10  --->     1836 x    2072 matrix
step  27 :   209 spairs of degree  11  --->     1856 x    2008 matrix
step  28 :   406 spairs of degree  12  --->     2302 x    2314 matrix
step  29 :   616 spairs of degree  13  --->     2265 x    2097 matrix
step  30 :   254 spairs of degree   6  --->      689 x     707 matrix
step  31 :    63 spairs of degree   7  --->      486 x     689 matrix
step  32 :     6 spairs of degree   8  --->       28 x     281 matrix
step  33 :     6 spairs of degree   9  --->      256 x     507 matrix
step  34 :    14 spairs of degree  10  --->      263 x     521 matrix
step  35 :    32 spairs of degree  11  --->      406 x     659 matrix
step  36 :    52 spairs of degree  12  --->      454 x     697 matrix
step  37 :    33 spairs of degree  13  --->      446 x     713 matrix
step  38 :   324 spairs of degree  14  --->      893 x     867 matrix
step  39 :     5 spairs of degree  15  --->      676 x     982 matrix
---------------------------------------------------------------------------
Time for updating pairs ...               0.356 sec
Time for symbolic preprocessing ...       0.408 sec
Time for sorting of columns ...           0.012 sec
Time for constructing matrices ...        0.539 sec
Time for linear algebra ...               2.428 sec
---------------------------------------------------------------------------
Computation completed ...                 3.204 sec
---------------------------------------------------------------------------
Size of basis                           372
Number of zero reductions              4402
---------------------------------------------------------------------------
