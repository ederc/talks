git commit: 20b3d03dd95cf356487549de61117ad57ec90e96
---------------------------------------------------------------------------
----------------------------- Computing Groebner --------------------------
--------------------- with the following options set ----------------------
---------------------------------------------------------------------------
number of threads                        12
hash table size                     4194304 (2^22)
compute reduced basis?                    0
do not reduce A|B in gbla                 0
limit for handling spairs??            2500
use simplify?                             1
generate pbm files?                       0
print resulting basis?                    0
---------------------------------------------------------------------------
Loading input data ...                    0.000 sec
---------------------------------------------------------------------------
Data for tests/cyclic/affine/9
---------------------------------------------------------------------------
field characteristic                  65521
monomial order                          DRL
number of variables                       9
number of generators                      9
homogeneous input?                        0
homogeneous computation?                  0
input file size                           1.02 KB
---------------------------------------------------------------------------
step   1 :     1 spairs of degree   1  --->        1 x       9 matrix
step   2 :     1 spairs of degree   2  --->        3 x      22 matrix
step   3 :     1 spairs of degree   3  --->        6 x      47 matrix
step   4 :     2 spairs of degree   4  --->       17 x     156 matrix
step   5 :     4 spairs of degree   5  --->       61 x     360 matrix
step   6 :     9 spairs of degree   6  --->      108 x     717 matrix
step   7 :    22 spairs of degree   7  --->      334 x    1591 matrix
step   8 :    47 spairs of degree   8  --->      852 x    3123 matrix
step   9 :   106 spairs of degree   9  --->     1654 x    5166 matrix
step  10 :   255 spairs of degree  10  --->     3292 x    8156 matrix
step  11 :   568 spairs of degree  11  --->     5615 x   11915 matrix
step  12 :  1042 spairs of degree  12  --->     9778 x   17719 matrix
step  13 :  1918 spairs of degree  13  --->    17700 x   27247 matrix
step  14 :  2500 spairs of degree  14  --->    14925 x   21752 matrix
step  15 :     4 spairs of degree   6  --->      118 x     859 matrix
step  16 :    17 spairs of degree   7  --->      473 x    1977 matrix
step  17 :    46 spairs of degree   8  --->     1183 x    3646 matrix
step  18 :   111 spairs of degree   9  --->     2220 x    6009 matrix
step  19 :   259 spairs of degree  10  --->     4217 x    9493 matrix
step  20 :   435 spairs of degree  11  --->     7869 x   14987 matrix
step  21 :   718 spairs of degree  12  --->    13604 x   22323 matrix
step  22 :  1102 spairs of degree  13  --->    23187 x   33633 matrix
step  23 :  1583 spairs of degree  14  --->    39782 x   51987 matrix
step  24 :  2500 spairs of degree  15  --->    21614 x   28092 matrix
step  25 :   291 spairs of degree   7  --->     1454 x    2967 matrix
step  26 :  1503 spairs of degree   8  --->     4049 x    4451 matrix
step  27 :  2501 spairs of degree   9  --->     4755 x    3361 matrix
step  28 :  2414 spairs of degree   9  --->     7674 x    6450 matrix
step  29 :   341 spairs of degree  10  --->     7917 x    8830 matrix
step  30 :   400 spairs of degree  11  --->    10705 x   11565 matrix
step  31 :   524 spairs of degree  12  --->    13248 x   14097 matrix
step  32 :  1009 spairs of degree  13  --->    16872 x   17374 matrix
step  33 :  1911 spairs of degree  14  --->    25875 x   25657 matrix
step  34 :  2500 spairs of degree  15  --->     6908 x    5989 matrix
step  35 :  1261 spairs of degree   7  --->     3093 x    2795 matrix
step  36 :   898 spairs of degree   8  --->     2510 x    2434 matrix
step  37 :    31 spairs of degree   9  --->      244 x    1016 matrix
step  38 :    28 spairs of degree  10  --->     1194 x    1975 matrix
step  39 :    27 spairs of degree  11  --->     1180 x    1986 matrix
step  40 :    88 spairs of degree  12  --->     1264 x    2064 matrix
step  41 :   201 spairs of degree  13  --->     1398 x    2181 matrix
step  42 :   595 spairs of degree  14  --->     2043 x    2428 matrix
step  43 :   573 spairs of degree  15  --->    15071 x   15490 matrix
step  44 :   813 spairs of degree  16  --->     2450 x    2477 matrix
---------------------------------------------------------------------------
Time for updating pairs ...              11.877 sec
Time for symbolic preprocessing ...      15.774 sec
Time for sorting of columns ...           0.183 sec
Time for constructing matrices ...        3.897 sec
Time for linear algebra ...              35.744 sec
---------------------------------------------------------------------------
Computation completed ...                63.578 sec
---------------------------------------------------------------------------
Size of basis                          1344
Number of zero reductions             23946
---------------------------------------------------------------------------
