git commit: 20b3d03dd95cf356487549de61117ad57ec90e96
---------------------------------------------------------------------------
----------------------------- Computing Groebner --------------------------
--------------------- with the following options set ----------------------
---------------------------------------------------------------------------
number of threads                        12
hash table size                    16777216 (2^24)
compute reduced basis?                    0
do not reduce A|B in gbla                 0
limit for handling spairs??               0
use simplify?                             1
generate pbm files?                       0
print resulting basis?                    0
---------------------------------------------------------------------------
Loading input data ...                    0.000 sec
---------------------------------------------------------------------------
Data for tests/katsura/affine/13
---------------------------------------------------------------------------
field characteristic                  65521
monomial order                          DRL
number of variables                      13
number of generators                     13
homogeneous input?                        0
homogeneous computation?                  0
input file size                           1.11 KB
---------------------------------------------------------------------------
step   1 :     1 spairs of degree   1  --->        1 x      14 matrix
step   2 :    12 spairs of degree   2  --->       26 x     105 matrix
step   3 :    16 spairs of degree   3  --->      150 x     448 matrix
step   4 :   104 spairs of degree   4  --->      747 x    1499 matrix
step   5 :   557 spairs of degree   5  --->     3562 x    4765 matrix
step   6 :  1688 spairs of degree   6  --->    11564 x   12720 matrix
step   7 :  3424 spairs of degree   7  --->    27065 x   27405 matrix
step   8 :  4882 spairs of degree   8  --->    44967 x   44343 matrix
step   9 :  4962 spairs of degree   9  --->    56934 x   56318 matrix
step  10 :  3585 spairs of degree  10  --->    61255 x   61917 matrix
step  11 :  1805 spairs of degree  11  --->    55368 x   57712 matrix
step  12 :   604 spairs of degree  12  --->    50482 x   53984 matrix
step  13 :   121 spairs of degree  13  --->    45215 x   49190 matrix
step  14 :    11 spairs of degree  14  --->    43600 x   47684 matrix
---------------------------------------------------------------------------
Time for updating pairs ...               2.043 sec
Time for symbolic preprocessing ...      10.376 sec
Time for sorting of columns ...           0.272 sec
Time for constructing matrices ...        2.509 sec
Time for linear algebra ...              21.927 sec
---------------------------------------------------------------------------
Computation completed ...                34.619 sec
---------------------------------------------------------------------------
Size of basis                          2091
Number of zero reductions             19681
---------------------------------------------------------------------------
