git commit: 20b3d03dd95cf356487549de61117ad57ec90e96
---------------------------------------------------------------------------
----------------------------- Computing Groebner --------------------------
--------------------- with the following options set ----------------------
---------------------------------------------------------------------------
number of threads                         2
hash table size                    16777216 (2^24)
compute reduced basis?                    0
do not reduce A|B in gbla                 0
limit for handling spairs??               0
use simplify?                             1
generate pbm files?                       0
print resulting basis?                    0
---------------------------------------------------------------------------
Loading input data ...                    0.000 sec
---------------------------------------------------------------------------
Data for tests/katsura/affine/15
---------------------------------------------------------------------------
field characteristic                  65521
monomial order                          DRL
number of variables                      15
number of generators                     15
homogeneous input?                        0
homogeneous computation?                  0
input file size                           1.50 KB
---------------------------------------------------------------------------
step   1 :     1 spairs of degree   1  --->        1 x      16 matrix
step   2 :    14 spairs of degree   2  --->       30 x     136 matrix
step   3 :    19 spairs of degree   3  --->      203 x     672 matrix
step   4 :   140 spairs of degree   4  --->     1136 x    2549 matrix
step   5 :   914 spairs of degree   5  --->     6545 x    9407 matrix
step   6 :  3408 spairs of degree   6  --->    25656 x   29453 matrix
step   7 :  8657 spairs of degree   7  --->    73934 x   76477 matrix
step   8 : 15874 spairs of degree   8  --->   161590 x  160343 matrix
step   9 : 21528 spairs of degree   9  --->   267200 x  262300 matrix
step  10 : 21813 spairs of degree  10  --->   355455 x  350842 matrix
step  11 : 16511 spairs of degree  11  --->   426671 x  427152 matrix
step  12 :  9229 spairs of degree  12  --->   425276 x  432701 matrix
step  13 :  3706 spairs of degree  13  --->   386016 x  398770 matrix
step  14 :  1013 spairs of degree  14  --->   339176 x  354559 matrix
step  15 :   169 spairs of degree  15  --->   311864 x  328079 matrix
step  16 :    13 spairs of degree  16  --->   299211 x  315581 matrix
---------------------------------------------------------------------------
Time for updating pairs ...              24.265 sec
Time for symbolic preprocessing ...     220.710 sec
Time for sorting of columns ...           2.026 sec
Time for constructing matrices ...      255.225 sec
Time for linear algebra ...            4385.783 sec
---------------------------------------------------------------------------
Computation completed ...              4632.785 sec
---------------------------------------------------------------------------
Size of basis                          8269
Number of zero reductions             94740
---------------------------------------------------------------------------
