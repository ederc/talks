---------------------------------------------------------------------------
----------------------------- Computing Groebner --------------------------
--------------------- with the following options set ----------------------
---------------------------------------------------------------------------
number of threads                         1
hash table size                      524288 (2^19)
compute reduced basis?                    0
do not reduce A|B in gbla                 0
limit for handling spairs??            2500
use simplify?                             1
generate pbm files?                       0
print resulting basis?                    0
---------------------------------------------------------------------------
Loading input data ...                    0.000 sec
---------------------------------------------------------------------------
Data for tests/cyclic/affine/7
---------------------------------------------------------------------------
field characteristic                  65521
monomial order                          DRL
number of variables                       7
number of generators                      7
homogeneous input?                        0
homogeneous computation?                  0
input file size                           0.49 KB
---------------------------------------------------------------------------
step   1 :     1 spairs of degree   1  --->        1 x       7 matrix
step   2 :     1 spairs of degree   2  --->        3 x      16 matrix
step   3 :     1 spairs of degree   3  --->        6 x      31 matrix
step   4 :     2 spairs of degree   4  --->       15 x      75 matrix
step   5 :     4 spairs of degree   5  --->       41 x     142 matrix
step   6 :     9 spairs of degree   6  --->       77 x     243 matrix
step   7 :    22 spairs of degree   7  --->      159 x     404 matrix
step   8 :    47 spairs of degree   8  --->      282 x     582 matrix
step   9 :    98 spairs of degree   9  --->      477 x     788 matrix
step  10 :   164 spairs of degree  10  --->      746 x     990 matrix
step  11 :   229 spairs of degree  11  --->      872 x    1048 matrix
step  12 :     4 spairs of degree   5  --->       34 x     160 matrix
step  13 :    11 spairs of degree   6  --->      104 x     299 matrix
step  14 :    25 spairs of degree   7  --->      208 x     457 matrix
step  15 :    49 spairs of degree   8  --->      412 x     685 matrix
step  16 :    58 spairs of degree   9  --->      472 x     760 matrix
step  17 :    91 spairs of degree  10  --->      682 x     958 matrix
step  18 :   121 spairs of degree  11  --->      689 x     872 matrix
step  19 :   158 spairs of degree  12  --->      702 x     861 matrix
step  20 :    89 spairs of degree   6  --->      304 x     382 matrix
step  21 :   121 spairs of degree   7  --->      393 x     405 matrix
step  22 :    26 spairs of degree   8  --->      287 x     398 matrix
step  23 :    49 spairs of degree   9  --->      447 x     544 matrix
step  24 :    69 spairs of degree  10  --->      303 x     395 matrix
step  25 :   103 spairs of degree  11  --->      333 x     410 matrix
step  26 :   115 spairs of degree  12  --->      362 x     414 matrix
step  27 :   175 spairs of degree  13  --->      440 x     397 matrix
---------------------------------------------------------------------------
Time for updating pairs ...               0.058 sec
Time for symbolic preprocessing ...       0.043 sec
Time for sorting of columns ...           0.003 sec
Time for constructing matrices ...        0.052 sec
Time for linear algebra ...               0.221 sec
---------------------------------------------------------------------------
Computation completed ...                 0.325 sec
---------------------------------------------------------------------------
Size of basis                           209
Number of zero reductions              1257
---------------------------------------------------------------------------
