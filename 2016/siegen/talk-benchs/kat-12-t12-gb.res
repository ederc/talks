git commit: 20b3d03dd95cf356487549de61117ad57ec90e96
---------------------------------------------------------------------------
----------------------------- Computing Groebner --------------------------
--------------------- with the following options set ----------------------
---------------------------------------------------------------------------
number of threads                        12
hash table size                    16777216 (2^24)
compute reduced basis?                    0
do not reduce A|B in gbla                 0
limit for handling spairs??               0
use simplify?                             1
generate pbm files?                       0
print resulting basis?                    0
---------------------------------------------------------------------------
Loading input data ...                    0.000 sec
---------------------------------------------------------------------------
Data for tests/katsura/affine/12
---------------------------------------------------------------------------
field characteristic                  65521
monomial order                          DRL
number of variables                      12
number of generators                     12
homogeneous input?                        0
homogeneous computation?                  0
input file size                           0.94 KB
---------------------------------------------------------------------------
step   1 :     1 spairs of degree   1  --->        1 x      13 matrix
step   2 :    11 spairs of degree   2  --->       24 x      91 matrix
step   3 :    14 spairs of degree   3  --->      126 x     357 matrix
step   4 :    85 spairs of degree   4  --->      581 x    1107 matrix
step   5 :   407 spairs of degree   5  --->     2529 x    3269 matrix
step   6 :  1106 spairs of degree   6  --->     7177 x    7767 matrix
step   7 :  1983 spairs of degree   7  --->    14298 x   14382 matrix
step   8 :  2436 spairs of degree   8  --->    20338 x   20092 matrix
step   9 :  2064 spairs of degree   9  --->    23776 x   23867 matrix
step  10 :  1191 spairs of degree  10  --->    22293 x   23193 matrix
step  11 :   449 spairs of degree  11  --->    19126 x   20734 matrix
step  12 :   100 spairs of degree  12  --->    17226 x   19174 matrix
step  13 :    10 spairs of degree  13  --->    16530 x   18567 matrix
---------------------------------------------------------------------------
Time for updating pairs ...               0.666 sec
Time for symbolic preprocessing ...       2.501 sec
Time for sorting of columns ...           0.095 sec
Time for constructing matrices ...        1.007 sec
Time for linear algebra ...               5.131 sec
---------------------------------------------------------------------------
Computation completed ...                 8.394 sec
---------------------------------------------------------------------------
Size of basis                          1050
Number of zero reductions              8807
---------------------------------------------------------------------------
