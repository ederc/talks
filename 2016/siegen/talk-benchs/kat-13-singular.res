                     SINGULAR                                 /  Development
 A Computer Algebra System for Polynomial Computations       /   version 4.0.3
                                                           0<
 by: W. Decker, G.-M. Greuel, G. Pfister, H. Schoenemann     \   Jan 2016
FB Mathematik der Universitaet, D-67653 Kaiserslautern        \
// ** loaded /scratch/hannes/snork.new/Singular/LIB/poly.lib (4.0.0.0,Jun_2013)
// ** loaded /scratch/hannes/snork.new/Singular/LIB/ring.lib (4.0.2.2,Jan_2016)
// ** loaded /scratch/hannes/snork.new/Singular/LIB/primdec.lib (4.0.2.0,Apr_2015)
// ** loaded /scratch/hannes/snork.new/Singular/LIB/absfact.lib (4.0.0.0,Jun_2013)
// ** loaded /scratch/hannes/snork.new/Singular/LIB/triang.lib (4.0.0.0,Jun_2013)
// ** loaded /scratch/hannes/snork.new/Singular/LIB/matrix.lib (4.0.0.0,Jun_2013)
// ** loaded /scratch/hannes/snork.new/Singular/LIB/nctools.lib (4.0.3.3,Sep_2016)
// ** loaded /scratch/hannes/snork.new/Singular/LIB/random.lib (4.0.0.0,Jun_2013)
// ** loaded /scratch/hannes/snork.new/Singular/LIB/elim.lib (4.0.0.1,Jan_2014)
// ** loaded /scratch/hannes/snork.new/Singular/LIB/inout.lib (4.0.0.0,Jun_2013)
// ** loaded /scratch/hannes/snork.new/Singular/LIB/general.lib (4.0.0.1,Jan_2014)
5291310

$Bye.
