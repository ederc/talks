git commit: 20b3d03dd95cf356487549de61117ad57ec90e96
---------------------------------------------------------------------------
----------------------------- Computing Groebner --------------------------
--------------------- with the following options set ----------------------
---------------------------------------------------------------------------
number of threads                         2
hash table size                    16777216 (2^24)
compute reduced basis?                    0
do not reduce A|B in gbla                 0
limit for handling spairs??               0
use simplify?                             1
generate pbm files?                       0
print resulting basis?                    0
---------------------------------------------------------------------------
Loading input data ...                    0.000 sec
---------------------------------------------------------------------------
Data for tests/katsura/affine/14
---------------------------------------------------------------------------
field characteristic                  65521
monomial order                          DRL
number of variables                      14
number of generators                     14
homogeneous input?                        0
homogeneous computation?                  0
input file size                           1.30 KB
---------------------------------------------------------------------------
step   1 :     1 spairs of degree   1  --->        1 x      15 matrix
step   2 :    13 spairs of degree   2  --->       28 x     120 matrix
step   3 :    17 spairs of degree   3  --->      175 x     552 matrix
step   4 :   118 spairs of degree   4  --->      917 x    1961 matrix
step   5 :   703 spairs of degree   5  --->     4811 x    6716 matrix
step   6 :  2399 spairs of degree   6  --->    17106 x   19302 matrix
step   7 :  5528 spairs of degree   7  --->    44539 x   45615 matrix
step   8 :  9053 spairs of degree   8  --->    85115 x   84084 matrix
step   9 : 10758 spairs of degree   9  --->   122017 x  119864 matrix
step  10 :  9339 spairs of degree  10  --->   152155 x  151410 matrix
step  11 :  5885 spairs of degree  11  --->   156746 x  159258 matrix
step  12 :  2629 spairs of degree  12  --->   139117 x  144744 matrix
step  13 :   791 spairs of degree  13  --->   124373 x  131785 matrix
step  14 :   144 spairs of degree  14  --->   116620 x  124668 matrix
step  15 :    12 spairs of degree  15  --->   111640 x  119819 matrix
---------------------------------------------------------------------------
Time for updating pairs ...               6.169 sec
Time for symbolic preprocessing ...      45.890 sec
Time for sorting of columns ...           0.715 sec
Time for constructing matrices ...       45.486 sec
Time for linear algebra ...             526.865 sec
---------------------------------------------------------------------------
Computation completed ...               579.640 sec
---------------------------------------------------------------------------
Size of basis                          4140
Number of zero reductions             43250
---------------------------------------------------------------------------
