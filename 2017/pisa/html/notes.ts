PROJECTIVE SPECIAL LINEAR GROUP:
PSL(V) = SL(V)/SZ(V)
SL = special linear group: group of n x n matrices A with det A=1
SZ = scalar transofrmations with det = 1

IDEAL CLASS GROUP:
J_A / P_A
A Dedekind ring with quotient field K
J_A group of fractional ideals 
P_A subgroup of principal fractional ideals
maximal order = set of algebraic integers in K
measure how far the ring of integers of an algebraic number field is away from having a unique prime factorization
lll only for shorter basis vectors of maximal_order of K

PARAMETRIZATION RATIONAL CURVES:
rational curve = genus(f) = 0
rational parametrization
f1 = p1/q1, f2=p2/q2, p1,p2,q1,q2 \in K[T], q1,q2 not zero
C=V(f), f \in K[x,y] => F(f1(T),f2(T)) = 0
adjungierte kurve: A mit V(A) = Sing(C)

SURFACES:
alg. surface of general type = surface with kodaira dim = 2
(K_X) canonical divisor class

FEYNMAN DIAGRAMS:
pictorial representation of the mathematical expressions describing the
behaviour of subatomic particles
