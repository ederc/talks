-------------------------------
IMAGINARY - open mathematics

This picture has been downloaded from:
www.imaginary.org



The following license regulations apply:

- The license of this picture is as given at the download page!
- All authors have to be mentioned for any use of this picture! 
(you can find the authors also in the filename of the picture)
- Please report back to us if you use the picture at any event, exhibition or in a museum installation. 
We would be happy to feature this on the IMAGINARY platform.


IMAGINARY is a project by the Mathematisches Forschungsinstitut Oberwolfach (www.mfo.de)
supported by the Klaus Tschira Stiftung (http://klaus-tschira-stiftung.de).


Contact:

info@imaginary.org
www.imaginary.org
