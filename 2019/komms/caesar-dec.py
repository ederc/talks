#!/usr/bin/env python3
import string

def caesar(plaintext, shift, alphabet=string.ascii_lowercase):
    shifted_alphabet = alphabet[shift:] + alphabet[:shift]
    return plaintext.translate(plaintext.maketrans(alphabet, shifted_alphabet))

message = input("Geben Sie die verschlüsselte Nachricht ein: ")
for shift in range(1,26):
    print(caesar(message.lower(), -shift), " ( Verschiebung ist ", shift, ")")
