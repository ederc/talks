#!/usr/bin/env python3
import string

def caesar(plaintext, shift, alphabet=string.ascii_lowercase):
    shifted_alphabet = alphabet[shift:] + alphabet[:shift]
    return plaintext.translate(plaintext.maketrans(alphabet, shifted_alphabet))

message = input("Geben Sie die zu verschlüsselnde Nachricht ein: ")
shift = int(input("Geben Sie die Verschiebung ein: "))
print(caesar(message.lower(), shift))
