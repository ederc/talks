\beamer@endinputifotherversion {3.07pt}
\beamer@sectionintoc {1}{Gr\"obner bases}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{The problem of zero reductions}{3}{0}{1}
\beamer@sectionintoc {2}{Signature-based algorithms}{9}{0}{2}
\beamer@subsectionintoc {2}{1}{The basic idea}{10}{0}{2}
\beamer@subsectionintoc {2}{2}{Computing Gr\"obner bases using signatures}{17}{0}{2}
\beamer@subsectionintoc {2}{3}{How to reject useless pairs?}{30}{0}{2}
\beamer@sectionintoc {3}{G2V and F5 -- Differences and similarities}{34}{0}{3}
\beamer@subsectionintoc {3}{1}{Implementations of the criteria}{35}{0}{3}
\beamer@subsectionintoc {3}{2}{F5E -- Combine the ideas}{39}{0}{3}
\beamer@subsectionintoc {3}{3}{Implementations of the sig-safe reductions}{40}{0}{3}
\beamer@sectionintoc {4}{Experimental results}{41}{0}{4}
\beamer@subsectionintoc {4}{1}{Experimental results}{42}{0}{4}
\beamer@sectionintoc {5}{Outlook}{43}{0}{5}
