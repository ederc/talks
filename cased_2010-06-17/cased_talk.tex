\documentclass[]{beamer}
\usetheme{Pittsburgh}

%\usepackage{beamerthemesplit}
\usepackage{framed}
\usepackage{enumerate}
\usepackage{array}
\usepackage{amsxtra}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{latexsym}
\newtheorem {thm}{Theorem}[section]
\newtheorem {prop}[thm]{Proposition}
\newtheorem {lem}[thm]{Lemma}
\newtheorem {cor}[thm]{Corollary}
\newtheorem {con}[thm]{Conjecture}
\theoremstyle {definition}
\newtheorem {defi}[thm]{Definition}
\theoremstyle {remark}
\newtheorem {rem}[thm]{Remark}
\newtheorem {nota}[thm]{Notation}
\newtheorem {remdef}[thm]{Remark and Definition}
\newtheorem {construction}[thm]{Construction}
\newtheorem {convention}[thm]{Convention}
\newtheorem*{acknowledgement*}{Acknowledgement}
\hyphenation {Kai-sers-lau-tern}
\hyphenation {po-ly-no-mi-al}

\newcommand{\etalchar}[1]{$^{#1}$}
\newcommand\A{\ensuremath{\newline\textbf{Alice: }}}
\newcommand\B{\ensuremath{\newline\textbf{Bob: }}}

\newcommand\lot{\ensuremath{\mathrm{lot}}}
\newcommand\htt{\ensuremath{\mathrm{ht}}}
\newcommand\lc{\ensuremath{\mathrm{lc}}}
\newcommand\lm{\ensuremath{\mathrm{lm}}}
\newcommand\mhm{\ensuremath{\mathrm{mhm}}}
\newcommand\MHTMTM{\ensuremath{\mathrm{MHT}_{\mathrm{\tiny{M}}}}}
\newcommand\MHTF{\ensuremath{\mathrm{MHT}_{\mathrm{\tiny{F}}}}}

\newcommand\crit{\ensuremath{\mathrm{crit}}}
\newcommand\rew{\ensuremath{\mathrm{rew}}}
\newcommand\red{\ensuremath{\mathrm{red}}}
\newcommand\spp{\ensuremath{\mathrm{sp}}}
\newcommand\new{\ensuremath{\mathrm{new}}}
\newcommand\prev{\ensuremath{\mathrm{prev}}}
\newcommand\me{\ensuremath{\mathbf{e}}}
\newcommand\mg{\ensuremath{\mathbf{g}}}
\newcommand\ms{\ensuremath{\mathbf{s}}}
\newcommand\ind{\ensuremath{\mathrm{index}}}
\newcommand\poly{\ensuremath{\mathrm{poly}}}
\newcommand\Spol{\ensuremath{\mathrm{Spol}}}
\newcommand\Sig{\ensuremath{\mathcal{S}}}
\newcommand\Syz{\ensuremath{\mathrm{Syz}}}
\newcommand\PSyz{\ensuremath{\mathrm{PSyz}}}
\newcommand\Rt{\ensuremath{R_\mathrm{top}}}
\newcommand\Kx{\ensuremath{\mathbb{K}[\underline{x}]}}
\newcommand\Kxm{\ensuremath{\mathbb{K}[\underline{x}]^m}}
\newcommand\KxnG{\ensuremath{\mathbb{K}[\underline{x}]^{n_G}}}
\newcommand\neqf{\ensuremath{\neq_{\mathrm{F}}}}
\newcommand\eqf{\ensuremath{=_{\mathrm{F}}}}
\newcommand\preceqf{\ensuremath{\preceq_{\mathrm{F}}}}
\newcommand\succeqf{\ensuremath{\succeq_{\mathrm{F}}}}
\newcommand\precf{\ensuremath{\prec_{\mathrm{F}}}}
\newcommand\succf{\ensuremath{\succ_{\mathrm{F}}}}
\newcommand\preceqm{\ensuremath{\preceq_{\mathrm{\tiny{M}}}}}
\newcommand\precm{\ensuremath{\prec_{\mathrm{\tiny{M}}}}}
\newcommand\lcm{\ensuremath{\mathrm{lcm}}}
\newcommand\GCD{\ensuremath{\mathrm{GCD}}}
\beamertemplatenavigationsymbolsempty
\setbeamertemplate{footline}[frame number]
\setbeamertemplate{headline}[default]
\title{Faug\`{e}re's F5 algorithm: variants and termination issues}
\author{Christian Eder \\ (joint work with Justin Gash and John Perry)}
\institute{University of Kaiserslautern}
\date{June 17, 2010}
%\usetheme[height=8mm]{Rochester}
\useinnertheme{default}
\useoutertheme{default}
\begin{document}
\AtBeginSection[]
{
\begin{frame}<beamer>
\frametitle{The following section is about}
\tableofcontents[currentsection,hideothersubsections]
\end{frame}
}
\frame{\titlepage}

\section*{What is this talk about?}
\begin{frame}
\frametitle{What is this talk all about?}
\begin{enumerate}
\onslide<+->
\item Efficient computations of Gr\"obner bases using Faug\`{e}re's F5 Algorithm and variants of it
\item Explanation of the F5 Algorithm, its criteria used to detect useless
s-polynomials
\item Presentation of the variant F5C which reduce some inefficiencies of F5
\item Explanation and solution of the termination issue of F5
\end{enumerate}
\end{frame}

\section{Introducing Gr\"obner bases}
\subsection{Gr\"obner basics}
\begin{frame}
\frametitle{Basic problem}
\begin{enumerate}
\onslide<+->
\item Given a ring $R$ and an ideal $I \lhd R$ we want to compute a \textbf{Gr\"obner basis $G$ of $I$}. \\
\item $G$ can be understood as a \textbf{nice representation for $I$}. Gr\"obner bases were discovered by Bruno Buchberger in 1965 \cite{buchberger}. 
    Having computed $G$ lots of \textbf{difficult questions} concerning $I$ are \textbf{easier to answer using $G$} instead of $I$.
\item This is due to some nice properties of Gr\"obner bases. The following is very useful to understand how to compute a Gr\"obner basis.
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Main properties of G\"obner bases}
\onslide<+->
\begin{lem}
$G=\{g_1,\dots,g_n\}$ is a Gr\"obner basis of an ideal $I=\langle
f_1,\dots,f_m\rangle$ iff $G \subset I$ and $\langle \lm(g_1),\dots,\lm(g_n)\rangle=\langle
\lm(f_1),\dots\lm(f_m)\rangle$.
\end{lem}
\onslide<+->
\begin{lem}
Let $G$ be a Gr\"obner basis of an ideal $I$. It holds that for all $p,q \in G$ it holds that
$$\Spol(p,q) \xrightarrow{G} 0,$$
where 
\begin{itemize}
\item $\Spol(p,q) = \lc(q)u_p p - \lc(p)u_q q$ and 
\item $u_k = \frac{\lcm\left(\lm(p),\lm(q)\right)}{\lm(k)}$.
\end{itemize}
\end{lem}
\end{frame}

\begin{frame}
\frametitle{A lovely example to get to know F5}
\onslide<1->
\begin{example}
Assume the ideal $I=\langle g_1,g_2 \rangle \lhd \mathbb{Q}[x,y,z]$ where
\color{black}$g_1 = xy-z^2$, $g_2 = y^2-z^2$; $x>y>z$.\\
Computing 
\begin{align*}
\Spol(g_2,g_1) &= xg_2 - yg_1 \\ 
               &= \mathbf{xy^2} - xz^2 - \mathbf{xy^2} +yz^2 \\
               &= -xz^2 + yz^2,
\end{align*}
we get a new element \color{black}$g_3 = xz^2-yz^2$. \\ 
\end{example}
\end{frame}

\subsection{Computation of Gr\"obner bases}
\begin{frame}
\frametitle{Computation of Gr\"obner bases}
\onslide<1->
The standard \textbf{Buchberger Algorithm} to compute $G$ follows easily from
the previously stated property of $G$:\\
{\bf Input:} Ideal $I=\langle f_1,\ldots,f_m \rangle$ \\
{\bf Output:} Gr\"obner basis $G$ of $I$
\begin{enumerate}
\item $G = \emptyset$
\item $G := G \cup \{f_i\}$ for all $i\in \{1,\ldots,m\}$
\item Set $P := \{\Spol(g_i,g_j) \mid g_i,g_j \in G, i > j \}$
\onslide<2->
\item Choose $p \in P$, $P := P \setminus \{p\}$
\begin{enumerate}
    \onslide<3->
\item[(a)] If $p \xrightarrow{G} 0 \Rightarrow$ \textbf{no new information} \\
    Go on with the next element in $P$.
\onslide<4->
\item[(b)] If $p \xrightarrow{G} h \neq 0 \Rightarrow$ \textbf{new information} \\
    Add $h$ to $G$. \\
    Build new s-polynomials with $h$ and add them to $P$.\\
    Go on with the next element in $P$.
\end{enumerate}
\item When $P= \emptyset$ we are done and $G$ is a Gr\"obner basis of $I$.
\end{enumerate}
\end{frame}

\begin{frame}    
\frametitle{Computing Gr\"obner bases incrementally}
\onslide<+->
A slightly variant of this algorithm is the following computing the Gr\"obner basis \textbf{incrementally}: \\
\onslide<+->
\textbf{Input: } Ideal $I=\langle f_1,\ldots,f_m \rangle$ \\
\textbf{Output:} Gr\"obner basis $G$ of $I$ \\
\begin{enumerate}
\onslide<+->
\item Compute Gr\"obner basis $G_1$ of $\langle f_1 \rangle$.
\onslide<+->
\item Compute Gr\"obner basis $G_2$ of $\langle f_1,f_2 \rangle$ by 
\begin{enumerate}
\item[(a)] $G_2 = G_1 \cup \{f_2\}$,
\item[(b)] computing s-polynomials of $f_2$ with elements of $G_1$
\item[(c)] reducing all s-polynomials w.r.t. $G_2$ and possibly add new elements to $G_2$
\end{enumerate}
\onslide<+->
\item \ldots
\item $G := G_m$ is the Gr\"obner basis of $I$
\end{enumerate}
%\onslide<+->
%\begin{block}{Remark}
%Note that from this point on $f_i = g_i$ is no longer true for all $i \in \{1,\ldots,m\}$. 
%\end{block}
\end{frame}

\subsection{Problem of zero reduction}
\begin{frame}
\frametitle{Problem of zero reduction}
\begin{exampleblock}{Lots of useless computations}
\onslide<+->
It is very time-consuming to compute $G$ such that $\Spol(p,q)$ \textbf{reduces to zero w.r.t. $G$} for all $p,q \in G$. \\
\onslide<+->
When such an s-polynomial reduces to an element $h \neq 0$ w.r.t. $G$ then we get \textbf{new information} for the structure of $G$, namely adding $h$ to $G$.\\
\onslide<+->
But most of the s-polynomials considered during the algorithm reduce to zero
w.r.t. $G$. \\
%\onslide<+->
\alert{$\Rightarrow$ No new information from zero reductions}
\end{exampleblock}
\onslide<+->
Let's have a look at the example again:
\end{frame}

\begin{frame}
\frametitle{An example of zero reduction}
\onslide<1->
\begin{example}
\color<3->{lightgray}
Given \color<3->{black}$g_1=xy-z^2$, $g_2=y^2-z^2$,\color<3->{lightgray} we have computed 
$$ \Spol(g_2,g_1) = \mathbf{xy^2} - xz^2 - \mathbf{xy^2} +yz^2 = -xz^2 +
yz^2.$$
\onslide<2->
We get a new element \color{black}$g_3 = xz^2-yz^2$ \color<3->{lightgray}for $G$. \\ 
\onslide<3->
\color{black}
Let us compute $\Spol(g_3,g_1)$ next: 
\onslide<4->
$$\Spol(g_3,g_1) = \mathbf{xyz^2} - y^2z^2 - \mathbf{xyz^2} + z^4 = -y^2z^2 + z^4.$$
\onslide<5->
Now we can reduce further with $z^2 g_2$: 
\[-y^2z^2 + z^4 + y^2z^2 - z^4 = 0.\]
\end{example}
\onslide<6->
\alert{$\Rightarrow$ How to detect zero reductions in advance?}     
\end{frame}

%\begin{frame}
%\frametitle{How to detect zero reductions in advance?}
%\onslide<1->
%There are different attempts to detect zero reductions: \\
%\begin{enumerate}
%\item Buchberger's criteria and the well-known implementation of Gebauer \& M\"oller \cite{gm}.
%\item<alert@2-> In 2002 \textbf{Faug\`{e}re} has published the \textbf{F5 Algorithm} \cite{f5}, a Gr\"obner basis algorithm which uses new criteria to detect such useless pairs.
%\end{enumerate}
%\onslide<2->
%$\Rightarrow$ In the following we need to understand how Faug\`{e}re's criteria work.
%\end{frame}

\section{The F5 Algorithm}
\subsection{F5 basics}
\begin{frame}
\frametitle{Signatures of polynomials}
\onslide<+->
Faug\`{e}re's idea is to give each polynomial during the computations of the algorithm a so-called \textbf{signature}: \\ 
\begin{enumerate}
\onslide<+->
\item Assuming a polynomial $p$ its signature is defined to be $\Sig(p) = (t,\ell)$ where $t$ is its monomial and $\ell \in \mathbb{N}$ is its index.
\item A generating element $f_i$ of $I$ gets the signature $\Sig(f_i) = (1,\mathrm{i})$. 
\item We have an \textbf{ordering $\prec$} on the signatures:
\begin{align*} 
(t_1,\ell_1) \succ (t_2,\ell_2) \quad \Leftrightarrow \quad &\textrm{(a)} \ell_1 > \ell_2 \textrm{ or}\\
                                                            &\textrm{(b)} \ell_1
                                                            = \ell_2 \textrm{
                                                            and } t_1 > t_2 
\end{align*}
\end{enumerate}
\onslide<+->
\begin{example}
Assume $\mathbb{Q}[x,y,z]$ with degree reverse lexicographical ordering. Then
\begin{enumerate}
\item $(x^2y,3) \succ (z^3,3)$,
\item $(1,5) \succ (x^{12}y^{234}z^{3456},4)$.
\end{enumerate}
\end{example}
\end{frame}
\begin{frame}
\frametitle{Signatures of polynomials}
\onslide<+->
\begin{rem}
    Note that there are other ways to define the ordering $\prec$ such that it prefers the degree of the monomial and not the index \cite{traverso}. \\
    Implementations of F5 with different orderings:
    \begin{enumerate}
      \item[(a)] 2009 Ars and Hashemi \cite{arshashemi}
      \item[(b)] 2010 Sun and Wang \cite{sunwang}
    \end{enumerate}
\end{rem}
\vspace*{5mm}
\onslide<+->
Using the signatures in the F5 Algorithm we also need to define them for s-polynomials: 
\[\Spol(p,q) = \lc(q) u_p p - \lc(p) u_q q \textrm{ where } \Sig\left(\Spol(p,q)\right) = u_p \Sig(p)\]
where we assume that $u_p \Sig(p) \succ u_q \Sig(q)$.
\end{frame}

\begin{frame}
\frametitle{Example revisited - with signatures}
\onslide<1->
\color<2->{lightgray}
In our example 
\begin{align*}
g_3 &= \Spol(g_2,g_1) = x g_2 - y g_1 \\ 
\onslide<+-> \Rightarrow \alert{\Sig(g_3)} &= x \Sig(g_2) = x(1,2) := \alert{(x,2)}.
\end{align*}
\onslide<2->
\color{black}
It follows that $\Spol(g_3,g_1) = y g_3 - z^2 g_1$ has 
\[\Sig\left(\Spol(g_3,g_1) \right) = y \Sig(g_3) = (xy,2).\]
\onslide<3->
Note that $\Sig\left(\Spol(g_3,g_1) \right) = (\alert{xy},2)$ and $\lm(g_1) = \alert{xy}$. \\
\onslide<4->
$\Rightarrow$ In F5 we \textbf{know} that $\Spol(g_3,g_1)$ will reduce to zero!
\end{frame}

\begin{frame}
\frametitle{How does this work?}
\onslide<2->
Let $\Spol(p,q) = \lc(p)u_p p - \lc(q)u_q q$ with $\Sig(p)=(s,k),
    \Sig(q)=(t,\ell)$.\\
Then $\Spol(p,q)$ does not need to be computed if
\begin{enumerate}
  \onslide<3->
  \item the leading monomial of some element $p\in
  G$ of index smaller $k$ ($\ell$) divides $u_p s$ ($u_q t$) 
{\bf (Faug\`ere's Criterion)}, or
  \onslide<4->
  \item the monomial of the signature of an element of
  index $k$ ($\ell$) divides $u_p s$ ($u_q t$). 
{\bf (Rewritten Criterion)}
\end{enumerate}
\onslide<5->
\begin{rem}
    \begin{enumerate}
      \item F5's criteria are based on the signatures.
      \item F5 computes degree-wise in each iteration step.
    \end{enumerate}
\end{rem}
\end{frame}
\subsection{Drawbacks of F5}
\begin{frame}
\frametitle{Difficulty of top-reduction in F5}
\onslide<+->
\textbf{On the one hand} adding signatures to polynomials makes it possible to use these powerful criteria, \\
\textbf{on the other hand} we have to keep track of the signatures, i.e. we must be very careful when reducing elements.
\onslide<+->
\begin{block}{Remark}
    We will see in the following example that we do not only need to be careful \textbf{if we are allowed to reduce an element}, but also must be able to generate \textbf{new polynomials during reduction} when \alert{reducing with elements generated in the current iteration step}.
\end{block}
\onslide<+->
\begin{example}
Assume the polynomial $p = xy^2-z^3$ with $\Sig(p) = (t_p,\ell)$ and a possible
reducer $q = y^2-xz$ with $\Sig(q) = (t_q,\ell)$. \\
\onslide<+->
In Buchberger-like implementations the top-reduction would take place, i.e. we
would compute  $p - x q$. 
\end{example}
\end{frame}
\begin{frame}
\frametitle{Difficulty of top-reduction in F5}
\begin{example}
In F5 the following can happen:
\onslide<+->
\begin{enumerate}
  \item If $x \Sig(q)$ satisfies Faug\`{e}re's Criterion $\Rightarrow$ \textbf{no reduction}!
\onslide<+->
\item If $x \Sig(q)$ satisfies the Rewritten Criterion $\Rightarrow$ \textbf{no reduction}!
\onslide<+->
\item None of the above cases holds and $x\Sig(q) \prec \Sig(p)$ $\Rightarrow$
    $p - x q$ is computed and gets the signature $\Sig(p)$.
\onslide<+->
\item None of the first two cases holds and $x \Sig(q) \succ \Sig(p)$: 
\onslide<+->
\begin{enumerate}
  \item[(a)] $p$ is \textbf{not reduced}, but searching for another possible reducer of it.
\onslide<+->
\item[(b)] A new \textbf{s-polynomial} $r := x q - p$ where $\Sig(r) =
    x\Sig(q)$ is computed.
\end{enumerate}
\end{enumerate}
\end{example}
\end{frame}

\begin{frame}
\frametitle{Redundant polynomials}
\onslide<+->
%If we do not top-reduce $g_i$ by $g_j$ or if we add the new element $g_{\new}$ (more precisely: the later on reduced $g_{\new}$) to $G$ our Gr\"obner basis will contain \textbf{redundant polynomials}. \\
\begin{example}
Assuming one of the first two cases of the previous example and moreover that there
exists no other top-reducer of $p$ we would end up with both, $p$ and $q$ being 
in $G$ whereas clearly $\lm(q) \mid \lm(p)$. \\
Thus $p$ is {\bf redundant} for $G$ {\bf at the moment it is added}.
\end{example}
\onslide<+->
\begin{alertblock}{But\ldots}
For the F5 Algorithm itself and the criteria based on the signatures $p$ could be necessary \textbf{in this iteration step}!\\
$\Rightarrow$ Disrespecting the way F5 top-reduces polynomials would harm the correctness of F5 \textbf{in this iteration step}!
\end{alertblock}
\end{frame}


\section{Optimizations of F5}
\subsection{Points of inefficiency}
\begin{frame}
\frametitle{Points of inefficiency}
\onslide<+->
The difficulty of top-reduction in F5 leads to an \textbf{inefficiency}, namely we have way too many polynomials in the intermediate $G_i$s 
\begin{enumerate}
\item which are possible reducers, i.e. more checks for divisibility and the criteria have to be done, and
\item with which we compute new s-polynomials, i.e. more (for the resulting Gr\"obner basis redundant) data is generated.
\end{enumerate}
\onslide<+->
\begin{exampleblock}{Question}
How can these two points be avoided as far as possible?     
\end{exampleblock}
\end{frame}
%\subsection{F5R: F5 Algorithm Reducing by reduced Gr\"obner bases}
%\begin{frame}
%\frametitle{F5R: reduced GB reduction}
%\onslide<+->
%\color<2->{lightgray}An idea how to fix the first inefficiency, was given by Till Stegers in 2005. His optimization of F5 \textbf{using reduced Gr\"obner bases for reduction} is called \color{black}\textbf{F5R} \color<2->{lightgray}in the following:
%\begin{enumerate}
%\onslide<+->
%\item Compute a Gr\"obner basis $G_i$ of $\langle f_1,\ldots,f_i \rangle$.
%\item Compute the reduced Gr\"obner basis $B_i$ of $G_i$.
%\item Compute a Gr\"obner basis $G_{i+1}$ of $\langle f_1,\ldots,f_{i+1} \rangle$ where
%\begin{enumerate}
%\item[(a)] $G_i$ is used to build the new pairs with $f_{i+1}$,
%\item[(b)] $B_i$ is used to reduce polynomials.
%\end{enumerate}
%\end{enumerate}
%\onslide<+->
%\color{black} $\Rightarrow$ \textbf{Fewer reductions} in F5R but still the \textbf{same number of pairs considered and polynomials generated} as in F5.
%\end{frame}
%
%\begin{frame}
%\frametitle{$B_i$ only for reduction?}    
%\onslide<+->
%\begin{exampleblock}{Question}
%Why is $B_i$ only used for reduction purposes, but not for new-pair computations?
%\end{exampleblock}
%\onslide<+->
%\begin{exampleblock}{Answer} 
%\begin{center}
%    \fbox{Interreducing $G_i$ to $B_i$ $\leftrightarrow $ reduction steps rejected by F5}
%\end{center}
%%\onslide<+->
%%Thus we cannot tell what is the signature of an element in $B_i$, but \textbf{we need the signatures for further computations} due to the F5 and the Rewritten Criterion. 
%\end{exampleblock}
%\onslide<+->
%$\Rightarrow$ Reducing $G_i$ to $B_i$ renders the data saved in the \textbf{signatures} of the polynomials \textbf{useless}!
%\end{frame}
%
\subsection{F5C: F5 Algorithm Computing with reduced Gr\"obner bases}
\begin{frame}
\frametitle{F5C: Computations with reduced GB}
\onslide<1->
\color<3->{lightgray}
In 2009 John Perry \& Christian Eder have implemented a new variant of the F5 Algorithm, called \color{black}\textbf{F5C}. \\
\onslide<2->
\color<3->{lightgray}
F5C uses the reduced Gr\"obner basis not only for reduction purposes, but also
for the generation of new s-polynomials:
\onslide<3->
\color{black}
\begin{enumerate}
\item Compute a Gr\"obner basis $G_i$ of $\langle f_1,\ldots,f_i \rangle$.
\item Compute the reduced Gr\"obner basis $B_i$ of $G_i$.
\item Compute a Gr\"obner basis $G_{i+1}$ of $\langle f_1,\ldots,f_{i+1} \rangle$ where
\begin{enumerate}
\item[(a)]<alert@4->$B_i$ is used to build new s-polynomials with $f_{i+1}$,
\item[(b)] $B_i$ is used to reduce polynomials.
\end{enumerate}
\end{enumerate}
\onslide<4->
$\Rightarrow$ \textbf{Fewer reductions} and \textbf{fewer polynomials generated
  and considered} during the algorithm
\end{frame}

\begin{frame}
\frametitle{How to use $B_i$ for computations?}
\onslide<+->
\color<3->{lightgray}We have seen that \textbf{if we interreduce $G_i$} then \textbf{the current signatures are useless} in the following. \\
\onslide<+->
$\Rightarrow$ If the current signatures are useless, then \textbf{throw them away} and \textbf{compute new useful ones}!
\onslide<+->
\color{black}
\begin{block}{Recomputation of signatures}
\begin{enumerate}        
\onslide<+->
\item Delete all signatures.
\item Interreduce $G_i$ to $B_i$. 
\item For each element $g_k \in B_i$ set $\Sig(g_k) = (1,\mathrm{k})$.
\color<5->{lightgray}
\item For all elements $g_j,g_k \in B_i$ \textbf{recompute signatures} for $\Spol(g_j,g_k)$.
\color{black}
\item Start the next iteration step with $f_{i+1}$ by computing all
s-polynomials with elements from $B_i$.
\onslide<+->
\end{enumerate}
\end{block}
\end{frame}
%
%\begin{frame}
%\frametitle{Recomputation of signatures?}
%\onslide<+->
%Why do we recompute the signatures of the s-polynomials in $B_i$?
%\begin{enumerate}
%	\item Both criteria are based on signatures.
%	\item More signatures $\Rightarrow$ possibly more rejections of useless elements.
%	\item Also a {\bf zero polynomial} should have a signature.
%\end{enumerate}
%\onslide<+->
%\begin{exampleblock}{Question}
%Do we really need them?
%\end{exampleblock}
%\onslide<+->
%\begin{exampleblock}{Answer}
%{\bf Not in F5C :)}
%\end{exampleblock}
%\end{frame}
%\begin{frame}
%  \frametitle{How to use $B_i$ for computations?}
%\begin{enumerate}        
%\onslide<+->
%\item Delete all signatures.
%\item Interreduce $G_i$ to $B_i$. 
%\item For each element $g_k \in B_i$ set $\Sig(g_k) = (1,\mathrm{k})$.
%\item \color{lightgray}For all elements $g_j,g_k \in B_i$ \textbf{recompute signatures} for $\Spol(g_j,g_k)$.
%\item\color{black} Start the next iteration step with $f_{i+1}$ by computing all pairs with elements from $B_i$.
%\end{enumerate}
%\end{frame}
\subsection{Comparing F5 and F5C}
\begin{frame}
\frametitle{Implementations}
Three free available implementations:
\begin{enumerate}
\onslide<+->
\item F5 \& F5C as a \textsc{Singular} library (Perry \& Eder)
\item F5 \& F5C implemented in Python for Sage (Perry \& Albrecht): {\bf F4-ish} reduction possible.
\item F5 \& F5C implementation in the \textsc{Singular} kernel: {\bf under development} 
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Comparing F5 and F5C}
\onslide<+->
We are comparing F5 and F5C in the way that we use the \textbf{same implementation} of the \textbf{core algorithm} for all variants. \newline \\
\onslide<+->
Moreover we do not only compare
\begin{enumerate}
\item \textbf{timings}, but also 
\item the \textbf{number of reductions}, and 
\item the \textbf{number of polynomials generated}.
\end{enumerate}
\end{frame}


\begin{frame}
\frametitle{Comparing F5 and F5C}
\onslide<+->
Instead of the timings themselves we present the ratios of the timings comparing
the two variants. \newline \\
\onslide<+->
\centering
\begin{tabular}{|c||c|}
\hline
system & F5C / F5 \\
\hline
\hline
Katsura 7 & 1.06 \\
\hline
Katsura 8 & 0.83 \\
\hline
Katsura 9 & 0.62 \\
\hline
Schrans-Troost & 0.71 \\
\hline
Cyclic 6 & 0.60 \\
\hline
Cyclic 7 & 0.49 \\
\hline
Cyclic 8 & 0.62 \\
\hline
\end{tabular}\\
\hspace*{0.4cm}\tiny{\textsc{Singular} 3.1.0, kernel implementation; Linux-gentoo-r8 2009 x86\_64, Intel Xeon @ 3.16 GHz, 64 GB RAM}
\end{frame}

\begin{frame}
\frametitle{Number of reductions}
\onslide<+->
\centering
\begin{tabular}{|c||c|c|}
\hline
system & \# red in F5 & \# red in F5C \\
\hline
\hline
Katsura 4 & 774 & 222 \\
\hline
Katsura 5 & 14,597 & 3,985 \\
\hline
Katsura 6 & 1,029,614 & 58,082 \\
\hline
Cyclic 5 & 512 & 446 \\
\hline
Cyclic 6 & 41,333 & 14,167 \\
\hline
\end{tabular}\\
\hspace*{0.7cm}\tiny{Sage 3.2.1, Python implementation; Ubuntu Linux 8.10, Intel Core 2 Quad @ 2.66 GHz, 3 GB RAM}
\end{frame}

\begin{frame}
\frametitle{Number of polynomials generated}
\onslide<+->
In the following we present internal data from the computation of Katsura 9. \newline \\
\onslide<+->
\centering
\begin{tabular}{|c||c|c|}
\hline
i & \# $G_i$ in F5 & \# $G_i$ in F5C \\
\hline
\hline
2 & 2 & 2 \\
\hline
3 & 4 & 4 \\
\hline
4 & 8 & 8 \\
\hline
5 & 16 & 15 \\
\hline
6 & 32 & 29 \\
\hline
7 & 60 & 51 \\
\hline
8 & 132 & 109 \\
\hline
9 & 524 & 472 \\
\hline
10 & 1,165 & 778 \\
\hline
\end{tabular}\\
\tiny{Sage 3.2.1, Python implementation; Ubuntu Linux 8.10, Intel Core 2 Quad @
2.66 GHz, 3 GB RAM}\\
\vspace*{5mm}
\hfill\hyperlink{biblio}{\beamergotobutton{Skip}}
\end{frame}
\section{Termination issues of F5}
\subsection{Difficulty of top-reduction revisited}
\begin{frame}
  \frametitle{Difficulty of top-reduction revisited}
  \onslide<+->
  Remember that due to F5's criteria {\bf redundant elements} are added to
  $G$.\\
  \onslide<+->
  $\Rightarrow$ What happens if F5 computes infinitely many redundant elements?
  \onslide<+->
  \begin{center}
    \fbox{Termination of F5?}
  \end{center}
\end{frame}
\begin{frame}
  \frametitle{Do we need those redundant elements?}
  \onslide<+->
  \onslide<+->
    \begin{center}
        \fbox{Yes, we do!}
    \end{center}
  \onslide<+->
  F5 works degree-wise in each iteration step.\\
  \onslide<+->
  Assume that $\lm(p_k) \mid \lm(p_i)$, but
  the corresponding reductions have not taken place when $p_i$ was 
  computed.
  \begin{enumerate}
  \onslide<+->
    \item If there is an element $p_m$ such that $\lm(p_i) \mid
      \lm(p_m)$, we possibly need $p_i$ as reducer.
    \onslide<+->
    \item If $\Spol(p_i,p_j) = \lambda \Spol(p_k,p_j) + \sum_{s} \lambda_s
      p_s$ and {\bf $\Spol(p_k,p_j)$ is not computed}, we need $\Spol(p_i,p_j)$.
  \end{enumerate}
\end{frame}
\subsection{Resolving the termination issue}
\begin{frame}
  \frametitle{Resolving the termination issue}
    \onslide<+->
    \begin{defi}
      An s-polynomial $\Spol(p,q)$ is called an {\bf F5-s-polynomial} if either
      $\lm(p)$ or $\lm(q)$ is redundant in $G$. \\
        \onslide<+->
      Otherwise $\Spol(p,q)$ is called a {\bf
        GB-s-polynomial}.
    \end{defi}
    \onslide<+->
    \begin{example}
      Recall the last slide: Assume that $\lm(p_j)$ and $\lm(p_k)$ are
      non-redundant in $G$.\\
      Then $\Spol(p_i,p_j)$ is an F5-s-polynomial as $\lm(p_k) \mid \lm(p_i)$, whereas
      $\Spol(p_k,p_j)$ is an GB-s-polynomial.
    \end{example}
\end{frame}
\begin{frame}
  \frametitle{Resolving the termination issue}
  \begin{enumerate}
    \onslide<+->
    \item After finitely many steps only F5-s-polynomials are left.
    \onslide<+->
    \item No GB-s-polynomial can be generated from this point
      onwards.
    \onslide<+->
    \item We can go on with the next iteration step / terminate F5.
  \end{enumerate}
\end{frame}
\begin{frame}
  \frametitle{Resolving the termination issue}
  \begin{enumerate}
    \onslide<+->
    \item We label each computed polynomial by a boolean value to distinguish
      redundant and non-redundant ones.
    \onslide<+->
    \item We add a global variable $d$ in the implementation storing the highest
      known degree GB-s-polynomials.
    \onslide<+->
    \item When computing new s-polynomials we have to check and possibly change
      $d$'s value.
    \onslide<+->
    \item If the degree of the next bunch of s-polynomials to be computed is
      greater than $d$, we go to the next iteration step / terminate the
      algorithm.
  \end{enumerate}
\end{frame}

\begin{frame}[label = biblio]
\frametitle{References}
\begingroup
\tiny
\begin{thebibliography}{Faug\`{e}re, 2002}
\bibitem[AH09]{arshashemi}
G. Ars and A. Hashemi.
\newblock{Extended F5 Criteria}
    
\bibitem[Bu65]{buchberger}
B. Buchberger.
\newblock{Ein Algorithmus zum Auffinden der Basiselement des Restklassenrings nach einem nulldimensionalen Polynomideal}
%\newblock{phD thesis at the university of Innsbruck, Austria}, 1965

\bibitem[Fa02]{f5}
J.-C. Faug\`{e}re. 
\newblock{A new efficient algorithm for computing Gr\"obner bases without reduction to zero $F_5$}
%\newblock{Proceedings of the 2002 International Symposium on Symbolic and Algebraic Computation ISSAC}, pages 75-83, 2002

\bibitem[GM88]{gm}
R. Gebauer and H.M. M\"oller.
\newblock{On an Installation of Buchberger's Algorithm}
%\newblock{Journal of Symbolic Computation, 6(2 and 3)}, pages 275-286, 1988

\bibitem[singular]{singular}
W. Decker, G.-M. Greuel, G. Pfister and H. Sch\"onemann.
\newblock{\emph{\textsc{Singular 3-1-1}. A computer algebra system for
polynomial computations}, University of Kaiserslautern, 2010, {\tt http://www.singular.uni-kl.de}.}

\bibitem[MMT92]{traverso}
H. M. M\"oller, T. Mora and C. Traverso.
\newblock{Gr\"obner bases computation using syzygies}
%\newblock{ISSAC 92: Papers from the International Symposium on Symbolic and Algebraic Computation}, pages 320-328, 1992

\bibitem[S{\etalchar{+}}09]{sage}
W.\thinspace{}A. Stein et~al.
\newblock{\emph{{S}age {M}athematics {S}oftware ({V}ersion3.2.1)}, The
Sage~Development Team, 2008, {\tt http://www.sagemath.org}.}

\bibitem[St05]{f5rev}
T. Stegers.
\newblock{Faug\`{e}re's F5 Algorithm Revisited}

\bibitem[SW10]{sunwang}
Y. Sun and D. Wang.
\newblock{A new proof of the F5 Algorithm}
%\newblock{Thesis for the degree of Diplom-Mathematiker at the university of Darmstadt, Germany}, 2005

\end{thebibliography}        
\endgroup
\end{frame}
\end{document}

