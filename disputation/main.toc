\beamer@endinputifotherversion {3.10pt}
\select@language {ngerman}
\beamer@sectionintoc {2}{Eine kleine Einf\"uhrung in die Theorie der Gr\"obner Basen}{5}{0}{1}
\beamer@subsectionintoc {2}{1}{Grunds\"atzliches zu Gr\"obner Basen}{6}{0}{1}
\beamer@subsectionintoc {2}{2}{Berechnung von Gr\"obner Basen}{9}{0}{1}
\beamer@subsectionintoc {2}{3}{Das Problem der Nullreduktion}{15}{0}{1}
\beamer@sectionintoc {3}{Signaturbasierter Ansatz}{19}{0}{2}
\beamer@subsectionintoc {3}{1}{Die grundlegende Idee}{20}{0}{2}
\beamer@subsectionintoc {3}{2}{Signaturbasierte Berechnungen von Gr\"obner Basen}{33}{0}{2}
\beamer@subsectionintoc {3}{3}{Signaturbasierte Kriterien}{44}{0}{2}
\beamer@sectionintoc {4}{Implementierungen \& Ausblick}{49}{0}{3}
\beamer@subsectionintoc {4}{1}{Effiziente Varianten}{50}{0}{3}
\beamer@subsectionintoc {4}{2}{Zeiten}{56}{0}{3}
\beamer@subsectionintoc {4}{3}{Ausblick}{58}{0}{3}
