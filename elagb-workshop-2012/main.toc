\beamer@endinputifotherversion {3.10pt}
\select@language {ngerman}
\beamer@sectionintoc {2}{The basic problem}{6}{0}{1}
\beamer@sectionintoc {3}{Generic signature-based algorithms}{12}{0}{2}
\beamer@subsectionintoc {3}{1}{The basic idea}{13}{0}{2}
\beamer@subsectionintoc {3}{2}{Generic signature-based Gr\"obner basis algorithm}{26}{0}{2}
\beamer@subsectionintoc {3}{3}{Signature-based criteria}{37}{0}{2}
\beamer@sectionintoc {4}{Implementations and recent work}{42}{0}{3}
\beamer@subsectionintoc {4}{1}{Efficient variants}{43}{0}{3}
\beamer@subsectionintoc {4}{2}{Timings}{49}{0}{3}
\beamer@subsectionintoc {4}{3}{Recent work}{51}{0}{3}
