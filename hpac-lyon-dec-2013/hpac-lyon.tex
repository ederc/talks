% load all styles, commands, etc.
\input{../style}
\input{../colors-and-tikz}
% for tables
%\setlength{\tabcolsep}{8pt}
\renewcommand{\arraystretch}{1.4}
\title{Hybrid\\Matrix Multiplication\\\&\\Gaussian Elimination}
\author{Christian Eder, Jean-Charles Faug\`ere, Fayssal Martani\\and Bjarke
  Hammersholt Roune}
\institute{HPAC Meeting -- Lyon}
\date{December 10 -- 11, 2013}
% logo of my university
\titlegraphic{\hspace*{-0.7cm}\includegraphics[width=4.2cm]{../logo/INRIA-transparent.png}\hspace*{5mm}~%
     \includegraphics[width=1.2cm]{../logo/CNRS.jpg}\hspace*{5mm}~%
     \includegraphics[width=2.5cm]{../logo/UPMC-orange-transparent.png}\hspace*{5mm}~%
     \includegraphics[width=1.5cm]{../logo/LIP6.png}
}
%\usetheme{Singapore}
\begin{document}
\nocite{fF41999}
\nocite{martani-thesis}
\nocite{lachartre-thesis}
\nocite{demmel-grigori-xiang}
%\renewcommand{\inserttotalframenumber}{13}
\AtBeginSection[]
{
\begin{frame}<beamer>
\frametitle{}
\tableofcontents[currentsection]
\end{frame}
}
\frame{\titlepage}

\section{Fast linear algebra for computing \grobner{} bases}
\begin{frame}
\frametitle{First attempts}
\onslide<1->
In 2011 the original LELA project for fast
linear algebra on special structured
hybrid-sparse-dense matrices was started at the university of Kaiserslautern in
the Singular team by Bradford Hovinen.
\begin{center}
\url{https://github.com/Singular/LELA}
\end{center}
\vspace*{3mm}
\onslide<2->
In 2012, doing a master thesis at the INRIA PolSys team Fayssal Martani optimized this
code and added a completely new component with a specialized algorithm for
computing the Gaussian Elimination.
\begin{center}
\url{https://github.com/martani/LELA}
\end{center}
\vspace*{3mm}
\onslide<3->
Also in 2012, Bjarke Hammersholt Roune started working on Linear Algebra for
computing \grobner{} bases in the MATHICGB package at the university of
Kaiserslautern.
\begin{center}
\url{https://github.com/broune/mathicgb}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Basic idea}
\onslide<+->
The idea for a specialized Gaussian Elimination was initially presented by
Faug\`ere and Lachartre (\cite{FL10b}).
\onslide<+->
\begin{itemize}
\item Special structure of the matrices due to \grobner{} basis computations.
\onslide<+->
\item Restructure matrix, swap rows and columns, take care of underlying
monomial order
\onslide<+->
\item Split matrix in $4$ parts:
\begin{center}
\begin{tikzpicture}[scale=0.5]
\fill [fill=myblued!90!white,draw=white,densely dashed, fill opacity=0.6]
(0,0) rectangle (3.9,2.9); 
\draw [ draw=white, densely dashed, fill opacity=0.6] (0,3.1) rectangle
(3.9,7); 
\fill [fill=myyellowd!90!white, fill opacity=0.6] (3.9,3.1) -- (3.9,7) --
(0,7) -- cycle;
\fill [fill=mygreend!90!white,draw=white, densely dashed, fill
opacity=0.6] (4.1,0) rectangle (8,2.9); 
\fill [fill=myredd!90!white,draw=white, densely dashed, fill opacity=0.6]
(4.1,3.1) rectangle (8,7); 
\draw (2,5) node[color=white]{$A$};
\draw (6,5) node[color=white]{$B$};
\draw (2,1.5) node[color=white]{$C$};
\draw (6,1.5) node[color=white]{$D$};
\end{tikzpicture}
\end{center}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{How our matrices look like}
\begin{center}
\includegraphics[width=0.8\textwidth]{f4-kat12-mat6-oldMethod-sub-ABCD-transparent.pdf}
\end{center}
\end{frame}

\section{Hybrid Matrix Multiplication}
\begin{frame}
\frametitle{Hybrid Matrix Multiplication}
\begin{center}
\includegraphics[width=0.8\textwidth]{f4-kat12-mat6-upper.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{$B \gets A^{-1}B$}
\begin{center}
\includegraphics[width=0.8\textwidth]{ab-computation.pdf}
\end{center}
\end{frame}

%\begin{frame}
%\frametitle{Block representation\footnote{Taken from~\cite{FL10b}.} of sub matrices}
%\begin{center}
%\includegraphics[width=0.7\textwidth]{quad-matrix.pdf}
%\end{center}
%\end{frame}

\begin{frame}
\frametitle{$B \gets A^{-1}B$ -- Block Version}
\begin{center}
\includegraphics[width=0.9\textwidth]{ab-block-1.pdf}
\end{center}
\end{frame}

\begin{frame}
\addtocounter{framenumber}{-1}
\frametitle{$B \gets A^{-1}B$ -- Block Version}
\begin{center}
\includegraphics[width=0.9\textwidth]{ab-block-2.pdf}
\end{center}
\end{frame}

\begin{frame}
\addtocounter{framenumber}{-1}
\frametitle{$B \gets A^{-1}B$ -- Block Version}
\begin{center}
\includegraphics[width=0.9\textwidth]{ab-block-3.pdf}
\end{center}
\end{frame}

\begin{frame}
\addtocounter{framenumber}{-1}
\frametitle{$B \gets A^{-1}B$ -- Block Version}
\begin{center}
\includegraphics[width=0.9\textwidth]{ab-block-4.pdf}
\end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{LELA implementation}
\begin{lstlisting}
#ifdef L1	//LEVEL 1 PARALLELISM
omp_set_nested(1);
#pragma omp parallel num_threads(NUM_THREADS_OMP_MASTER) {
#endif
//for all columns of blocs of B
#ifdef L1	//LEVEL 1 PARALLELISM
#pragma omp for schedule(dynamic) nowait
#endif
for (uint32 ii = 0; ii < nb_column_blocs_B; ++ii) {
	//for all rows of blocs in A (starting from the end)
	for (uint32 jj = 0; jj < nb_row_blocs_A; ++jj) {
  const uint32 first_bloc_idx = 
    A.FirstBlocsColumIndexes[jj] / A.bloc_width();
	  const uint32 last_bloc_idx = MIN(A[jj].size () - 1, jj);

	  Level1Ops::memsetToZero(dense_bloc);
	  Level1Ops::copySparseBlocToDenseBlocArray(R, B[jj][ii], dense_bloc);

	  //for all the blocs in the current row of A
	  for (uint32 k = 0; k < last_bloc_idx; ++k) {
		    if (A[jj][k].empty() || B[k + first_bloc_idx][ii].empty())
		      continue;

#ifdef L2 //LEVEL 2!
#pragma omp parallel num_threads(NUM_THREADS_OMP_SLAVES_PER_MASTER) {
#pragma omp for schedule(dynamic) nowait
#endif
    for (int i = 0; i < DEFAULT_BLOC_HEIGHT / 2; ++i) {
      uint8 is_sparse = 0;
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Some examples -- Structures}
\small{
\begin{table}
\centering
\begin{tabular}{|c||c|c|c|c|}
\hline
Benchmark & size $A$ & density $A$ & size $B$ & density $B$\\
\hline
\hline
Kat12-Mat7 & $18,890 \times 18,890$ & $0.76\%$ & $18,890 \times 4,237$ &
$14.02\%$\\
\hline
Kat13-Mat10 & $40,023 \times 40,023$ & $0.50\%$ & $40,023 \times 8,204$ &
$13.53\%$\\
\hline
Kat14-Mat14 & $88,941 \times 88,941$ & $0.37\%$ & $88,941 \times 16,397$ &
$12.03\%$\\
\hline
Kat16-Mat6 & $82,086 \times 82,086$ & $0.22\%$ & $82,086 \times 32,166$ &
$3.54\%$\\
\hline
Minrank-9-10-7-Mat3 & $18,460 \times 18,460$ & $4.01\%$ & $18,460
\times 56,095$ & $14.01\%$\\
\hline
Minrank-9-10-7-Mat4 & $34,053 \times 34,053$ & $3.52\%$ & $34,053
\times 74,125$ & $38.93\%$\\
\hline
\end{tabular}
\end{table}
}
\end{frame}

\begin{frame}
\frametitle{Some examples -- Timings LELA \& MATHICGB}
\footnotesize{
\begin{table}
\begin{tabular}{|c|rrrrrrr|}
\hline
\multirow{2}{*}{Benchmark} & \multicolumn{7}{c|}{Timings in seconds with given number of threads}\\
 & \multicolumn{1}{c|}{$1$} & \multicolumn{1}{c|}{$2$} &
 \multicolumn{1}{c|}{$4$} &
   \multicolumn{1}{c|}{$8$} & \multicolumn{1}{c|}{$16$} &
   \multicolumn{1}{c|}{$32$} & \multicolumn{1}{c|}{$64$}\\
\hline
K12-M7-L & $31.09$ & $16.80$ & $8.99$ & $4.86$ & $3.15$ &
$2.81$ & $3.00$\\
K14-M7-M & $32.11$ & $17.30$ & $8.76$ & $4.55$ & $3.01$ &
$2.41$ & $2.13$\\
\hline
K13-M10-L & $175.41$ & $87.65$ & $45.13$ & $24.33$ & $13.34$ &
$8.33$ & $8.59$\\
K13-M10-M & $176.52$ & $86.98$ & $44.35$ & $24.02$ & $12.78$ &
$7.96$ & $6.89$\\
\hline
K14-M14-L & $1,285.10$ & $635.83$ & $324.21$ & $170.62$ & $92.85$ &
$52.51$ & $43.71$\\
K14-M14-M & $1,2799.23$ & $631.74$ & $322.56$ & $168.75$ & $93.45$ &
$50.13$ & $38.68$\\
\hline
K16-M6-L & $593.32$ & $298.87$ & $153.33$ & $79.17$ & $45.66$ &
$29.62$ & $37.59$\\
K16-M6-M & $597.41$ & $300.16$ & $154.67$ & $78.53$ & $44.77$ &
$27.32$ & $31.14$\\
\hline
M-9-10-7-M3-L & $1,734.00$ & $862.80$ & $432.40$ & $218.10$ &
$116.30$ & $67.04$ & $51.62$\\
M-9-10-7-M3-M & $1,745.81$ & $867.98$ & $438.31$ & $221.12$ &
$124.46$ & $73.01$ & $58.23$\\
\hline
M-9-10-7-M4-L & $6,410.00$ & $3,174.22$ & $1,583.04$ &
$801.63$ & $428.50$ & $243.62$ & $183.55$\\
M-9-10-7-M4-M & $6,421.23$ & $3,197.42$ & $1,612.87$ &
$809.62$ & $426.16$ & $247.11$ & $188.16$\\
\hline
\end{tabular}
\end{table}
}
\end{frame}
\begin{frame}
\frametitle{Some examples -- Speedups LELA \& MATHICGB}
\small{
\begin{table}
\begin{tabular}{|c|rrrrrrr|}
\hline
\multirow{2}{*}{Benchmark} & \multicolumn{7}{c|}{Speedup w.r.t.
  computation on $1$ thread}\\
 & \multicolumn{1}{c|}{$1$} & \multicolumn{1}{c|}{$2$} &
 \multicolumn{1}{c|}{$4$} &
   \multicolumn{1}{c|}{$8$} & \multicolumn{1}{c|}{$16$} &
   \multicolumn{1}{c|}{$32$} & \multicolumn{1}{c|}{$64$}\\
\hline
K12-M7-L & $1.00$ & $\color{yellow}{1.85}$ & $3.45$ & $6.40$ & $9.86$ &
$11.06$ & $10.36$\\
K12-M7-M & $0.97$ & $1.79$ & $\color{yellow}{3.54}$ & $\color{yellow}{6.83}$ & $\color{yellow}{10.32}$ &
$\color{yellow}{12.90}$ & $\color{yellow}{14.60}$\\
\hline
K13-M10-L & $1.00$ & $2.00$ & $3.89$ & $7.21$ & $13.15$ &
$21.07$ & $20.44$\\
K13-M10-M & $0.99$ & $\color{yellow}{2.01}$ & $\color{yellow}{3.95}$ &
$\color{yellow}{7.30}$ & $\color{yellow}{13.73}$ &
$\color{yellow}{22.04}$ & $\color{yellow}{25.45}$\\
\hline
K14-M14-L & $0.99$ & $2.01$ & $3.95$ & $7.50$ & $\color{yellow}{13.78}$ &
$24.37$ & $29.28$\\
K14-M14-M & $1.00$ & $\color{yellow}{2.03}$ & $\color{yellow}{3.97}$ & $\color{yellow}{7.58}$ & $13.70$ &
$\color{yellow}{25.53}$ & $\color{yellow}{33.09}$\\
\hline
K16-M6-L & $1.00$ & $\color{yellow}{1.99}$ & $\color{yellow}{3.87}$ & $7.49$ & $13.00$ &
$20.03$ & $15.78$\\
K16-M6-M & $0.99$ & $1.97$ & $3.83$ & $\color{yellow}{7.55}$ & $\color{yellow}{13.25}$ &
$\color{yellow}{21.71}$ & $\color{yellow}{19.05}$\\
\hline
M-9-10-7-M3-L & $1.00$ & $\color{yellow}{2.01}$ & $\color{yellow}{4.01}$ & $\color{yellow}{7.95}$ & $\color{yellow}{14.91}$ &
$\color{yellow}{25.87}$ & $\color{yellow}{33.59}$\\
M-9-10-7-M3-M & $0.99$ & $1.99$ & $3.95$ & $7.84$ & $13.93$ &
$23.75$ & $29.77$\\
\hline
M-9-10-7-M4-L & $1.00$ & $\color{yellow}{2.02}$ & $\color{yellow}{4.04}$ & $\color{yellow}{8.00}$ & $14.96$ &
$\color{yellow}{26.31}$ & $\color{yellow}{34.93}$\\
M-9-10-7-M4-M & $0.99$ & $2.00$ & $3.97$ & $7.91$ & $\color{yellow}{15.04}$ &
$25.93$ & $34.06$\\
\hline
\end{tabular}
\end{table}
}
\end{frame}

\section{Next steps on matrix multiplication}
\begin{frame}
\frametitle{Next steps}
\begin{itemize}
\item Test and compare different sparse, hybrid, dense data structures in LELA
and MATHICGB
\item Implement and compare task-based hybrid tiled matrix multiplication with StarPU,
OpenMP/KAAPI and Intel TBB
\end{itemize}
\end{frame}

\section{First steps on Gaussian Elimination}
\begin{frame}
\frametitle{First step -- Dense Gaussian Elimination}
\begin{center}
\includegraphics[width=0.8\textwidth]{f4-kat12-mat6-dense.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Some examples -- Kat16-Mat9}
\begin{center}
\includegraphics[width=\textwidth]{timings-2395-74125-inv.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Some examples -- Minrank-10-9-7-Mat4}
\begin{center}
\includegraphics[width=\textwidth]{timings-3614-60539-inv.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Some examples -- Minrank-10-9-7-Mat6}
\begin{center}
\includegraphics[width=\textwidth]{timings-4040-101500-inv.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{LELA implementation}
\begin{itemize}
\item Using \textcolor{yellow}{pthreads}
\item Idea of structured Gaussian Elimination:
\[\text{row}_i \gets \text{row}_i + \sum_{j=1}^{i-1} a_j \text{row}_j\]
At step $i$ rows $1$ to $i-1$ are reduced.
\begin{itemize}
\item Reduce in parallel rows $i$ to $i+p$ by pivots up to $i-1$
\item Row $i+\text{offset}$ waits until rows from $i$ to $i+\text{offset}-1$ are
done\\
$\Rightarrow$ waiting queue
\item Threads share global variable \texttt{last\_pivot}
\end{itemize}
\item Blocksize $256$ works out best for the shown examples
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Tiled implementation}
\begin{center}
\begin{tikzpicture}[scale=0.5, transform shape]
\fill[fill=lgrey, fill opacity=0.6] (0,0) rectangle (3,15);
\fill[fill=lgrey, fill opacity=0.6] (3,15) rectangle (15,12);
\fill[fill=mygreend, fill opacity=0.6] (3,12) rectangle (6,9);
\fill[fill=myblued, fill opacity=0.6] (9,12) rectangle (12,9);
\fill[fill=myredd!50!myyellowd, fill opacity=0.6] (9,9) rectangle (12,6);
\fill[fill=myredd!50!myyellowd, fill opacity=0.6] (9,6) rectangle (12,3);
\fill[fill=myredd!50!myyellowd, fill opacity=0.6] (9,3) rectangle (12,0);
\node (task_11) at (4.5,10.5) {\large{\bf{Task 11}}};
\node (task_12) at (10.5,10.5) {\large{\bf{Task 12}}};
\node (task_22_1) at (10.5,7.5) {\large{\bf{Task 22}}};
\node (task_22_2) at (10.5,4.5) {\large{\bf{Task 22}}};
\node (task_22_3) at (10.5,1.5) {\large{\bf{Task 22}}};
\path[->] (5.5,10.5) edge[in=170,out=10] (9.5,10.5);
\draw[draw=white, densely dotted] (0,0) rectangle (15,15);
\draw[densely dotted,draw=white] (0.1,3) -- (14.9,3); 
\draw[densely dotted,draw=white] (0.1,6) -- (14.9,6); 
\draw[densely dotted,draw=white] (0.1,9) -- (14.9,9); 
\draw[densely dotted,draw=white] (0.1,12) -- (14.9,12); 
\draw[densely dotted,draw=white] (3,14.9) -- (3,0.1); 
\draw[densely dotted,draw=white] (6,14.9) -- (6,0.1); 
\draw[densely dotted,draw=white] (9,14.9) -- (9,0.1); 
\draw[densely dotted,draw=white] (12,14.9) -- (12,0.1); 
\end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{StarPU implementation}
Tiled Gaussian Elimination\\
\begin{lstlisting}
struct starpu_data_filter f = {
  .filter_func = starpu_matrix_filter_vertical_block,
  .nchildren = lblocks
};

struct starpu_data_filter f2 = {
  .filter_func = starpu_matrix_filter_block,
  .nchildren = mblocks
};

starpu_data_map_filters(dataA, 2, &f, &f2);
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{StarPU Implementation}
Tasks \& Dependencies\\
\small{
\begin{lstlisting}
static int create_task_12(starpu_data_handle_t dataA, unsigned k, unsigned j)
{
  int ret;

  struct starpu_task *task = create_task(TAG12(k, j));

  task->cl = &task_12_cl;

  /* what sub-data is manipulated ? */
  task->handles[0] = starpu_data_get_sub_data(dataA, 2, k, k);
  task->handles[1] = starpu_data_get_sub_data(dataA, 2, k, j);

  if (!no_prio && (j == k+1)) {
	    task->priority = STARPU_MAX_PRIO;
  }

  /* enforce dependencies ... */
  if (k > 0) {
	    starpu_tag_declare_deps(TAG12(k, j), 2, TAG11(k), TAG22(k-1, k, j));
  } else {
	    starpu_tag_declare_deps(TAG12(k, j), 1, TAG11(k));
  }
...
}
\end{lstlisting}
}
\end{frame}

\begin{frame}[fragile]
\frametitle{Intel TBB Implementation}
\begin{lstlisting}
class TASK_12 : public task {
public:
  TYPE *a;
  TYPE *b;
  TYPE offset;
  // successors, all TASK_22
  TASK_22 **task_22_succ;

  TASK_12(TYPE *a_, TYPE *b_, TYPE offset);

  task* execute();
};
\end{lstlisting}

\begin{lstlisting}
for (j=k+1; j<mblocks; ++j) {
  task_12_queue[task_12_ctr] = new(task::allocate_root()) 
    TASK_12(&mat[tile_size*(k+k*m)], &mat[tile_size*(j+k*m)],k);
  task_11_queue[k]->task_12_succ[j-k-1] = task_12_queue[task_12_ctr];
  if (k!=0) {
    task_22_queue[task_22_start_idx+j-k]->task_12_succ =
      task_12_queue[task_12_ctr];
    task_12_queue[task_12_ctr]->set_ref_count(2);
  } else {
    task_12_queue[task_12_ctr]->set_ref_count(1);
  }
  task_12_ctr++;  
}
\end{lstlisting}

\end{frame}
\section{Next steps}
\begin{frame}
\frametitle{Next steps}
\begin{itemize}
\item Finish Intel TBB implementation of dense Gaussian Elimination and compare
to StarPU
\item Improve scheduling for \#columns $>>$ \#rows
\item Make these implementations available in LELA resp. MATHICGB
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Directly reduce C?}
\begin{center}
\includegraphics[width=0.8\textwidth]{f4-kat12-mat6-direct.pdf}
\end{center}
\end{frame}
\begin{frame}
\frametitle{Other tests -- CALU 1}
\begin{center}
\includegraphics[width=\textwidth]{calu-36000-l8.pdf}
\end{center}
\end{frame}
\begin{frame}
\frametitle{Other tests -- CALU 2}
\begin{center}
\includegraphics[width=\textwidth]{calu-inc-32-4.pdf}
\end{center}
\end{frame}
\begin{frame}
\addtocounter{framenumber}{-1}
\frametitle{Other tests -- CALU 2}
\begin{center}
\includegraphics[width=\textwidth]{calu-inc-32-8.pdf}
\end{center}
\end{frame}
\begin{frame}
\addtocounter{framenumber}{-1}
\frametitle{Other tests -- CALU 2}
\begin{center}
\includegraphics[width=\textwidth]{calu-inc-32-16.pdf}
\end{center}
\end{frame}
\begin{frame}
\addtocounter{framenumber}{-1}
\frametitle{Other tests -- CALU 2}
\begin{center}
\includegraphics[width=\textwidth]{calu-inc-32-32.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{References}
\begingroup
\scriptsize
\bibliographystyle{plain}
\bibliography{../bib}
\endgroup
\end{frame}

\end{document}

