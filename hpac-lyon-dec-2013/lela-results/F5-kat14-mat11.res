Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/kat14/mat11
    86547 x 102826 matrix
    mod 65521
    Nb of Nz elements 185647492 (density 2.0861% ) -	size 1062 MB
  Finished activity (rea: 2.827s, cpu: 0.916s, sys: 1.26s): done
  [[[Loading matrix]]]		 Memory (RSS: 1071MB; VM: 1108MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 3.087s, cpu: 3.064s, sys: 0.02s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 1104MB; VM: 1140MB)
      
      Pivots found: 85889
      
      ****	sub_B (85889x 16937)	- nb elements 149232867 - density: 10.26 % 	 bloc dim 256 x 256  ****
      ****	sub_D (  658x 16937)	- nb elements 4072777 - density: 36.55 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (85889x 85889)	- nb elements 15957652 - density: 0.2163 % 	 bloc dim   ****
      ****	sub_C_multiline (  658x 85889)	- nb elements 5602239 - density: 9.913 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 1
      Finished activity (rea: 26.35s, cpu: 26.24s, sys: 0.08s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.9372s, cpu: 0.924s, sys: 0.012s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 1104MB; VM: 1212MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 1
      Finished activity (rea: 266.1s, cpu: 265.8s, sys: 0.012s): [D = D - C*B]
      ****	sub_D (  658x 16937)	- nb elements 11144397 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 1114MB; VM: 1212MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 1
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.155s, u: 0.152s, s: 0.004s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 3.569s, cpu: 3.532s, sys: 0.032s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 1104MB; VM: 1212MB)
      
      Rank of D 658
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.0007391s, cpu: 0s, sys: 0s): done
      Pivots found: 658
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.000355s, cpu: 0.004s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 3.02s, cpu: 2.792s, sys: 0.224s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 1654MB; VM: 1844MB)
      
      True rank 86547
    Finished activity (rea: 303.4s, cpu: 302.7s, sys: 0.384s): ROUND 1
  Finished activity (rea: 303.4s, cpu: 302.7s, sys: 0.384s): FGL BLOC NEW METHOD
  
  ****	A (86547x 102826)	- nb elements 186092549 - density: 2.091 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 1654MB; VM: 1844MB)
  
Faugère-Lachartre Bloc Version (303.616 s)
Finished activity (rea: 306.3s, cpu: 303.6s, sys: 1.648s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.05078MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/kat14/mat11
    86547 x 102826 matrix
    mod 65521
    Nb of Nz elements 185647492 (density 2.0861% ) -	size 1062 MB
  Finished activity (rea: 1.44s, cpu: 0.8s, sys: 0.64s): done
  [[[Loading matrix]]]		 Memory (RSS: 1071MB; VM: 1108MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 2.129s, cpu: 3.7s, sys: 0.508s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 1480MB; VM: 1573MB)
      
      Pivots found: 85889
      
      ****	sub_B (85889x 16937)	- nb elements 149232867 - density: 10.26 % 	 bloc dim 256 x 256  ****
      ****	sub_D (  658x 16937)	- nb elements 4072777 - density: 36.55 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (85889x 85889)	- nb elements 15957652 - density: 0.2163 % 	 bloc dim   ****
      ****	sub_C_multiline (  658x 85889)	- nb elements 5602239 - density: 9.913 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 2
      Finished activity (rea: 12.28s, cpu: 24.42s, sys: 0.096s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.6708s, cpu: 1.264s, sys: 0.064s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 1515MB; VM: 1717MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 2
      Finished activity (rea: 134.4s, cpu: 265.7s, sys: 0.008s): [D = D - C*B]
      ****	sub_D (  658x 16937)	- nb elements 11144397 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 1526MB; VM: 1717MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 2
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.1096s, u: 0.18s, s: 0.012s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 2.081s, cpu: 4.044s, sys: 0.076s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 1488MB; VM: 1717MB)
      
      Rank of D 658
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.0005879s, cpu: 0s, sys: 0s): done
      Pivots found: 658
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0003691s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 2.546s, cpu: 4.616s, sys: 0.276s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 1883MB; VM: 2166MB)
      
      True rank 86547
    Finished activity (rea: 154.5s, cpu: 304.2s, sys: 1.028s): ROUND 1
  Finished activity (rea: 154.5s, cpu: 304.2s, sys: 1.028s): FGL BLOC NEW METHOD
  
  ****	A (86547x 102826)	- nb elements 186092549 - density: 2.091 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 1883MB; VM: 2166MB)
  
Faugère-Lachartre Bloc Version (305.016 s)
Finished activity (rea: 155.9s, cpu: 305s, sys: 1.668s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/kat14/mat11
    86547 x 102826 matrix
    mod 65521
    Nb of Nz elements 185647492 (density 2.0861% ) -	size 1062 MB
  Finished activity (rea: 1.441s, cpu: 0.764s, sys: 0.676s): done
  [[[Loading matrix]]]		 Memory (RSS: 1071MB; VM: 1108MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 2.05s, cpu: 4.86s, sys: 1.692s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 1573MB; VM: 1714MB)
      
      Pivots found: 85889
      
      ****	sub_B (85889x 16937)	- nb elements 149232867 - density: 10.26 % 	 bloc dim 256 x 256  ****
      ****	sub_D (  658x 16937)	- nb elements 4072777 - density: 36.55 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (85889x 85889)	- nb elements 15957652 - density: 0.2163 % 	 bloc dim   ****
      ****	sub_C_multiline (  658x 85889)	- nb elements 5602239 - density: 9.913 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 4
      Finished activity (rea: 7.737s, cpu: 30.85s, sys: 0.044s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.2151s, cpu: 0.756s, sys: 0.068s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 1612MB; VM: 2066MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 4
      Finished activity (rea: 67.11s, cpu: 264.8s, sys: 0.028s): [D = D - C*B]
      ****	sub_D (  658x 16937)	- nb elements 11144397 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 1623MB; VM: 2066MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 4
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.0764s, u: 0.184s, s: 0.004s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 1.335s, cpu: 5.064s, sys: 0.084s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 1574MB; VM: 2002MB)
      
      Rank of D 658
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.0003049s, cpu: 0s, sys: 0s): done
      Pivots found: 658
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.000181s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 2.492s, cpu: 8.064s, sys: 0.864s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 2210MB; VM: 2707MB)
      
      True rank 86547
    Finished activity (rea: 81.35s, cpu: 314.8s, sys: 2.78s): ROUND 1
  Finished activity (rea: 81.35s, cpu: 314.8s, sys: 2.78s): FGL BLOC NEW METHOD
  
  ****	A (86547x 102826)	- nb elements 186092549 - density: 2.091 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 2210MB; VM: 2707MB)
  
Faugère-Lachartre Bloc Version (315.556 s)
Finished activity (rea: 82.79s, cpu: 315.6s, sys: 3.456s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04688MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/kat14/mat11
    86547 x 102826 matrix
    mod 65521
    Nb of Nz elements 185647492 (density 2.0861% ) -	size 1062 MB
  Finished activity (rea: 1.952s, cpu: 0.984s, sys: 0.968s): done
  [[[Loading matrix]]]		 Memory (RSS: 1071MB; VM: 1108MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 2.35s, cpu: 6.028s, sys: 2.66s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 1590MB; VM: 1810MB)
      
      Pivots found: 85889
      
      ****	sub_B (85889x 16937)	- nb elements 149232867 - density: 10.26 % 	 bloc dim 256 x 256  ****
      ****	sub_D (  658x 16937)	- nb elements 4072777 - density: 36.55 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (85889x 85889)	- nb elements 15957652 - density: 0.2163 % 	 bloc dim   ****
      ****	sub_C_multiline (  658x 85889)	- nb elements 5602239 - density: 9.913 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 8
      Finished activity (rea: 3.563s, cpu: 28.29s, sys: 0s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.2527s, cpu: 1.096s, sys: 0.228s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 1669MB; VM: 2610MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 8
      Finished activity (rea: 35.21s, cpu: 266.3s, sys: 0s): [D = D - C*B]
      ****	sub_D (  658x 16937)	- nb elements 11144397 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 1681MB; VM: 2610MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 8
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.07639s, u: 0.188s, s: 0s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 1.035s, cpu: 7.292s, sys: 0.148s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 1634MB; VM: 2546MB)
      
      Rank of D 658
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.0002909s, cpu: 0s, sys: 0s): done
      Pivots found: 658
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.000181s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 1.749s, cpu: 9.072s, sys: 1.816s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 2441MB; VM: 3442MB)
      
      True rank 86547
    Finished activity (rea: 44.78s, cpu: 318.7s, sys: 4.852s): ROUND 1
  Finished activity (rea: 44.78s, cpu: 318.7s, sys: 4.852s): FGL BLOC NEW METHOD
  
  ****	A (86547x 102826)	- nb elements 186092549 - density: 2.091 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 2441MB; VM: 3442MB)
  
Faugère-Lachartre Bloc Version (319.724 s)
Finished activity (rea: 46.74s, cpu: 319.7s, sys: 5.82s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/kat14/mat11
    86547 x 102826 matrix
    mod 65521
    Nb of Nz elements 185647492 (density 2.0861% ) -	size 1062 MB
  Finished activity (rea: 1.504s, cpu: 0.74s, sys: 0.764s): done
  [[[Loading matrix]]]		 Memory (RSS: 1071MB; VM: 1108MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 2.433s, cpu: 6.696s, sys: 2.924s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 1627MB; VM: 2322MB)
      
      Pivots found: 85889
      
      ****	sub_B (85889x 16937)	- nb elements 149232867 - density: 10.26 % 	 bloc dim 256 x 256  ****
      ****	sub_D (  658x 16937)	- nb elements 4072777 - density: 36.55 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (85889x 85889)	- nb elements 15957652 - density: 0.2163 % 	 bloc dim   ****
      ****	sub_C_multiline (  658x 85889)	- nb elements 5602239 - density: 9.913 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 16
      Finished activity (rea: 2.181s, cpu: 34.52s, sys: 0.044s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.41s, cpu: 1.52s, sys: 0.436s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 1732MB; VM: 3378MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 16
      Finished activity (rea: 20.79s, cpu: 284.7s, sys: 0.016s): [D = D - C*B]
      ****	sub_D (  658x 16937)	- nb elements 11144397 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 1748MB; VM: 3378MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 16
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.06896s, u: 0.216s, s: 0.016s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 1.039s, cpu: 12.99s, sys: 0.748s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 1720MB; VM: 3378MB)
      
      Rank of D 658
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.0002961s, cpu: 0s, sys: 0s): done
      Pivots found: 658
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0001831s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 1.551s, cpu: 8.872s, sys: 4.184s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 2524MB; VM: 4210MB)
      
      True rank 86547
    Finished activity (rea: 29.07s, cpu: 350s, sys: 8.356s): ROUND 1
  Finished activity (rea: 29.07s, cpu: 350s, sys: 8.356s): FGL BLOC NEW METHOD
  
  ****	A (86547x 102826)	- nb elements 186092549 - density: 2.091 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 2524MB; VM: 4210MB)
  
Faugère-Lachartre Bloc Version (350.76 s)
Finished activity (rea: 30.58s, cpu: 350.8s, sys: 9.12s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/kat14/mat11
    86547 x 102826 matrix
    mod 65521
    Nb of Nz elements 185647492 (density 2.0861% ) -	size 1062 MB
  Finished activity (rea: 1.773s, cpu: 0.928s, sys: 0.844s): done
  [[[Loading matrix]]]		 Memory (RSS: 1071MB; VM: 1108MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 2.568s, cpu: 7.044s, sys: 3.284s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 1658MB; VM: 3346MB)
      
      Pivots found: 85889
      
      ****	sub_B (85889x 16937)	- nb elements 149232867 - density: 10.26 % 	 bloc dim 256 x 256  ****
      ****	sub_D (  658x 16937)	- nb elements 4072777 - density: 36.55 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (85889x 85889)	- nb elements 15957652 - density: 0.2163 % 	 bloc dim   ****
      ****	sub_C_multiline (  658x 85889)	- nb elements 5602239 - density: 9.913 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 32
      Finished activity (rea: 1.802s, cpu: 56.27s, sys: 0.212s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.4789s, cpu: 1.496s, sys: 0.652s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 1780MB; VM: 5426MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 32
      Finished activity (rea: 12.61s, cpu: 310.8s, sys: 0.02s): [D = D - C*B]
      ****	sub_D (  658x 16937)	- nb elements 11144397 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 1786MB; VM: 5426MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 32
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.05458s, u: 0.216s, s: 0.032s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 1.018s, cpu: 22.38s, sys: 2.536s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 1729MB; VM: 5426MB)
      
      Rank of D 658
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.000319s, cpu: 0s, sys: 0s): done
      Pivots found: 658
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0001941s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 1.741s, cpu: 8.516s, sys: 10.84s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 2492MB; VM: 5490MB)
      
      True rank 86547
    Finished activity (rea: 20.86s, cpu: 407.3s, sys: 17.56s): ROUND 1
  Finished activity (rea: 20.86s, cpu: 407.3s, sys: 17.56s): FGL BLOC NEW METHOD
  
  ****	A (86547x 102826)	- nb elements 186092549 - density: 2.091 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 2492MB; VM: 5490MB)
  
Faugère-Lachartre Bloc Version (408.248 s)
Finished activity (rea: 22.64s, cpu: 408.2s, sys: 18.4s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04688MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/kat14/mat11
    86547 x 102826 matrix
    mod 65521
    Nb of Nz elements 185647492 (density 2.0861% ) -	size 1062 MB
  Finished activity (rea: 1.758s, cpu: 1.008s, sys: 0.748s): done
  [[[Loading matrix]]]		 Memory (RSS: 1071MB; VM: 1108MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 3.483s, cpu: 8.272s, sys: 4.596s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 1657MB; VM: 5651MB)
      
      Pivots found: 85889
      
      ****	sub_B (85889x 16937)	- nb elements 149232867 - density: 10.26 % 	 bloc dim 256 x 256  ****
      ****	sub_D (  658x 16937)	- nb elements 4072777 - density: 36.55 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (85889x 85889)	- nb elements 15957652 - density: 0.2163 % 	 bloc dim   ****
      ****	sub_C_multiline (  658x 85889)	- nb elements 5602239 - density: 9.913 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 64
      Finished activity (rea: 1.746s, cpu: 97.12s, sys: 0.692s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.7935s, cpu: 2.324s, sys: 1.436s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 1797MB; VM: 9779MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 64
      Finished activity (rea: 9.058s, cpu: 418.4s, sys: 0.132s): [D = D - C*B]
      ****	sub_D (  658x 16937)	- nb elements 11144397 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 1793MB; VM: 9779MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 64
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.06505s, u: 0.256s, s: 0.06s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 1.61s, cpu: 35.87s, sys: 12.94s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 1731MB; VM: 9779MB)
      
      Rank of D 658
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.000365s, cpu: 0s, sys: 0s): done
      Pivots found: 658
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.000232s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 2.621s, cpu: 12.2s, sys: 21.16s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 2499MB; VM: 9779MB)
      
      True rank 86547
    Finished activity (rea: 19.98s, cpu: 575.3s, sys: 40.97s): ROUND 1
  Finished activity (rea: 19.98s, cpu: 575.3s, sys: 40.97s): FGL BLOC NEW METHOD
  
  ****	A (86547x 102826)	- nb elements 186092549 - density: 2.091 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 2499MB; VM: 9779MB)
  
Faugère-Lachartre Bloc Version (576.304 s)
Finished activity (rea: 21.75s, cpu: 576.3s, sys: 41.72s): Faugère-Lachartre Bloc Version
