Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04688MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/kat16/mat9
    328980 x 385905 matrix
    mod 65521
    Nb of Nz elements 1737728359 (density 1.36877% ) -	size 9944 MB
  Finished activity (rea: 24.09s, cpu: 8.464s, sys: 10.65s): done
  [[[Loading matrix]]]		 Memory (RSS: 9971MB; VM: 1.001e+04MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 27.85s, cpu: 27.62s, sys: 0.2s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 1.04e+04MB; VM: 1.043e+04MB)
      
      Pivots found: 325366
      
      ****	sub_B (325366x 60539)	- nb elements 1216746663 - density: 6.177 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 3614x 60539)	- nb elements 52399179 - density: 23.95 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (325366x 325366)	- nb elements 231461258 - density: 0.2186 % 	 bloc dim   ****
      ****	sub_C_multiline ( 3614x 325366)	- nb elements 78373351 - density: 6.665 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 1
      Finished activity (rea: 1521s, cpu: 1518s, sys: 1.292s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 13.23s, cpu: 12.27s, sys: 0.944s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 1.234e+04MB; VM: 1.245e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 1
      Finished activity (rea: 1.042e+04s, cpu: 1.041e+04s, sys: 0.66s): [D = D - C*B]
      ****	sub_D ( 3614x 60539)	- nb elements 209536137 - density: 95.77 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 1.266e+04MB; VM: 1.277e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 1
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 1.717s, u: 1.692s, s: 0.024s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 316.4s, cpu: 315.4s, sys: 0.676s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 1.234e+04MB; VM: 1.245e+04MB)
      
      Rank of D 3614
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.05824s, cpu: 0.056s, sys: 0s): done
      Pivots found: 3614
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.001321s, cpu: 0.004s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 26.46s, cpu: 24.42s, sys: 2.012s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 1.762e+04MB; VM: 1.934e+04MB)
      
      True rank 328980
    Finished activity (rea: 1.233e+04s, cpu: 1.231e+04s, sys: 5.784s): ROUND 1
  Finished activity (rea: 1.233e+04s, cpu: 1.231e+04s, sys: 5.784s): FGL BLOC NEW METHOD
  
  ****	A (328980x 385905)	- nb elements 1781403366 - density: 1.403 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 1.762e+04MB; VM: 1.934e+04MB)
  
Faugère-Lachartre Bloc Version (12322.2 s)
Finished activity (rea: 1.236e+04s, cpu: 1.232e+04s, sys: 16.43s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04688MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/kat16/mat9
    328980 x 385905 matrix
    mod 65521
    Nb of Nz elements 1737728359 (density 1.36877% ) -	size 9944 MB
  Finished activity (rea: 13.17s, cpu: 7.104s, sys: 6.048s): done
  [[[Loading matrix]]]		 Memory (RSS: 9971MB; VM: 1.001e+04MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 18.36s, cpu: 31.45s, sys: 4.988s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 1.438e+04MB; VM: 1.471e+04MB)
      
      Pivots found: 325366
      
      ****	sub_B (325366x 60539)	- nb elements 1216746663 - density: 6.177 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 3614x 60539)	- nb elements 52399179 - density: 23.95 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (325366x 325366)	- nb elements 231461258 - density: 0.2186 % 	 bloc dim   ****
      ****	sub_C_multiline ( 3614x 325366)	- nb elements 78373351 - density: 6.665 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 2
      Finished activity (rea: 718.8s, cpu: 1435s, sys: 1.512s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 9.649s, cpu: 17.44s, sys: 1.464s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 1.545e+04MB; VM: 1.581e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 2
      Finished activity (rea: 5185s, cpu: 1.036e+04s, sys: 0.528s): [D = D - C*B]
      ****	sub_D ( 3614x 60539)	- nb elements 209536137 - density: 95.77 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 1.577e+04MB; VM: 1.607e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 2
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 1.485s, u: 2.48s, s: 0.232s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 158s, cpu: 313.7s, sys: 1.716s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 1.452e+04MB; VM: 1.492e+04MB)
      
      Rank of D 3614
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.06159s, cpu: 0.06s, sys: 0s): done
      Pivots found: 3614
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.001387s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 28.31s, cpu: 51.19s, sys: 3.16s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 1.882e+04MB; VM: 2.034e+04MB)
      
      True rank 328980
    Finished activity (rea: 6123s, cpu: 1.221e+04s, sys: 13.37s): ROUND 1
  Finished activity (rea: 6123s, cpu: 1.221e+04s, sys: 13.37s): FGL BLOC NEW METHOD
  
  ****	A (328980x 385905)	- nb elements 1781403366 - density: 1.403 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 1.882e+04MB; VM: 2.034e+04MB)
  
Faugère-Lachartre Bloc Version (12218 s)
Finished activity (rea: 6136s, cpu: 1.222e+04s, sys: 19.42s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04688MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/kat16/mat9
    328980 x 385905 matrix
    mod 65521
    Nb of Nz elements 1737728359 (density 1.36877% ) -	size 9944 MB
  Finished activity (rea: 14.54s, cpu: 7.116s, sys: 7.412s): done
  [[[Loading matrix]]]		 Memory (RSS: 9971MB; VM: 1.001e+04MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 15.08s, cpu: 37.4s, sys: 12.76s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 1.568e+04MB; VM: 1.616e+04MB)
      
      Pivots found: 325366
      
      ****	sub_B (325366x 60539)	- nb elements 1216746663 - density: 6.177 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 3614x 60539)	- nb elements 52399179 - density: 23.95 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (325366x 325366)	- nb elements 231461258 - density: 0.2186 % 	 bloc dim   ****
      ****	sub_C_multiline ( 3614x 325366)	- nb elements 78373351 - density: 6.665 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 4
      Finished activity (rea: 339.9s, cpu: 1357s, sys: 0.908s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 7.212s, cpu: 17.14s, sys: 4.844s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 1.724e+04MB; VM: 1.792e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 4
      Finished activity (rea: 2605s, cpu: 1.034e+04s, sys: 0.328s): [D = D - C*B]
      ****	sub_D ( 3614x 60539)	- nb elements 209536137 - density: 95.77 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 1.756e+04MB; VM: 1.817e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 4
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.8442s, u: 2.32s, s: 0.304s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 78.42s, cpu: 310.2s, sys: 1.764s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 1.579e+04MB; VM: 1.645e+04MB)
      
      Rank of D 3614
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.0348s, cpu: 0.036s, sys: 0s): done
      Pivots found: 3614
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0006349s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 22.47s, cpu: 76.82s, sys: 6.032s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 2.206e+04MB; VM: 2.426e+04MB)
      
      True rank 328980
    Finished activity (rea: 3073s, cpu: 1.214e+04s, sys: 26.63s): ROUND 1
  Finished activity (rea: 3073s, cpu: 1.214e+04s, sys: 26.63s): FGL BLOC NEW METHOD
  
  ****	A (328980x 385905)	- nb elements 1781403366 - density: 1.403 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 2.206e+04MB; VM: 2.426e+04MB)
  
Faugère-Lachartre Bloc Version (12146.8 s)
Finished activity (rea: 3087s, cpu: 1.215e+04s, sys: 34.04s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/kat16/mat9
    328980 x 385905 matrix
    mod 65521
    Nb of Nz elements 1737728359 (density 1.36877% ) -	size 9944 MB
  Finished activity (rea: 16.48s, cpu: 7.176s, sys: 9.288s): done
  [[[Loading matrix]]]		 Memory (RSS: 9971MB; VM: 1.001e+04MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 18.09s, cpu: 50.45s, sys: 22.84s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 1.534e+04MB; VM: 1.605e+04MB)
      
      Pivots found: 325366
      
      ****	sub_B (325366x 60539)	- nb elements 1216746663 - density: 6.177 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 3614x 60539)	- nb elements 52399179 - density: 23.95 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (325366x 325366)	- nb elements 231461258 - density: 0.2186 % 	 bloc dim   ****
      ****	sub_C_multiline ( 3614x 325366)	- nb elements 78373351 - density: 6.665 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 8
      Finished activity (rea: 181.4s, cpu: 1449s, sys: 0.328s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 11.07s, cpu: 29.5s, sys: 11.03s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 1.717e+04MB; VM: 1.819e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 8
      Finished activity (rea: 1386s, cpu: 1.092e+04s, sys: 0.232s): [D = D - C*B]
      ****	sub_D ( 3614x 60539)	- nb elements 209536137 - density: 95.77 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 1.749e+04MB; VM: 1.819e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 8
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.5205s, u: 2.184s, s: 0.384s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 41.92s, cpu: 329.4s, sys: 2.584s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 1.554e+04MB; VM: 1.666e+04MB)
      
      Rank of D 3614
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.03456s, cpu: 0.036s, sys: 0s): done
      Pivots found: 3614
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0006471s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 15.8s, cpu: 79.23s, sys: 21.15s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 2.408e+04MB; VM: 2.709e+04MB)
      
      True rank 328980
    Finished activity (rea: 1660s, cpu: 1.286e+04s, sys: 58.17s): ROUND 1
  Finished activity (rea: 1660s, cpu: 1.286e+04s, sys: 58.17s): FGL BLOC NEW METHOD
  
  ****	A (328980x 385905)	- nb elements 1781403366 - density: 1.403 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 2.408e+04MB; VM: 2.709e+04MB)
  
Faugère-Lachartre Bloc Version (12865.8 s)
Finished activity (rea: 1676s, cpu: 1.287e+04s, sys: 67.46s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/kat16/mat9
    328980 x 385905 matrix
    mod 65521
    Nb of Nz elements 1737728359 (density 1.36877% ) -	size 9944 MB
  Finished activity (rea: 16.7s, cpu: 7.392s, sys: 9.288s): done
  [[[Loading matrix]]]		 Memory (RSS: 9971MB; VM: 1.001e+04MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 19.94s, cpu: 59.93s, sys: 30.94s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 1.54e+04MB; VM: 1.636e+04MB)
      
      Pivots found: 325366
      
      ****	sub_B (325366x 60539)	- nb elements 1216746663 - density: 6.177 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 3614x 60539)	- nb elements 52399179 - density: 23.95 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (325366x 325366)	- nb elements 231461258 - density: 0.2186 % 	 bloc dim   ****
      ****	sub_C_multiline ( 3614x 325366)	- nb elements 78373351 - density: 6.665 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 16
      Finished activity (rea: 115.4s, cpu: 1841s, sys: 0.476s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 10.93s, cpu: 34.14s, sys: 14.4s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 1.733e+04MB; VM: 1.902e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 16
      Finished activity (rea: 706.5s, cpu: 1.085e+04s, sys: 0.2s): [D = D - C*B]
      ****	sub_D ( 3614x 60539)	- nb elements 209536137 - density: 95.77 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 1.766e+04MB; VM: 1.902e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 16
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.4328s, u: 2.488s, s: 0.528s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 23.06s, cpu: 354.9s, sys: 3.244s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 1.566e+04MB; VM: 1.78e+04MB)
      
      Rank of D 3614
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.03771s, cpu: 0.036s, sys: 0s): done
      Pivots found: 3614
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0006449s, cpu: 0.004s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 13.3s, cpu: 76.8s, sys: 68.05s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 2.492e+04MB; VM: 2.888e+04MB)
      
      True rank 328980
    Finished activity (rea: 894.3s, cpu: 1.322e+04s, sys: 117.3s): ROUND 1
  Finished activity (rea: 894.3s, cpu: 1.322e+04s, sys: 117.3s): FGL BLOC NEW METHOD
  
  ****	A (328980x 385905)	- nb elements 1781403366 - density: 1.403 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 2.492e+04MB; VM: 2.888e+04MB)
  
Faugère-Lachartre Bloc Version (13228 s)
Finished activity (rea: 911.1s, cpu: 1.323e+04s, sys: 126.6s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/kat16/mat9
    328980 x 385905 matrix
    mod 65521
    Nb of Nz elements 1737728359 (density 1.36877% ) -	size 9944 MB
  Finished activity (rea: 16.39s, cpu: 7.144s, sys: 9.228s): done
  [[[Loading matrix]]]		 Memory (RSS: 9971MB; VM: 1.001e+04MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 20.15s, cpu: 59.8s, sys: 35.03s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 1.581e+04MB; VM: 1.726e+04MB)
      
      Pivots found: 325366
      
      ****	sub_B (325366x 60539)	- nb elements 1216746663 - density: 6.177 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 3614x 60539)	- nb elements 52399179 - density: 23.95 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (325366x 325366)	- nb elements 231461258 - density: 0.2186 % 	 bloc dim   ****
      ****	sub_C_multiline ( 3614x 325366)	- nb elements 78373351 - density: 6.665 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 32
      Finished activity (rea: 94.55s, cpu: 3010s, sys: 1.704s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 11.2s, cpu: 36.36s, sys: 13.68s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 1.779e+04MB; VM: 2.119e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 32
      Finished activity (rea: 410.6s, cpu: 1.179e+04s, sys: 0.188s): [D = D - C*B]
      ****	sub_D ( 3614x 60539)	- nb elements 209536137 - density: 95.77 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 1.81e+04MB; VM: 2.119e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 32
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.4546s, u: 2.78s, s: 0.836s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 20.38s, cpu: 584.1s, sys: 22.22s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 1.682e+04MB; VM: 2.004e+04MB)
      
      Rank of D 3614
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.03696s, cpu: 0.036s, sys: 0s): done
      Pivots found: 3614
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0007288s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 14.53s, cpu: 96.39s, sys: 142.5s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 2.514e+04MB; VM: 3.073e+04MB)
      
      True rank 328980
    Finished activity (rea: 576.8s, cpu: 1.558e+04s, sys: 215.3s): ROUND 1
  Finished activity (rea: 576.8s, cpu: 1.558e+04s, sys: 215.3s): FGL BLOC NEW METHOD
  
  ****	A (328980x 385905)	- nb elements 1781403366 - density: 1.403 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 2.514e+04MB; VM: 3.073e+04MB)
  
Faugère-Lachartre Bloc Version (15589 s)
Finished activity (rea: 593.3s, cpu: 1.559e+04s, sys: 224.6s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/kat16/mat9
    328980 x 385905 matrix
    mod 65521
    Nb of Nz elements 1737728359 (density 1.36877% ) -	size 9944 MB
  Finished activity (rea: 16.88s, cpu: 7.304s, sys: 9.56s): done
  [[[Loading matrix]]]		 Memory (RSS: 9971MB; VM: 1.001e+04MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 24.6s, cpu: 59.71s, sys: 38.15s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 1.489e+04MB; VM: 1.796e+04MB)
      
      Pivots found: 325366
      
      ****	sub_B (325366x 60539)	- nb elements 1216746663 - density: 6.177 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 3614x 60539)	- nb elements 52399179 - density: 23.95 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (325366x 325366)	- nb elements 231461258 - density: 0.2186 % 	 bloc dim   ****
      ****	sub_C_multiline ( 3614x 325366)	- nb elements 78373351 - density: 6.665 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 64
      Finished activity (rea: 125s, cpu: 7919s, sys: 5.352s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 17.77s, cpu: 41.74s, sys: 22.28s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 1.709e+04MB; VM: 2.343e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 64
      Finished activity (rea: 308.3s, cpu: 1.66e+04s, sys: 0.36s): [D = D - C*B]
      ****	sub_D ( 3614x 60539)	- nb elements 209536137 - density: 95.77 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 1.718e+04MB; VM: 2.318e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 64
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.5129s, u: 3.032s, s: 1.424s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 37.43s, cpu: 1816s, sys: 346s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 1.629e+04MB; VM: 2.299e+04MB)
      
      Rank of D 3614
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.03994s, cpu: 0.04s, sys: 0s): done
      Pivots found: 3614
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.001407s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 21.41s, cpu: 121.3s, sys: 325.8s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 2.408e+04MB; VM: 3.227e+04MB)
      
      True rank 328980
    Finished activity (rea: 539.8s, cpu: 2.657e+04s, sys: 738s): ROUND 1
  Finished activity (rea: 539.8s, cpu: 2.657e+04s, sys: 738s): FGL BLOC NEW METHOD
  
  ****	A (328980x 385905)	- nb elements 1781403366 - density: 1.403 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 2.408e+04MB; VM: 3.227e+04MB)
  
Faugère-Lachartre Bloc Version (26574.5 s)
Finished activity (rea: 556.9s, cpu: 2.657e+04s, sys: 747.6s): Faugère-Lachartre Bloc Version
