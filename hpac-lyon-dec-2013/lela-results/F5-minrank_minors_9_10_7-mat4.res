Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/minrank_minors_9_10_7/mat4
    36448 x 108178 matrix
    mod 65521
    Nb of Nz elements 1153881815 (density 29.265% ) -	size 6602 MB
  Finished activity (rea: 17.26s, cpu: 5.512s, sys: 7.832s): done
  [[[Loading matrix]]]		 Memory (RSS: 6608MB; VM: 6644MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 10.79s, cpu: 18.98s, sys: 2.404s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 8610MB; VM: 8721MB)
      
      Pivots found: 34053
      
      ****	sub_B (34053x 74125)	- nb elements 982615684 - density: 38.93 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 2395x 74125)	- nb elements 88355598 - density: 49.77 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (34053x 34053)	- nb elements 20480046 - density: 1.766 % 	 bloc dim   ****
      ****	sub_C_multiline ( 2395x 34053)	- nb elements 29693916 - density: 36.41 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 2
      Finished activity (rea: 43.8s, cpu: 87.42s, sys: 0.084s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 1.023s, cpu: 2.028s, sys: 0.008s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 8624MB; VM: 8865MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 2
      Finished activity (rea: 2942s, cpu: 5877s, sys: 0.204s): [D = D - C*B]
      ****	sub_D ( 2395x 74125)	- nb elements 177526647 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 8669MB; VM: 8865MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 2
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 1.129s, u: 1.944s, s: 0.132s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 90.41s, cpu: 178.6s, sys: 1.768s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 8761MB; VM: 9121MB)
      
      Rank of D 2395
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.006553s, cpu: 0.004s, sys: 0s): done
      Pivots found: 2395
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0003901s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 25.03s, cpu: 48.18s, sys: 1.516s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 1.147e+04MB; VM: 1.264e+04MB)
      
      True rank 36448
    Finished activity (rea: 3115s, cpu: 6215s, sys: 5.984s): ROUND 1
  Finished activity (rea: 3115s, cpu: 6215s, sys: 5.984s): FGL BLOC NEW METHOD
  
  ****	A (36448x 108178)	- nb elements 1198133781 - density: 30.39 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 1.147e+04MB; VM: 1.264e+04MB)
  
Faugère-Lachartre Bloc Version (6220.02 s)
Finished activity (rea: 3132s, cpu: 6220s, sys: 13.82s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04688MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/minrank_minors_9_10_7/mat4
    36448 x 108178 matrix
    mod 65521
    Nb of Nz elements 1153881815 (density 29.265% ) -	size 6602 MB
  Finished activity (rea: 8.732s, cpu: 4.876s, sys: 3.848s): done
  [[[Loading matrix]]]		 Memory (RSS: 6608MB; VM: 6644MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 8.651s, cpu: 21.16s, sys: 7.304s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 9106MB; VM: 9313MB)
      
      Pivots found: 34053
      
      ****	sub_B (34053x 74125)	- nb elements 982615684 - density: 38.93 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 2395x 74125)	- nb elements 88355598 - density: 49.77 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (34053x 34053)	- nb elements 20480046 - density: 1.766 % 	 bloc dim   ****
      ****	sub_C_multiline ( 2395x 34053)	- nb elements 29693916 - density: 36.41 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 4
      Finished activity (rea: 19.76s, cpu: 78.92s, sys: 0s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.3622s, cpu: 1.404s, sys: 0s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 9139MB; VM: 9601MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 4
      Finished activity (rea: 1470s, cpu: 5868s, sys: 0s): [D = D - C*B]
      ****	sub_D ( 2395x 74125)	- nb elements 177526647 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 9174MB; VM: 9601MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 4
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 1.003s, u: 2.912s, s: 0.3s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 45.35s, cpu: 178.2s, sys: 1.872s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 9528MB; VM: 1.018e+04MB)
      
      Rank of D 2395
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.003635s, cpu: 0s, sys: 0s): done
      Pivots found: 2395
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0002339s, cpu: 0.004s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 11.95s, cpu: 42.57s, sys: 2.948s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 1.407e+04MB; VM: 1.587e+04MB)
      
      True rank 36448
    Finished activity (rea: 1559s, cpu: 6192s, sys: 12.12s): ROUND 1
  Finished activity (rea: 1559s, cpu: 6192s, sys: 12.12s): FGL BLOC NEW METHOD
  
  ****	A (36448x 108178)	- nb elements 1198133781 - density: 30.39 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 1.407e+04MB; VM: 1.587e+04MB)
  
Faugère-Lachartre Bloc Version (6197.27 s)
Finished activity (rea: 1568s, cpu: 6197s, sys: 15.97s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/minrank_minors_9_10_7/mat4
    36448 x 108178 matrix
    mod 65521
    Nb of Nz elements 1153881815 (density 29.265% ) -	size 6602 MB
  Finished activity (rea: 9.5s, cpu: 4.64s, sys: 4.852s): done
  [[[Loading matrix]]]		 Memory (RSS: 6608MB; VM: 6644MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 11.09s, cpu: 30.7s, sys: 12.68s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 9056MB; VM: 9473MB)
      
      Pivots found: 34053
      
      ****	sub_B (34053x 74125)	- nb elements 982615684 - density: 38.93 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 2395x 74125)	- nb elements 88355598 - density: 49.77 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (34053x 34053)	- nb elements 20480046 - density: 1.766 % 	 bloc dim   ****
      ****	sub_C_multiline ( 2395x 34053)	- nb elements 29693916 - density: 36.41 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 8
      Finished activity (rea: 10.3s, cpu: 82.47s, sys: 0s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.2699s, cpu: 1.752s, sys: 0s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 9127MB; VM: 1.008e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 8
      Finished activity (rea: 750.3s, cpu: 5944s, sys: 0s): [D = D - C*B]
      ****	sub_D ( 2395x 74125)	- nb elements 177526647 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 9128MB; VM: 1.002e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 8
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.3648s, u: 1.804s, s: 0.144s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 23.58s, cpu: 184s, sys: 1.896s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 9508MB; VM: 1.059e+04MB)
      
      Rank of D 2395
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.003257s, cpu: 0s, sys: 0.004s): done
      Pivots found: 2395
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.000196s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 9.987s, cpu: 69.39s, sys: 5.04s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 1.531e+04MB; VM: 1.776e+04MB)
      
      True rank 36448
    Finished activity (rea: 808s, cpu: 6315s, sys: 19.62s): ROUND 1
  Finished activity (rea: 808s, cpu: 6315s, sys: 19.62s): FGL BLOC NEW METHOD
  
  ****	A (36448x 108178)	- nb elements 1198133781 - density: 30.39 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 1.531e+04MB; VM: 1.776e+04MB)
  
Faugère-Lachartre Bloc Version (6319.44 s)
Finished activity (rea: 817.5s, cpu: 6319s, sys: 24.48s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/minrank_minors_9_10_7/mat4
    36448 x 108178 matrix
    mod 65521
    Nb of Nz elements 1153881815 (density 29.265% ) -	size 6602 MB
  Finished activity (rea: 10.68s, cpu: 4.62s, sys: 6.048s): done
  [[[Loading matrix]]]		 Memory (RSS: 6608MB; VM: 6644MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 12.24s, cpu: 34.58s, sys: 15.73s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 9384MB; VM: 1.011e+04MB)
      
      Pivots found: 34053
      
      ****	sub_B (34053x 74125)	- nb elements 982615684 - density: 38.93 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 2395x 74125)	- nb elements 88355598 - density: 49.77 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (34053x 34053)	- nb elements 20480046 - density: 1.766 % 	 bloc dim   ****
      ****	sub_C_multiline ( 2395x 34053)	- nb elements 29693916 - density: 36.41 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 16
      Finished activity (rea: 5.646s, cpu: 90.36s, sys: 0s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.3908s, cpu: 2.924s, sys: 0s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 9510MB; VM: 1.117e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 16
      Finished activity (rea: 394.4s, cpu: 6174s, sys: 0s): [D = D - C*B]
      ****	sub_D ( 2395x 74125)	- nb elements 177526647 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 9508MB; VM: 1.117e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 16
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.4154s, u: 2.388s, s: 0.316s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 15.14s, cpu: 224.2s, sys: 4.404s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 9766MB; VM: 1.194e+04MB)
      
      Rank of D 2395
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.003278s, cpu: 0.004s, sys: 0s): done
      Pivots found: 2395
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0001969s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 5.77s, cpu: 60.19s, sys: 11.58s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 1.626e+04MB; VM: 1.981e+04MB)
      
      True rank 36448
    Finished activity (rea: 435.9s, cpu: 6588s, sys: 31.72s): ROUND 1
  Finished activity (rea: 435.9s, cpu: 6588s, sys: 31.72s): FGL BLOC NEW METHOD
  
  ****	A (36448x 108178)	- nb elements 1198133781 - density: 30.39 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 1.626e+04MB; VM: 1.981e+04MB)
  
Faugère-Lachartre Bloc Version (6592.67 s)
Finished activity (rea: 446.6s, cpu: 6593s, sys: 37.77s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/minrank_minors_9_10_7/mat4
    36448 x 108178 matrix
    mod 65521
    Nb of Nz elements 1153881815 (density 29.265% ) -	size 6602 MB
  Finished activity (rea: 11.04s, cpu: 4.916s, sys: 6.116s): done
  [[[Loading matrix]]]		 Memory (RSS: 6608MB; VM: 6644MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 13.6s, cpu: 38.1s, sys: 19.18s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 9644MB; VM: 1.095e+04MB)
      
      Pivots found: 34053
      
      ****	sub_B (34053x 74125)	- nb elements 982615684 - density: 38.93 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 2395x 74125)	- nb elements 88355598 - density: 49.77 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (34053x 34053)	- nb elements 20480046 - density: 1.766 % 	 bloc dim   ****
      ****	sub_C_multiline ( 2395x 34053)	- nb elements 29693916 - density: 36.41 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 32
      Finished activity (rea: 3.845s, cpu: 122.1s, sys: 0s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.7965s, cpu: 4.1s, sys: 0.584s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 9816MB; VM: 1.354e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 32
      Finished activity (rea: 224.6s, cpu: 6647s, sys: 0s): [D = D - C*B]
      ****	sub_D ( 2395x 74125)	- nb elements 177526647 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 9781MB; VM: 1.322e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 32
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.4754s, u: 2.656s, s: 0.552s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 17.46s, cpu: 450s, sys: 54.04s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 9997MB; VM: 1.379e+04MB)
      
      Rank of D 2395
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.003424s, cpu: 0s, sys: 0.004s): done
      Pivots found: 2395
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0002301s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 6.688s, cpu: 49.99s, sys: 71.2s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 1.634e+04MB; VM: 2.141e+04MB)
      
      True rank 36448
    Finished activity (rea: 269.6s, cpu: 7314s, sys: 145s): ROUND 1
  Finished activity (rea: 269.6s, cpu: 7314s, sys: 145s): FGL BLOC NEW METHOD
  
  ****	A (36448x 108178)	- nb elements 1198133781 - density: 30.39 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 1.634e+04MB; VM: 2.141e+04MB)
  
Faugère-Lachartre Bloc Version (7319.28 s)
Finished activity (rea: 280.7s, cpu: 7319s, sys: 151.1s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/minrank_minors_9_10_7/mat4
    36448 x 108178 matrix
    mod 65521
    Nb of Nz elements 1153881815 (density 29.265% ) -	size 6602 MB
  Finished activity (rea: 9.819s, cpu: 4.972s, sys: 4.836s): done
  [[[Loading matrix]]]		 Memory (RSS: 6608MB; VM: 6644MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 20.53s, cpu: 41.68s, sys: 29.21s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 9813MB; VM: 1.248e+04MB)
      
      Pivots found: 34053
      
      ****	sub_B (34053x 74125)	- nb elements 982615684 - density: 38.93 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 2395x 74125)	- nb elements 88355598 - density: 49.77 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (34053x 34053)	- nb elements 20480046 - density: 1.766 % 	 bloc dim   ****
      ****	sub_C_multiline ( 2395x 34053)	- nb elements 29693916 - density: 36.41 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 64
      Finished activity (rea: 3.024s, cpu: 190.2s, sys: 0s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 1.65s, cpu: 6.368s, sys: 0.712s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 1.002e+04MB; VM: 1.68e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 64
      Finished activity (rea: 169.7s, cpu: 9284s, sys: 0s): [D = D - C*B]
      ****	sub_D ( 2395x 74125)	- nb elements 177526647 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 1.002e+04MB; VM: 1.661e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 64
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.5956s, u: 2.692s, s: 0.824s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 26.23s, cpu: 1128s, sys: 313.6s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 1.039e+04MB; VM: 1.712e+04MB)
      
      Rank of D 2395
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.004555s, cpu: 0.004s, sys: 0s): done
      Pivots found: 2395
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0003111s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 9.572s, cpu: 84.59s, sys: 141.1s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 1.636e+04MB; VM: 2.455e+04MB)
      
      True rank 36448
    Finished activity (rea: 233.5s, cpu: 1.074e+04s, sys: 484.7s): ROUND 1
  Finished activity (rea: 233.5s, cpu: 1.074e+04s, sys: 484.7s): FGL BLOC NEW METHOD
  
  ****	A (36448x 108178)	- nb elements 1198133781 - density: 30.39 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 1.636e+04MB; VM: 2.455e+04MB)
  
Faugère-Lachartre Bloc Version (10742.4 s)
Finished activity (rea: 243.3s, cpu: 1.074e+04s, sys: 489.5s): Faugère-Lachartre Bloc Version
