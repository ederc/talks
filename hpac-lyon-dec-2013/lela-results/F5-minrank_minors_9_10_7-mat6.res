Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04688MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/minrank_minors_9_10_7/mat6
    73761 x 171221 matrix
    mod 65521
    Nb of Nz elements 3007506141 (density 23.8135% ) -	size 17209 MB
  Finished activity (rea: 48.4s, cpu: 15.48s, sys: 22.56s): done
  [[[Loading matrix]]]		 Memory (RSS: 1.722e+04MB; VM: 1.725e+04MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 22.77s, cpu: 56.42s, sys: 19.06s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 2.382e+04MB; VM: 2.421e+04MB)
      
      Pivots found: 69721
      
      ****	sub_B (69721x 101500)	- nb elements 2524367273 - density: 35.67 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 4040x 101500)	- nb elements 219074213 - density: 53.42 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (69721x 69721)	- nb elements 66708288 - density: 1.372 % 	 bloc dim   ****
      ****	sub_C_multiline ( 4040x 69721)	- nb elements 101897844 - density: 36.18 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 4
      Finished activity (rea: 111.5s, cpu: 445.6s, sys: 0s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 1.352s, cpu: 5.328s, sys: 0s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 2.396e+04MB; VM: 2.45e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 4
      Finished activity (rea: 6841s, cpu: 2.729e+04s, sys: 0s): [D = D - C*B]
      ****	sub_D ( 4040x 101500)	- nb elements 410053770 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 2.402e+04MB; VM: 2.45e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 4
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 1.346s, u: 4.764s, s: 0s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 168.7s, cpu: 669.6s, sys: 2.98s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 2.413e+04MB; VM: 2.469e+04MB)
      
      Rank of D 4040
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.01291s, cpu: 0.016s, sys: 0s): done
      Pivots found: 4040
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.00037s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 33.58s, cpu: 119.1s, sys: 8.732s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 3.602e+04MB; VM: 4.025e+04MB)
      
      True rank 73761
    Finished activity (rea: 7185s, cpu: 2.859e+04s, sys: 30.77s): ROUND 1
  Finished activity (rea: 7185s, cpu: 2.859e+04s, sys: 30.77s): FGL BLOC NEW METHOD
  
  ****	A (73761x 171221)	- nb elements 3059478090 - density: 24.22 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 3.602e+04MB; VM: 4.025e+04MB)
  
Faugère-Lachartre Bloc Version (28602.9 s)
Finished activity (rea: 7234s, cpu: 2.86e+04s, sys: 53.33s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/minrank_minors_9_10_7/mat6
    73761 x 171221 matrix
    mod 65521
    Nb of Nz elements 3007506141 (density 23.8135% ) -	size 17209 MB
  Finished activity (rea: 23.22s, cpu: 12.59s, sys: 10.6s): done
  [[[Loading matrix]]]		 Memory (RSS: 1.722e+04MB; VM: 1.725e+04MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 27.41s, cpu: 77.56s, sys: 31.99s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 2.334e+04MB; VM: 2.386e+04MB)
      
      Pivots found: 69721
      
      ****	sub_B (69721x 101500)	- nb elements 2524367273 - density: 35.67 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 4040x 101500)	- nb elements 219074213 - density: 53.42 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (69721x 69721)	- nb elements 66708288 - density: 1.372 % 	 bloc dim   ****
      ****	sub_C_multiline ( 4040x 69721)	- nb elements 101897844 - density: 36.18 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 8
      Finished activity (rea: 67.29s, cpu: 537.9s, sys: 0s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.9258s, cpu: 6.696s, sys: 0s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 2.356e+04MB; VM: 2.447e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 8
      Finished activity (rea: 3438s, cpu: 2.724e+04s, sys: 0s): [D = D - C*B]
      ****	sub_D ( 4040x 101500)	- nb elements 410053770 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 2.356e+04MB; VM: 2.441e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 8
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 1.134s, u: 6.332s, s: 0s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 85.08s, cpu: 672.3s, sys: 2.98s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 2.382e+04MB; VM: 2.479e+04MB)
      
      Rank of D 4040
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.01238s, cpu: 0.008s, sys: 0.004s): done
      Pivots found: 4040
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0003769s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 22.56s, cpu: 134.9s, sys: 21.17s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 3.874e+04MB; VM: 4.412e+04MB)
      
      True rank 73761
    Finished activity (rea: 3647s, cpu: 2.867e+04s, sys: 56.15s): ROUND 1
  Finished activity (rea: 3647s, cpu: 2.867e+04s, sys: 56.15s): FGL BLOC NEW METHOD
  
  ****	A (73761x 171221)	- nb elements 3059478090 - density: 24.22 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 3.874e+04MB; VM: 4.412e+04MB)
  
Faugère-Lachartre Bloc Version (28685.6 s)
Finished activity (rea: 3671s, cpu: 2.869e+04s, sys: 66.75s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/minrank_minors_9_10_7/mat6
    73761 x 171221 matrix
    mod 65521
    Nb of Nz elements 3007506141 (density 23.8135% ) -	size 17209 MB
  Finished activity (rea: 23.3s, cpu: 12.47s, sys: 10.8s): done
  [[[Loading matrix]]]		 Memory (RSS: 1.722e+04MB; VM: 1.725e+04MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 29.06s, cpu: 86.43s, sys: 41.11s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 2.4e+04MB; VM: 2.501e+04MB)
      
      Pivots found: 69721
      
      ****	sub_B (69721x 101500)	- nb elements 2524367273 - density: 35.67 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 4040x 101500)	- nb elements 219074213 - density: 53.42 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (69721x 69721)	- nb elements 66708288 - density: 1.372 % 	 bloc dim   ****
      ****	sub_C_multiline ( 4040x 69721)	- nb elements 101897844 - density: 36.18 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 16
      Finished activity (rea: 45.38s, cpu: 725.2s, sys: 0s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 1.433s, cpu: 10.14s, sys: 0s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 2.436e+04MB; VM: 2.62e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 16
      Finished activity (rea: 1801s, cpu: 2.83e+04s, sys: 0s): [D = D - C*B]
      ****	sub_D ( 4040x 101500)	- nb elements 410053770 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 2.425e+04MB; VM: 2.607e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 16
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.6004s, u: 6.544s, s: 0.28s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 50.32s, cpu: 776.9s, sys: 7.8s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 2.484e+04MB; VM: 2.677e+04MB)
      
      Rank of D 4040
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.01224s, cpu: 0.012s, sys: 0s): done
      Pivots found: 4040
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0003021s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 16.28s, cpu: 149.9s, sys: 57.35s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 4.093e+04MB; VM: 4.725e+04MB)
      
      True rank 73761
    Finished activity (rea: 1949s, cpu: 3.005e+04s, sys: 106.3s): ROUND 1
  Finished activity (rea: 1949s, cpu: 3.005e+04s, sys: 106.3s): FGL BLOC NEW METHOD
  
  ****	A (73761x 171221)	- nb elements 3059478090 - density: 24.22 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 4.093e+04MB; VM: 4.725e+04MB)
  
Faugère-Lachartre Bloc Version (30062.8 s)
Finished activity (rea: 1973s, cpu: 3.006e+04s, sys: 117.1s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04688MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/minrank_minors_9_10_7/mat6
    73761 x 171221 matrix
    mod 65521
    Nb of Nz elements 3007506141 (density 23.8135% ) -	size 17209 MB
  Finished activity (rea: 23.33s, cpu: 12.17s, sys: 11.14s): done
  [[[Loading matrix]]]		 Memory (RSS: 1.722e+04MB; VM: 1.725e+04MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 31.16s, cpu: 92.18s, sys: 48.49s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 2.442e+04MB; VM: 2.598e+04MB)
      
      Pivots found: 69721
      
      ****	sub_B (69721x 101500)	- nb elements 2524367273 - density: 35.67 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 4040x 101500)	- nb elements 219074213 - density: 53.42 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (69721x 69721)	- nb elements 66708288 - density: 1.372 % 	 bloc dim   ****
      ****	sub_C_multiline ( 4040x 69721)	- nb elements 101897844 - density: 36.18 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 32
      Finished activity (rea: 30.02s, cpu: 955.5s, sys: 0s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 2.771s, cpu: 13.94s, sys: 1.652s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 2.498e+04MB; VM: 2.844e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 32
      Finished activity (rea: 1024s, cpu: 3.113e+04s, sys: 0.032s): [D = D - C*B]
      ****	sub_D ( 4040x 101500)	- nb elements 410053770 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 2.475e+04MB; VM: 2.812e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 32
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.7113s, u: 5.832s, s: 1.276s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 58.09s, cpu: 1654s, sys: 133.7s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 2.488e+04MB; VM: 2.857e+04MB)
      
      Rank of D 4040
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.0127s, cpu: 0.012s, sys: 0s): done
      Pivots found: 4040
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0003681s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 15.89s, cpu: 139.9s, sys: 167.6s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 4.119e+04MB; VM: 4.924e+04MB)
      
      True rank 73761
    Finished activity (rea: 1168s, cpu: 3.4e+04s, sys: 351.5s): ROUND 1
  Finished activity (rea: 1168s, cpu: 3.4e+04s, sys: 351.5s): FGL BLOC NEW METHOD
  
  ****	A (73761x 171221)	- nb elements 3059478090 - density: 24.22 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 4.119e+04MB; VM: 4.924e+04MB)
  
Faugère-Lachartre Bloc Version (34008.7 s)
Finished activity (rea: 1191s, cpu: 3.401e+04s, sys: 362.6s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F5/minrank_minors_9_10_7/mat6
    73761 x 171221 matrix
    mod 65521
    Nb of Nz elements 3007506141 (density 23.8135% ) -	size 17209 MB
  Finished activity (rea: 23.42s, cpu: 12.54s, sys: 10.86s): done
  [[[Loading matrix]]]		 Memory (RSS: 1.722e+04MB; VM: 1.725e+04MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 40.84s, cpu: 92.82s, sys: 60.09s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 2.416e+04MB; VM: 2.719e+04MB)
      
      Pivots found: 69721
      
      ****	sub_B (69721x 101500)	- nb elements 2524367273 - density: 35.67 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 4040x 101500)	- nb elements 219074213 - density: 53.42 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (69721x 69721)	- nb elements 66708288 - density: 1.372 % 	 bloc dim   ****
      ****	sub_C_multiline ( 4040x 69721)	- nb elements 101897844 - density: 36.18 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 64
      Finished activity (rea: 30.8s, cpu: 1947s, sys: 0s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 5.123s, cpu: 15.73s, sys: 4.592s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 2.488e+04MB; VM: 3.209e+04MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 64
      Finished activity (rea: 740.7s, cpu: 4.34e+04s, sys: 0s): [D = D - C*B]
      ****	sub_D ( 4040x 101500)	- nb elements 410053770 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 2.452e+04MB; VM: 3.151e+04MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 64
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 1.14s, u: 5.288s, s: 5.184s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 90.99s, cpu: 5166s, sys: 329.4s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 2.447e+04MB; VM: 3.164e+04MB)
      
      Rank of D 4040
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.01229s, cpu: 0.012s, sys: 0s): done
      Pivots found: 4040
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 0.0003839s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 22.86s, cpu: 206.8s, sys: 361.4s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 3.999e+04MB; VM: 5.142e+04MB)
      
      True rank 73761
    Finished activity (rea: 937.7s, cpu: 5.084e+04s, sys: 755.5s): ROUND 1
  Finished activity (rea: 937.7s, cpu: 5.084e+04s, sys: 755.5s): FGL BLOC NEW METHOD
  
  ****	A (73761x 171221)	- nb elements 3059478090 - density: 24.22 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 3.999e+04MB; VM: 5.142e+04MB)
  
Faugère-Lachartre Bloc Version (50847.9 s)
Finished activity (rea: 961.1s, cpu: 5.085e+04s, sys: 766.4s): Faugère-Lachartre Bloc Version
