Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04688MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F4/kat12/mat6
    21182 x 22207 matrix
    mod 65521
    Nb of Nz elements 24006869 (density 5.10362% ) -	size 137 MB
  Finished activity (rea: 0.4478s, cpu: 0.204s, sys: 0.244s): done
  [[[Loading matrix]]]		 Memory (RSS: 141.1MB; VM: 177.5MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 0.5077s, cpu: 0.476s, sys: 0.032s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 181.6MB; VM: 225MB)
      
      Pivots found: 17915
      
      ****	sub_B (17915x 4292 )	- nb elements 9358872 - density: 12.17 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 3267x 4292 )	- nb elements 3704430 - density: 26.42 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (17915x 17915)	- nb elements 1407806 - density: 0.4386 % 	 bloc dim   ****
      ****	sub_C_multiline ( 3267x 17915)	- nb elements 7558395 - density: 12.91 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 1
      Finished activity (rea: 12.06s, cpu: 11.91s, sys: 0.132s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.9291s, cpu: 0.888s, sys: 0.04s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 239MB; VM: 347.1MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 1
      Finished activity (rea: 67.86s, cpu: 67.74s, sys: 0.048s): [D = D - C*B]
      ****	sub_D ( 3267x 4292 )	- nb elements 14021733 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 257.3MB; VM: 347.1MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 1
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.2091s, u: 0.204s, s: 0.004s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 3.696s, cpu: 3.676s, sys: 0.016s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 189.1MB; VM: 296.9MB)
      
      Rank of D 275
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.00015s, cpu: 0s, sys: 0s): done
      Pivots found: 275
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 8.512e-05s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 0.4398s, cpu: 0.44s, sys: 0s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 189.1MB; VM: 296.9MB)
      
      True rank 18190
    Finished activity (rea: 85.65s, cpu: 85.29s, sys: 0.268s): ROUND 1
  Finished activity (rea: 85.65s, cpu: 85.29s, sys: 0.268s): FGL BLOC NEW METHOD
  
  ****	A (21182x 22207)	- nb elements 12741575 - density: 2.709 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 189.1MB; VM: 296.9MB)
  
Faugère-Lachartre Bloc Version (85.492 s)
Finished activity (rea: 86.1s, cpu: 85.49s, sys: 0.512s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F4/kat12/mat6
    21182 x 22207 matrix
    mod 65521
    Nb of Nz elements 24006869 (density 5.10362% ) -	size 137 MB
  Finished activity (rea: 0.423s, cpu: 0.18s, sys: 0.24s): done
  [[[Loading matrix]]]		 Memory (RSS: 141.1MB; VM: 177.5MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 0.383s, cpu: 0.644s, sys: 0.08s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 192.7MB; VM: 250.9MB)
      
      Pivots found: 17915
      
      ****	sub_B (17915x 4292 )	- nb elements 9358872 - density: 12.17 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 3267x 4292 )	- nb elements 3704430 - density: 26.42 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (17915x 17915)	- nb elements 1407806 - density: 0.4386 % 	 bloc dim   ****
      ****	sub_C_multiline ( 3267x 17915)	- nb elements 7558395 - density: 12.91 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 2
      Finished activity (rea: 5.758s, cpu: 11.37s, sys: 0.128s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.7379s, cpu: 1.372s, sys: 0.096s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 239.4MB; VM: 458.9MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 2
      Finished activity (rea: 34.36s, cpu: 67.79s, sys: 0.036s): [D = D - C*B]
      ****	sub_D ( 3267x 4292 )	- nb elements 14021733 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 258.1MB; VM: 458.9MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 2
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.101s, u: 0.164s, s: 0.024s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 1.985s, cpu: 3.928s, sys: 0.024s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 196.2MB; VM: 394.9MB)
      
      Rank of D 275
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.0001519s, cpu: 0s, sys: 0s): done
      Pivots found: 275
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 8.893e-05s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 0.506s, cpu: 0.888s, sys: 0.084s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 209.6MB; VM: 458.9MB)
      
      True rank 18190
    Finished activity (rea: 43.85s, cpu: 86.12s, sys: 0.448s): ROUND 1
  Finished activity (rea: 43.85s, cpu: 86.12s, sys: 0.448s): FGL BLOC NEW METHOD
  
  ****	A (21182x 22207)	- nb elements 12741575 - density: 2.709 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 209.6MB; VM: 458.9MB)
  
Faugère-Lachartre Bloc Version (86.304 s)
Finished activity (rea: 44.27s, cpu: 86.3s, sys: 0.692s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04688MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F4/kat12/mat6
    21182 x 22207 matrix
    mod 65521
    Nb of Nz elements 24006869 (density 5.10362% ) -	size 137 MB
  Finished activity (rea: 0.3083s, cpu: 0.116s, sys: 0.192s): done
  [[[Loading matrix]]]		 Memory (RSS: 141.1MB; VM: 177.5MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 0.3018s, cpu: 0.716s, sys: 0.244s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 229MB; VM: 395MB)
      
      Pivots found: 17915
      
      ****	sub_B (17915x 4292 )	- nb elements 9358872 - density: 12.17 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 3267x 4292 )	- nb elements 3704430 - density: 26.42 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (17915x 17915)	- nb elements 1407806 - density: 0.4386 % 	 bloc dim   ****
      ****	sub_C_multiline ( 3267x 17915)	- nb elements 7558395 - density: 12.91 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 4
      Finished activity (rea: 2.763s, cpu: 10.84s, sys: 0.18s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.3231s, cpu: 1.012s, sys: 0.164s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 292.8MB; VM: 683.1MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 4
      Finished activity (rea: 18.05s, cpu: 67.54s, sys: 0.016s): [D = D - C*B]
      ****	sub_D ( 3267x 4292 )	- nb elements 14021733 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 312.2MB; VM: 683.1MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 4
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.0584s, u: 0.176s, s: 0.032s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 1.199s, cpu: 4.724s, sys: 0.044s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 201.8MB; VM: 683.1MB)
      
      Rank of D 275
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.000104s, cpu: 0s, sys: 0s): done
      Pivots found: 275
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 4.601e-05s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 0.2196s, cpu: 0.744s, sys: 0.064s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 242.6MB; VM: 683.1MB)
      
      True rank 18190
    Finished activity (rea: 22.99s, cpu: 85.72s, sys: 0.712s): ROUND 1
  Finished activity (rea: 22.99s, cpu: 85.72s, sys: 0.712s): FGL BLOC NEW METHOD
  
  ****	A (21182x 22207)	- nb elements 12741575 - density: 2.709 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 242.6MB; VM: 683.1MB)
  
Faugère-Lachartre Bloc Version (85.836 s)
Finished activity (rea: 23.3s, cpu: 85.84s, sys: 0.904s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04688MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F4/kat12/mat6
    21182 x 22207 matrix
    mod 65521
    Nb of Nz elements 24006869 (density 5.10362% ) -	size 137 MB
  Finished activity (rea: 0.2519s, cpu: 0.1s, sys: 0.152s): done
  [[[Loading matrix]]]		 Memory (RSS: 141.1MB; VM: 177.5MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 0.3171s, cpu: 0.84s, sys: 0.436s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 246.2MB; VM: 683.2MB)
      
      Pivots found: 17915
      
      ****	sub_B (17915x 4292 )	- nb elements 9358872 - density: 12.17 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 3267x 4292 )	- nb elements 3704430 - density: 26.42 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (17915x 17915)	- nb elements 1407806 - density: 0.4386 % 	 bloc dim   ****
      ****	sub_C_multiline ( 3267x 17915)	- nb elements 7558395 - density: 12.91 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 8
      Finished activity (rea: 1.589s, cpu: 12.46s, sys: 0.184s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.2994s, cpu: 1.352s, sys: 0.3s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 319.1MB; VM: 1227MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 8
      Finished activity (rea: 9.792s, cpu: 69.24s, sys: 0.036s): [D = D - C*B]
      ****	sub_D ( 3267x 4292 )	- nb elements 14021733 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 327.6MB; VM: 1227MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 8
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.06301s, u: 0.28s, s: 0.06s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 1.17s, cpu: 9.076s, sys: 0.08s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 197.9MB; VM: 1227MB)
      
      Rank of D 275
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.0001321s, cpu: 0s, sys: 0s): done
      Pivots found: 275
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 6.509e-05s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 0.1084s, cpu: 0.632s, sys: 0.048s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 261.6MB; VM: 1227MB)
      
      True rank 18190
    Finished activity (rea: 13.46s, cpu: 93.8s, sys: 1.092s): ROUND 1
  Finished activity (rea: 13.46s, cpu: 93.8s, sys: 1.092s): FGL BLOC NEW METHOD
  
  ****	A (21182x 22207)	- nb elements 12741575 - density: 2.709 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 261.6MB; VM: 1227MB)
  
Faugère-Lachartre Bloc Version (93.904 s)
Finished activity (rea: 13.71s, cpu: 93.9s, sys: 1.244s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04688MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F4/kat12/mat6
    21182 x 22207 matrix
    mod 65521
    Nb of Nz elements 24006869 (density 5.10362% ) -	size 137 MB
  Finished activity (rea: 0.4321s, cpu: 0.204s, sys: 0.228s): done
  [[[Loading matrix]]]		 Memory (RSS: 141.1MB; VM: 177.5MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 0.5412s, cpu: 1.3s, sys: 1s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 287.3MB; VM: 1259MB)
      
      Pivots found: 17915
      
      ****	sub_B (17915x 4292 )	- nb elements 9358872 - density: 12.17 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 3267x 4292 )	- nb elements 3704430 - density: 26.42 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (17915x 17915)	- nb elements 1407806 - density: 0.4386 % 	 bloc dim   ****
      ****	sub_C_multiline ( 3267x 17915)	- nb elements 7558395 - density: 12.91 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 16
      Finished activity (rea: 0.9374s, cpu: 14.41s, sys: 0.312s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.2254s, cpu: 2.068s, sys: 0.212s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 348.1MB; VM: 2315MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 16
      Finished activity (rea: 7.008s, cpu: 73.62s, sys: 0.012s): [D = D - C*B]
      ****	sub_D ( 3267x 4292 )	- nb elements 14021733 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 358.6MB; VM: 2315MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 16
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.04045s, u: 0.352s, s: 0.048s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 2.707s, cpu: 42.48s, sys: 0.08s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 210.1MB; VM: 2315MB)
      
      Rank of D 275
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.0001018s, cpu: 0s, sys: 0s): done
      Pivots found: 275
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 5.698e-05s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 0.09302s, cpu: 0.82s, sys: 0.2s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 275.7MB; VM: 2315MB)
      
      True rank 18190
    Finished activity (rea: 11.72s, cpu: 135s, sys: 1.828s): ROUND 1
  Finished activity (rea: 11.72s, cpu: 135s, sys: 1.828s): FGL BLOC NEW METHOD
  
  ****	A (21182x 22207)	- nb elements 12741575 - density: 2.709 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 275.7MB; VM: 2315MB)
  
Faugère-Lachartre Bloc Version (135.16 s)
Finished activity (rea: 12.15s, cpu: 135.2s, sys: 2.056s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F4/kat12/mat6
    21182 x 22207 matrix
    mod 65521
    Nb of Nz elements 24006869 (density 5.10362% ) -	size 137 MB
  Finished activity (rea: 0.2591s, cpu: 0.136s, sys: 0.124s): done
  [[[Loading matrix]]]		 Memory (RSS: 141.1MB; VM: 177.5MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 0.5026s, cpu: 1.508s, sys: 0.816s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 290.4MB; VM: 2411MB)
      
      Pivots found: 17915
      
      ****	sub_B (17915x 4292 )	- nb elements 9358872 - density: 12.17 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 3267x 4292 )	- nb elements 3704430 - density: 26.42 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (17915x 17915)	- nb elements 1407806 - density: 0.4386 % 	 bloc dim   ****
      ****	sub_C_multiline ( 3267x 17915)	- nb elements 7558395 - density: 12.91 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 32
      Finished activity (rea: 0.6953s, cpu: 20.78s, sys: 0.44s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 0.5368s, cpu: 5.124s, sys: 0.612s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 391.2MB; VM: 4491MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 32
      Finished activity (rea: 7.291s, cpu: 77.04s, sys: 0.064s): [D = D - C*B]
      ****	sub_D ( 3267x 4292 )	- nb elements 14021733 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 389.2MB; VM: 4491MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 32
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.03997s, u: 0.364s, s: 0.056s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 46.31s, cpu: 1477s, sys: 0.08s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 263.8MB; VM: 4491MB)
      
      Rank of D 275
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 8.202e-05s, cpu: 0s, sys: 0s): done
      Pivots found: 275
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 5.388e-05s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 0.08483s, cpu: 0.868s, sys: 0.964s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 295.9MB; VM: 4491MB)
      
      True rank 18190
    Finished activity (rea: 55.63s, cpu: 1582s, sys: 2.984s): ROUND 1
  Finished activity (rea: 55.63s, cpu: 1582s, sys: 2.984s): FGL BLOC NEW METHOD
  
  ****	A (21182x 22207)	- nb elements 12741575 - density: 2.709 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 295.9MB; VM: 4491MB)
  
Faugère-Lachartre Bloc Version (1582.34 s)
Finished activity (rea: 55.89s, cpu: 1582s, sys: 3.108s): Faugère-Lachartre Bloc Version
Writing report data to cout

Faugère-Lachartre Bloc Version
Faugère-Lachartre Bloc Version...  [[[starting]]]		 Memory (RSS: 2.04297MB; VM: 38.5312MB)
  Loading matrix loadF4Matrix__low_memory SYS CALL
    
    Loading file: /home/faugere/matrices/F4/kat12/mat6
    21182 x 22207 matrix
    mod 65521
    Nb of Nz elements 24006869 (density 5.10362% ) -	size 137 MB
  Finished activity (rea: 0.2547s, cpu: 0.116s, sys: 0.14s): done
  [[[Loading matrix]]]		 Memory (RSS: 141.1MB; VM: 177.5MB)
  
  Index type uint16
  FGL BLOC NEW METHOD
    ROUND 1
      [Bloc] construting submatrices
        Warning: Maps are not yet constructed. Constructing maps and reinitializing submatrices
      Finished activity (rea: 0.5929s, cpu: 2.236s, sys: 0.876s): done
      [[[[construting submatrices]]]]		 Memory (RSS: 289MB; VM: 4716MB)
      
      Pivots found: 17915
      
      ****	sub_B (17915x 4292 )	- nb elements 9358872 - density: 12.17 % 	 bloc dim 256 x 256  ****
      ****	sub_D ( 3267x 4292 )	- nb elements 3704430 - density: 26.42 % 	 bloc dim 256 x 256  ****
      ****	sub_A_multiline (17915x 17915)	- nb elements 1407806 - density: 0.4386 % 	 bloc dim   ****
      ****	sub_C_multiline ( 3267x 17915)	- nb elements 7558395 - density: 12.91 % 	 bloc dim   ****
      
      [Bloc] C = MatrixOps::reduceC
        [Level3ParallelOps::reduceC__Parallel] NB THREADS 64
      Finished activity (rea: 0.7269s, cpu: 27.4s, sys: 2.796s): [reduceC]
      
      Copy sub_C_multiline to sub_C_bloc
      Finished activity (rea: 1.067s, cpu: 6.744s, sys: 1.388s): [Copy sub_C_multiline to sub_C_bloc]
      
      [[[[Copy sub_C_multiline to sub_C_bloc]]]]		 Memory (RSS: 427.7MB; VM: 8844MB)
      
      [Bloc] D = D - C*B
        [Level3ParallelOps::reduceNonPivotsByPivots__Parallel] NB THREADS 64
      Finished activity (rea: 7.589s, cpu: 78s, sys: 0.056s): [D = D - C*B]
      ****	sub_D ( 3267x 4292 )	- nb elements 14021733 - density: 100 % 	 bloc dim 256 x 256  ****
      [[[[D = D - C*B]]]]		 Memory (RSS: 404.6MB; VM: 8844MB)
      
      [Bloc] echelonize
        [Level3ParallelEchelon::echelonize__Parallel] NB THREADS 64
        Completed activity: copyBlocMatrixToMultilineMatrix (r: 0.1591s, u: 0.548s, s: 0.348s) copyBlocMatrixToMultilineMatrix
      Finished activity (rea: 97.45s, cpu: 6129s, sys: 0.36s): [echelonize D]
      [[[[echelonize D]]]]		 Memory (RSS: 268.4MB; VM: 8844MB)
      
      Rank of D 275
      
      [Bloc] Processing new matrix D
      Finished activity (rea: 0.0001099s, cpu: 0s, sys: 0s): done
      Pivots found: 275
      
      [Bloc] Combine inner indexer
      Finished activity (rea: 7.105e-05s, cpu: 0s, sys: 0s): done
      
      [Bloc] Reconstructing matrix
      Finished activity (rea: 0.07282s, cpu: 1.2s, sys: 1.204s): [Reconstructing matrix]
      [[[[Reconstructing matrix]]]]		 Memory (RSS: 306.1MB; VM: 8844MB)
      
      True rank 18190
    Finished activity (rea: 107.7s, cpu: 6245s, sys: 6.696s): ROUND 1
  Finished activity (rea: 107.7s, cpu: 6245s, sys: 6.696s): FGL BLOC NEW METHOD
  
  ****	A (21182x 22207)	- nb elements 12741575 - density: 2.709 % 	 bloc dim   ****
  [[[[In main]]]]		 Memory (RSS: 306.1MB; VM: 8844MB)
  
Faugère-Lachartre Bloc Version (6245.11 s)
Finished activity (rea: 108s, cpu: 6245s, sys: 6.836s): Faugère-Lachartre Bloc Version
