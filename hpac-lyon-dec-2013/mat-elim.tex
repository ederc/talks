% load all styles, commands, etc.
\input{../style}
\input{../colors-and-tikz}
% for tables
%\setlength{\tabcolsep}{8pt}
\renewcommand{\arraystretch}{1.4}
\title{Hybrid Gaussian Elimination}
\author{Christian Eder, Jean-Charles Faug\`ere, Fayssal Martani\\and Bjarke
  Hammersholt Roune}
\institute{HPAC Meeting -- Lyon}
\date{December 10 -- 11, 2013}
% logo of my university
\titlegraphic{\hspace*{-0.7cm}\includegraphics[width=4.2cm]{../logo/INRIA-transparent.png}\hspace*{5mm}~%
     \includegraphics[width=1.2cm]{../logo/CNRS.jpg}\hspace*{5mm}~%
     \includegraphics[width=2.5cm]{../logo/UPMC-orange-transparent.png}\hspace*{5mm}~%
     \includegraphics[width=1.5cm]{../logo/LIP6.png}
}
%\usetheme{Singapore}
\begin{document}
\nocite{fF41999}
\nocite{FL10b}
\nocite{martani-thesis}
\nocite{lachartre-thesis}
%\renewcommand{\inserttotalframenumber}{13}
\AtBeginSection[]
{
\begin{frame}<beamer>
\frametitle{}
\tableofcontents[currentsection]
\end{frame}
}
\frame{\titlepage}

\section{First step -- Dense Gaussian Elimination}
\begin{frame}
\frametitle{First step -- Dense Gaussian Elimination}
\begin{center}
\includegraphics[width=0.8\textwidth]{f4-kat12-mat6-dense.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Some examples -- Kat16-Mat9}
\begin{center}
\includegraphics[width=\textwidth]{timings-2395-74125-inv.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Some examples -- Minrank-10-9-7-Mat4}
\begin{center}
\includegraphics[width=\textwidth]{timings-3614-60539-inv.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Some examples -- Minrank-10-9-7-Mat6}
\begin{center}
\includegraphics[width=\textwidth]{timings-4040-101500-inv.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{LELA implementation}
\begin{itemize}
\item Using \textcolor{yellow}{pthreads}
\item Idea of structured Gaussian Elimination:
\[\text{row}_i \gets \text{row}_i + \sum_{j=1}^{i-1} a_j \text{row}_j\]
At step $i$ rows $1$ to $i-1$ are reduced.
\begin{itemize}
\item Reduce in parallel rows $i$ to $i+p$ by pivots up to $i-1$
\item Row $i+\text{offset}$ waits until rows from $i$ to $i+\text{offset}-1$ are
done\\
$\Rightarrow$ waiting queue
\item Threads share global variable \texttt{last\_pivot}
\end{itemize}
\item Blocksize $256$ works out best for the shown examples
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Tiled implementation}
\begin{center}
\begin{tikzpicture}[scale=0.5, transform shape]
\fill[fill=lgrey, fill opacity=0.6] (0,0) rectangle (3,15);
\fill[fill=lgrey, fill opacity=0.6] (3,15) rectangle (15,12);
\fill[fill=mygreend, fill opacity=0.6] (3,12) rectangle (6,9);
\fill[fill=myblued, fill opacity=0.6] (9,12) rectangle (12,9);
\fill[fill=myredd!50!myyellowd, fill opacity=0.6] (9,9) rectangle (12,6);
\fill[fill=myredd!50!myyellowd, fill opacity=0.6] (9,6) rectangle (12,3);
\fill[fill=myredd!50!myyellowd, fill opacity=0.6] (9,3) rectangle (12,0);
\node (task_11) at (4.5,10.5) {\large{\bf{Task 11}}};
\node (task_12) at (10.5,10.5) {\large{\bf{Task 12}}};
\node (task_22_1) at (10.5,7.5) {\large{\bf{Task 22}}};
\node (task_22_2) at (10.5,4.5) {\large{\bf{Task 22}}};
\node (task_22_3) at (10.5,1.5) {\large{\bf{Task 22}}};
\path[->] (5.5,10.5) edge[in=170,out=10] (9.5,10.5);
\draw[draw=white, densely dotted] (0,0) rectangle (15,15);
\draw[densely dotted,draw=white] (0.1,3) -- (14.9,3); 
\draw[densely dotted,draw=white] (0.1,6) -- (14.9,6); 
\draw[densely dotted,draw=white] (0.1,9) -- (14.9,9); 
\draw[densely dotted,draw=white] (0.1,12) -- (14.9,12); 
\draw[densely dotted,draw=white] (3,14.9) -- (3,0.1); 
\draw[densely dotted,draw=white] (6,14.9) -- (6,0.1); 
\draw[densely dotted,draw=white] (9,14.9) -- (9,0.1); 
\draw[densely dotted,draw=white] (12,14.9) -- (12,0.1); 
\end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{StarPU implementation}
Tiled Gaussian Elimination\\
\begin{lstlisting}
struct starpu_data_filter f = {
  .filter_func = starpu_matrix_filter_vertical_block,
  .nchildren = lblocks
};

struct starpu_data_filter f2 = {
  .filter_func = starpu_matrix_filter_block,
  .nchildren = mblocks
};

starpu_data_map_filters(dataA, 2, &f, &f2);
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{StarPU Implementation}
Tasks \& Dependencies\\
\small{
\begin{lstlisting}
static int create_task_12(starpu_data_handle_t dataA, unsigned k, unsigned j)
{
  int ret;

  struct starpu_task *task = create_task(TAG12(k, j));

  task->cl = &task_12_cl;

  /* what sub-data is manipulated ? */
  task->handles[0] = starpu_data_get_sub_data(dataA, 2, k, k);
  task->handles[1] = starpu_data_get_sub_data(dataA, 2, k, j);

  if (!no_prio && (j == k+1)) {
	    task->priority = STARPU_MAX_PRIO;
  }

  /* enforce dependencies ... */
  if (k > 0) {
	    starpu_tag_declare_deps(TAG12(k, j), 2, TAG11(k), TAG22(k-1, k, j));
  } else {
	    starpu_tag_declare_deps(TAG12(k, j), 1, TAG11(k));
  }
...
}
\end{lstlisting}
}
\end{frame}

\begin{frame}[fragile]
\frametitle{Intel TBB Implementation}
\begin{lstlisting}
class TASK_12 : public task {
public:
  TYPE *a;
  TYPE *b;
  TYPE offset;
  // successors, all TASK_22
  TASK_22 **task_22_succ;

  TASK_12(TYPE *a_, TYPE *b_, TYPE offset);

  task* execute();
};
\end{lstlisting}

\begin{lstlisting}
for (j=k+1; j<mblocks; ++j) {
  task_12_queue[task_12_ctr] = new(task::allocate_root()) 
    TASK_12(&mat[tile_size*(k+k*m)], &mat[tile_size*(j+k*m)],k);
  task_11_queue[k]->task_12_succ[j-k-1] = task_12_queue[task_12_ctr];
  if (k!=0) {
    task_22_queue[task_22_start_idx+j-k]->task_12_succ =
      task_12_queue[task_12_ctr];
    task_12_queue[task_12_ctr]->set_ref_count(2);
  } else {
    task_12_queue[task_12_ctr]->set_ref_count(1);
  }
  task_12_ctr++;  
}
\end{lstlisting}

\end{frame}
\section{Next steps}
\begin{frame}
\frametitle{Next steps}
\begin{itemize}
\item Finish Intel TBB implementation of dense Gaussian Elimination and compare
to StarPU
\item Improve scheduling for \#columns $>>$ \#rows
\item Make these implementations available in LELA resp. MATHICGB
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Directly reduce C?}
\begin{center}
\includegraphics[width=0.8\textwidth]{f4-kat12-mat6-direct.pdf}
\end{center}
\end{frame}
\begin{frame}
\frametitle{References}
\begingroup
\scriptsize
\bibliographystyle{plain}
\bibliography{../bib}
\endgroup
\end{frame}

\end{document}

