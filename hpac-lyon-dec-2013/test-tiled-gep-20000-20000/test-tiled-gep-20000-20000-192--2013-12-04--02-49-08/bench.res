20000,20000
4, 8, 16, 32, 64
=======================================================
Method: Intel TBB - tiled Gaussian Elimination
-------------------------------------------------------
Field characteristic: 65521
modulus computations: yes, delayed
-------------------------------------------------------
Tile size:            192
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Matrix sizes
> user input:         20000 x 20000
> generated:          20160 x 20160
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
# row blocks:         105
# column blocks:      105
-------------------------------------------------------
# Threads:            4
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Real time:            950.217 sec
CPU time:             3796.270 sec
CPU/real time:        4.00
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
GFLOPS/sec:           9.82
-------------------------------------------------------
Shutting down
=======================================================
=======================================================
Method: Intel TBB - tiled Gaussian Elimination
-------------------------------------------------------
Field characteristic: 65521
modulus computations: yes, delayed
-------------------------------------------------------
Tile size:            192
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Matrix sizes
> user input:         20000 x 20000
> generated:          20160 x 20160
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
# row blocks:         105
# column blocks:      105
-------------------------------------------------------
# Threads:            8
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Real time:            476.685 sec
CPU time:             3808.130 sec
CPU/real time:        7.99
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
GFLOPS/sec:           19.58
-------------------------------------------------------
Shutting down
=======================================================
=======================================================
Method: Intel TBB - tiled Gaussian Elimination
-------------------------------------------------------
Field characteristic: 65521
modulus computations: yes, delayed
-------------------------------------------------------
Tile size:            192
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Matrix sizes
> user input:         20000 x 20000
> generated:          20160 x 20160
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
# row blocks:         105
# column blocks:      105
-------------------------------------------------------
# Threads:            16
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Real time:            252.775 sec
CPU time:             4036.650 sec
CPU/real time:        15.97
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
GFLOPS/sec:           36.92
-------------------------------------------------------
Shutting down
=======================================================
=======================================================
Method: Intel TBB - tiled Gaussian Elimination
-------------------------------------------------------
Field characteristic: 65521
modulus computations: yes, delayed
-------------------------------------------------------
Tile size:            192
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Matrix sizes
> user input:         20000 x 20000
> generated:          20160 x 20160
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
# row blocks:         105
# column blocks:      105
-------------------------------------------------------
# Threads:            32
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Real time:            138.250 sec
CPU time:             4409.720 sec
CPU/real time:        31.90
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
GFLOPS/sec:           67.51
-------------------------------------------------------
Shutting down
=======================================================
=======================================================
Method: Intel TBB - tiled Gaussian Elimination
-------------------------------------------------------
Field characteristic: 65521
modulus computations: yes, delayed
-------------------------------------------------------
Tile size:            192
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Matrix sizes
> user input:         20000 x 20000
> generated:          20160 x 20160
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
# row blocks:         105
# column blocks:      105
-------------------------------------------------------
# Threads:            64
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Real time:            134.354 sec
CPU time:             8561.140 sec
CPU/real time:        63.72
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
GFLOPS/sec:           69.47
-------------------------------------------------------
Shutting down
=======================================================
=======================================================
Method: StarPU - tiled Gaussian Elimination
-------------------------------------------------------
Field characteristic: 65521
modulus computations: yes, delayed
-------------------------------------------------------
Tile size:            192
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Matrix sizes
> user input:         20000 x 20000
> generated:          20160 x 20160
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
# row blocks:         105
# column blocks:      105
-------------------------------------------------------
#Threads:             4
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Real time:            969.248 sec
CPU time:             3874.700 sec
CPU/real time:        4.00
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
GFLOPS/sec:           9.63
-------------------------------------------------------
Shutting down
=======================================================
=======================================================
Method: StarPU - tiled Gaussian Elimination
-------------------------------------------------------
Field characteristic: 65521
modulus computations: yes, delayed
-------------------------------------------------------
Tile size:            192
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Matrix sizes
> user input:         20000 x 20000
> generated:          20160 x 20160
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
# row blocks:         105
# column blocks:      105
-------------------------------------------------------
#Threads:             8
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Real time:            533.620 sec
CPU time:             4266.270 sec
CPU/real time:        7.99
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
GFLOPS/sec:           17.49
-------------------------------------------------------
Shutting down
=======================================================
=======================================================
Method: StarPU - tiled Gaussian Elimination
-------------------------------------------------------
Field characteristic: 65521
modulus computations: yes, delayed
-------------------------------------------------------
Tile size:            192
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Matrix sizes
> user input:         20000 x 20000
> generated:          20160 x 20160
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
# row blocks:         105
# column blocks:      105
-------------------------------------------------------
#Threads:             16
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Real time:            269.996 sec
CPU time:             4317.120 sec
CPU/real time:        15.99
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
GFLOPS/sec:           34.57
-------------------------------------------------------
Shutting down
=======================================================
=======================================================
Method: StarPU - tiled Gaussian Elimination
-------------------------------------------------------
Field characteristic: 65521
modulus computations: yes, delayed
-------------------------------------------------------
Tile size:            192
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Matrix sizes
> user input:         20000 x 20000
> generated:          20160 x 20160
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
# row blocks:         105
# column blocks:      105
-------------------------------------------------------
#Threads:             32
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Real time:            135.131 sec
CPU time:             4322.310 sec
CPU/real time:        31.99
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
GFLOPS/sec:           69.07
-------------------------------------------------------
Shutting down
=======================================================
=======================================================
Method: StarPU - tiled Gaussian Elimination
-------------------------------------------------------
Field characteristic: 65521
modulus computations: yes, delayed
-------------------------------------------------------
Tile size:            192
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Matrix sizes
> user input:         20000 x 20000
> generated:          20160 x 20160
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
# row blocks:         105
# column blocks:      105
-------------------------------------------------------
#Threads:             64
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Real time:            140.577 sec
CPU time:             4488.140 sec
CPU/real time:        31.93
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
GFLOPS/sec:           66.39
-------------------------------------------------------
Shutting down
=======================================================
