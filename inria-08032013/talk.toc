\beamer@endinputifotherversion {3.10pt}
\select@language {ngerman}
\beamer@sectionintoc {2}{The basic problem}{2}{0}{1}
\beamer@sectionintoc {3}{Generic signature-based algorithms}{8}{0}{2}
\beamer@subsectionintoc {3}{1}{The basic idea}{9}{0}{2}
\beamer@subsectionintoc {3}{2}{Generic signature-based Gr\"obner basis algorithm}{22}{0}{2}
\beamer@subsectionintoc {3}{3}{Signature-based criteria}{33}{0}{2}
\beamer@sectionintoc {4}{Implementations and recent work}{38}{0}{3}
\beamer@subsectionintoc {4}{1}{A good decade on signature-based algorithms}{39}{0}{3}
\beamer@subsectionintoc {4}{2}{Implementation in Singular}{49}{0}{3}
\beamer@sectionintoc {5}{And what has really happened?}{51}{0}{4}
\beamer@subsectionintoc {5}{1}{Ongoing work}{64}{0}{4}
