\beamer@endinputifotherversion {3.26pt}
\select@language {ngerman}
\beamer@sectionintoc {1}{Overall structure}{2}{0}{1}
\beamer@sectionintoc {2}{Matrix reduction part}{19}{0}{2}
\beamer@sectionintoc {3}{Methods of Parallelization}{29}{0}{3}
\beamer@sectionintoc {4}{Future steps}{37}{0}{4}
