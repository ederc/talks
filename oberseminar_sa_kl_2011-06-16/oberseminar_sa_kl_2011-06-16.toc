\beamer@endinputifotherversion {3.07pt}
\beamer@sectionintoc {2}{Introducing Gr\"obner bases}{3}{0}{1}
\beamer@subsectionintoc {2}{1}{Gr\"obner basics}{4}{0}{1}
\beamer@subsectionintoc {2}{2}{Computation of Gr\"obner bases}{8}{0}{1}
\beamer@subsectionintoc {2}{3}{Problem of zero reduction}{19}{0}{1}
\beamer@sectionintoc {3}{Signature-based algorithms}{30}{0}{2}
\beamer@subsectionintoc {3}{1}{The basic idea}{31}{0}{2}
\beamer@subsectionintoc {3}{2}{Computing Gr\"obner bases using signatures}{48}{0}{2}
\beamer@subsectionintoc {3}{3}{How to reject useless pairs?}{66}{0}{2}
\beamer@sectionintoc {4}{GGV and F5 -- Differences and similarities}{70}{0}{3}
\beamer@subsectionintoc {4}{1}{What are the differences?}{71}{0}{3}
\beamer@subsectionintoc {4}{2}{F5}{75}{0}{3}
\beamer@subsectionintoc {4}{3}{GGV}{78}{0}{3}
\beamer@subsectionintoc {4}{4}{F5E -- Combine the ideas}{84}{0}{3}
\beamer@sectionintoc {5}{Experimental results}{86}{0}{4}
\beamer@subsectionintoc {5}{1}{Preliminaries}{87}{0}{4}
\beamer@subsectionintoc {5}{2}{Critical pairs \& zero reductions}{90}{0}{4}
\beamer@subsectionintoc {5}{3}{Timings}{92}{0}{4}
\beamer@sectionintoc {6}{Outlook}{93}{0}{5}
